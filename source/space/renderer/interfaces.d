module space.renderer.interfaces;

struct ImageData
{
    ubyte[] data;
    int width, height;
}

alias DataSource = ImageData delegate();

interface RendererInterface
{
    struct Vertex
    {
        float[3] position = [ 0, 0, 0 ];
        float[2] texcoord = [ 0, 0 ];
        float[4] colour = [ 0, 0, 0, 0 ];
        float[4] fog = [ 0, 0, 0, 0 ];
    }

    void startFrame();
    void finishFrame();
    void clearDepth();
    void setCamera(float[3] forward, float[3] right, float[3] up, float[3] origin, float hFov, float vFov, float near, float far);
    void set2D();
    void drawMesh(const Vertex[] verts);

    // Texture index 0 should always be a blank texture (full brightness on each colour channel and opaque alpha).
    int allocateTexture();
    void setTextureDataSource(int index, DataSource dataSource);
    void refreshTexture(int index);
    void setTexture(int slot, int textureIndex);
}

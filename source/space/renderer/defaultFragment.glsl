#version 110

uniform sampler2D sTexture;
uniform sampler2D sDetailTexture;

varying vec2 vTexCoord;
varying vec4 vColour;
varying vec4 vFog;

void main()
{
    // Blend detail texture based on its alpha.
    vec4 main = texture2D(sTexture, vTexCoord);

    vec4 detail = texture2D(sDetailTexture, vTexCoord);
    float detailBlend = detail.a * 0.2;

    vec4 fog = vec4(vFog.xyz, 1.0);
    float fogBlend = vFog.a;

    vec4 colour = main * (1.0 - detailBlend) + detail * detailBlend;
    colour *= vColour;
    colour = colour * (1.0 - fogBlend) + fog * fogBlend;

    // Quantizes to some number of colours:
    //colour = floor(colour * 16.99) / 16.0; // 4096
    //colour = floor(colour * 8.99) / 8.0; // 512
    //colour = floor(colour * 4.99) / 4.0; // 64
    gl_FragColor = colour;
}

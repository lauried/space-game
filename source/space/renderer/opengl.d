module space.renderer.opengl;

import space.exceptions;
import space.library.geometry.matrix;
import space.platform.interfaces;
import space.renderer.interfaces;
import derelict.opengl;
import std.conv;
import std.string;
import std.stdio;

class OpenGl : RendererInterface
{
    private struct Shader
    {
        GLuint program;
        GLuint vertex;
        GLuint fragment;
    }

    private struct DefaultShader
    {
        Shader shader;
        // attributes
        GLint position;
        GLint texCoord;
        GLint colour;
        GLint fog;
        // uniforms
        GLint texture;
        GLint detailTexture;
        GLint matrix;
    }

    private struct TextureData
    {
        bool refresh;
        bool allocated;
        GLuint texnum;
        DataSource dataSource;
    }

    private PlatformInterface platform;

    private DefaultShader defaultShader;
    int[2] screenSize;
    private Matrix!float cameraMatrix;
    private Matrix!float perspectiveMatrix;
    private Matrix!float combinedMatrix;
    //private GLuint sampler;

    private TextureData[] textures;

    this(PlatformInterface platform)
    {
        this.platform = platform;

        platform.getAfterInit().add(&this.afterInit);
        platform.getOnVideoMode().add(&this.onVideoMode);

        // Make index 0 be blank, so we needn't create blank textures where we want solid colour.
        allocateTexture();
        setTextureDataSource(0, delegate() {
            ubyte[] data = [ 255, 255, 255, 255 ];
            return ImageData(data, 1, 1);
        });
    }

    void startFrame()
    {
        glViewport(0, 0, screenSize[0], screenSize[1]);
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClearDepth(0.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_GEQUAL);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glUseProgram(defaultShader.shader.program);
    }

    void finishFrame()
    {
        platform.finishRenderFrame();
    }

    void clearDepth()
    {
        glClear(GL_DEPTH_BUFFER_BIT);
    }

    void setCamera(float[3] forward, float[3] right, float[3] up, float[3] origin, float hFov, float vFov, float near, float far)
    {
        cameraMatrix = Matrix!float.camera(forward, right, up, origin);
        
        // View matrix maps from camera space into OpenGL homogeneous coordinates:
        // +x is right, +y is up, -z is forward
        Matrix!float swizzleMatrix;
        swizzleMatrix.m = [
            [  0,  0, -1,  0 ], // forward
            [  1,  0,  0,  0 ], // right
            [  0,  1,  0,  0 ], // up
            [  0,  0,  0,  1 ],
            // x,  y,  z,  w
        ];

        perspectiveMatrix = Matrix!float.reversePerspective(hFov, vFov, near, far);
        combinedMatrix = perspectiveMatrix.multiply(swizzleMatrix).multiply(cameraMatrix);
        float[16] flatMatrix = combinedMatrix.toFlat();
        glUniformMatrix4fv(defaultShader.matrix, 1, GL_FALSE, &flatMatrix[0]);

        glEnable(GL_DEPTH_TEST);
    }

    void set2D()
    {
        Matrix!float ortho = Matrix!float.orthographicDocument(0, screenSize[0], 0, screenSize[1], -1, 1);
        float[16] flatMatrix = ortho.toFlat();
        glUniformMatrix4fv(defaultShader.matrix, 1, GL_FALSE, &flatMatrix[0]);

        glDisable(GL_DEPTH_TEST);
    }

    void drawMesh(const Vertex[] verts)
    {
        if (!verts.length)
        {
            return;
        }

        if (defaultShader.position >= 0)
        {
            glVertexAttribPointer(
                defaultShader.position,
                4,
                GL_FLOAT,
                GL_FALSE,
                Vertex.sizeof,
                cast(void*)&(verts[0].position[0])
            );
            glEnableVertexAttribArray(defaultShader.position);
        }

        if (defaultShader.texCoord >= 0)
        {
            glVertexAttribPointer(
                defaultShader.texCoord,
                2,
                GL_FLOAT,
                GL_FALSE,
                Vertex.sizeof,
                cast(void*)&(verts[0].texcoord[0])
            );
            glEnableVertexAttribArray(defaultShader.texCoord);
        }

        if (defaultShader.colour >= 0)
        {
            glVertexAttribPointer(
                defaultShader.colour,
                4,
                GL_FLOAT,
                GL_FALSE,
                Vertex.sizeof,
                cast(void*)&(verts[0].colour[0])
            );
            glEnableVertexAttribArray(defaultShader.colour);
        }

        if (defaultShader.fog >= 0)
        {
            glVertexAttribPointer(
                defaultShader.fog,
                4,
                GL_FLOAT,
                GL_FALSE,
                Vertex.sizeof,
                cast(void*)&(verts[0].fog[0])
            );
            glEnableVertexAttribArray(defaultShader.fog); checkError();
        }

        glDrawArrays(GL_TRIANGLES, 0, cast(int) verts.length);

        if (defaultShader.position >= 0)
        {
            glDisableVertexAttribArray(defaultShader.position);
        }
        if (defaultShader.texCoord >= 0)
        {
            glDisableVertexAttribArray(defaultShader.texCoord);
        }
        if (defaultShader.colour >= 0)
        {
            glDisableVertexAttribArray(defaultShader.colour);
        }
        if (defaultShader.fog >= 0)
        {
            glDisableVertexAttribArray(defaultShader.fog);
        }
    }

    int allocateTexture()
    {
        textures.length += 1;
        int index = cast(int)textures.length - 1;
        textures[index].allocated = false;
        textures[index].refresh = true;
        return index;
    }

    void setTextureDataSource(int index, DataSource dataSource)
    {
        textures[index].dataSource = dataSource;
        textures[index].refresh = true;
    }

    void refreshTexture(int index)
    {
        auto texture = &textures[index];

        if (!texture.refresh)
        {
            return;
        }

        if (!texture.allocated)
        {
            glGenTextures(1, &texture.texnum);
            texture.allocated = true;
        }

        auto image = texture.dataSource();

        glBindTexture(GL_TEXTURE_2D, texture.texnum); checkError();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST); checkError();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); checkError();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); checkError();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); checkError();

        glTexStorage2D(GL_TEXTURE_2D, mipLevelsForSize(image.width, image.height), GL_RGBA8, image.width, image.height); checkError();
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image.width, image.height, GL_RGBA, GL_UNSIGNED_BYTE, &image.data[0]); checkError();
        glGenerateMipmap(GL_TEXTURE_2D); checkError();

        texture.refresh = false;
    }

    void setTexture(int slot, int textureIndex)
    {
        glActiveTexture(GL_TEXTURE0 + slot); checkError();
        glBindTexture(GL_TEXTURE_2D, textures[textureIndex].texnum); checkError();
        refreshTexture(textureIndex);
    }

    private void afterInit()
    {
        // TODO: Log instead of print.
        auto glVersion = glGetString(GL_VERSION);
        writeln("OpenGL version: " ~ to!string(glVersion));

        defaultShader.shader = createShader(
            import("space/renderer/defaultVertex.glsl"),
            import("space/renderer/defaultFragment.glsl")
        );
        glUseProgram(defaultShader.shader.program);
        defaultShader.position = glGetAttribLocation(defaultShader.shader.program, "aPosition"); checkError();
        defaultShader.texCoord = glGetAttribLocation(defaultShader.shader.program, "aTexCoord"); checkError();
        defaultShader.colour = glGetAttribLocation(defaultShader.shader.program, "aColour"); checkError();
        defaultShader.fog = glGetAttribLocation(defaultShader.shader.program, "aFog"); checkError();

        defaultShader.texture = glGetUniformLocation(defaultShader.shader.program, "sTexture"); checkError();
        defaultShader.detailTexture = glGetUniformLocation(defaultShader.shader.program, "sDetailTexture"); checkError();
        defaultShader.matrix = glGetUniformLocation(defaultShader.shader.program, "mMatrix"); checkError();

        glUniform1i(defaultShader.texture, 0); checkError();
        glUniform1i(defaultShader.detailTexture, 1); checkError();

        // glGenSamplers(1, &sampler); checkError();
        // glSamplerParameterf(sampler, GL_TEXTURE_LOD_BIAS, 1.5f); checkError();
        // glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR); checkError();
        // glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST); checkError();
        // glBindSampler(0, sampler); checkError();

        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
    }

    private void onVideoMode(int width, int height, bool fullscreen)
    {
        screenSize[0] = width;
        screenSize[1] = height;
        // TODO: If it's Windows, do we still need to re-upload all the textures?
    }

    private Shader createShader(string vertex, string fragment)
    {
        Shader shader;

        shader.vertex = compileShader(vertex, GL_VERTEX_SHADER);
        shader.fragment = compileShader(fragment, GL_FRAGMENT_SHADER);

        shader.program = glCreateProgram();
        glAttachShader(shader.program, shader.vertex);
        glAttachShader(shader.program, shader.fragment);
        glLinkProgram(shader.program);

        GLint programOK;
        glGetProgramiv(shader.program, GL_LINK_STATUS, &programOK);
        if (programOK != GL_TRUE)
        {
            GLsizei logSize;
            glGetProgramiv(shader.program, GL_INFO_LOG_LENGTH, &logSize);
            char[] log = new char[logSize];
            glGetProgramInfoLog(shader.program, logSize, null, log.ptr);
            glDeleteProgram(shader.program);
            glDeleteShader(shader.vertex);
            glDeleteShader(shader.fragment);
            throw new RuntimeException("Failed to compile program: '" ~ to!string(log) ~ "'");
        }

        return shader;
    }

    private GLuint compileShader(string source, GLenum shaderType)
    {
        GLuint shader = glCreateShader(shaderType);
        const GLchar *pointer = toStringz(source);
        GLint length = cast(GLint)source.length;
        glShaderSource(shader, 1, &pointer, &length);
        glCompileShader(shader);

        GLint shaderOK;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &shaderOK);
        if (shaderOK != GL_TRUE)
        {
            GLsizei logSize;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
            char[] log = new char[logSize];
            glGetShaderInfoLog(shader, logSize, null, log.ptr);
            glDeleteShader(shader);
            throw new RuntimeException("Failed to compile shader: '" ~ to!string(log) ~ "'");
        }

        return shader;
    }

    int mipLevelsForSize(int width, int height)
    {
        if (height < width)
        {
            width = height;
        }

        int levels = 1;
        while (width >>= 1)
        {
            levels += 1;
        }

        return levels;
    }

    pragma(inline, true) private void checkError()
    {
        auto error = glGetError();
        if (error)
        {
            throw new LogicException("OpenGL error " ~ to!string(error));
        }
    }
}

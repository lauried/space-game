#version 110

attribute vec3 aPosition;
attribute vec2 aTexCoord;
attribute vec4 aColour;
attribute vec4 aFog;

uniform mat4 mMatrix;

varying vec2 vTexCoord;
varying vec4 vColour;
varying vec4 vFog;

void main()
{
    gl_Position = mMatrix * vec4(aPosition, 1);
    vTexCoord = aTexCoord;
    vColour = aColour;
    vFog = aFog;
}

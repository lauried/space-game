module space.engine.events.collision;

import entitysysd;

@event struct CollisionEvent
{
    Entity mover;
    float[3] point;
    float[3] normal;
    float[3] velocity;
}

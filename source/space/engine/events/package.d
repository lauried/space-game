module space.engine.events;

import entitysysd;

class Events
{
    private EventManager eventManager;

    this()
    {
        this.eventManager = new EventManager();
    }

    EventManager manager() @property { return eventManager; }
}
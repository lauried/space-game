module space.engine.entity;

// https://code.dlang.org/packages/entitysysd
// https://github.com/claudemr/entitysysd
import entitysysd;
import space.library.geometry.orientation;
import space.models.interfaces;

class Ecs : EntitySysD
{
}

@component struct PositionComponent
{
    Orientation!float orientation = Orientation!float.identity();
}

@component struct MovementComponent
{
    float[3] velocity = [ 0, 0, 0 ];
    float[3] avelocity = [ 0, 0, 0 ];
}

@component struct DynamicsComponent
{
    float[3] size;
    float mass;
    float[3] moment;
    float radius;

    void setSize(float[3] size, float mass)
    {
        this.size = size;
        this.mass = mass;
        moment[0] = (1/12) * mass * (size[1] * size[1] + size[2] * size[2]);
        moment[1] = (1/12) * mass * (size[0] * size[0] + size[2] * size[2]);
        moment[2] = (1/12) * mass * (size[0] * size[0] + size[1] * size[1]);
    }

    void setRadius(float radius)
    {
        this.radius = radius;
    }
}

@component struct ColliderComponent
{
    ColliderInstance collider = null;
}

@component struct ModelComponent
{
    ModelInstance model = null;
}

@component struct AtmosphereModelComponent
{
    ModelInstance model = null;
    float radius = 0;
    float[4] colour = [ 0, 0, 0, 0 ];
    float density = 0;
}

@component struct BackgroundModelComponent
{
    ModelInstance model = null;
}

@component struct LightingComponent
{
    float[4] light = [ 1, 1, 1, 1 ];
    float[4] ambientLight = [ 0, 0, 0, 1 ];
}

@component struct AtmosphereComponent
{
    // atmosphere lies within radius, depth is approximate distance from top to radius of planet.
    float radius = 0;
    float depth = 0;
    float density = 0;
}

@component struct WaypointComponent
{
    string name;
}

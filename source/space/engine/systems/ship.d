module space.engine.systems.ship;

import space.engine.entity;
import space.engine.systems.atmosphere;
import space.library.geometry.vector;
import entitysysd;
import std.algorithm;
import std.math;

@component struct ShipComponent
{
    enum Mode
    {
        wire,
        manual,
    }

    enum AutoLevelMode
    {
        none,
        roll,
        both,
        pitch,
    }

    // Input given to the ship. There'll be other things to control later.
    float[3] rotation = [ 0, 0, 0 ]; // -1 to 1 in each axis.
    float[3] throttle = [ 0, 0, 0 ]; // essentially desired speed.
    Mode mode = Mode.wire;
    AutoLevelMode autoLevel = AutoLevelMode.roll;
    bool autoLevelActive = false;

    // Units per second per second. Game uses a funky scale so this doesn't correspond to reality.
    float engineThrust = 1000;
    float lateralThrust = 250;
    // Maximum desired speed.
    float setSpeed = 3000;
    // Radians per second, and their acceleration.
    float turnSpeed = 2;
    float turnAccel = 1.5;
}

class ShipSystem : System
{
    private AtmosphereSystem atmosphereSystem;

    this(AtmosphereSystem atmosphereSystem)
    {
        this.atmosphereSystem = atmosphereSystem;
    }

    override void run(EntityManager entities, EventManager events, Duration dt)
    {
        auto usecs = dt.total!"usecs";
        float frametime = usecs / 1_000_000.0f;
        Atmosphere[] atmospheres = atmosphereSystem.getAtmosphereList(entities);

        foreach (entity, position, movement, ship;
            entities.entitiesWith!(PositionComponent, MovementComponent, ShipComponent))
        {
            applyRoll(position, movement, ship, frametime, atmospheres);
            applyThrust(position, movement, ship, frametime);
        }
    }

    void applyThrust(PositionComponent* position, MovementComponent* movement, ShipComponent* ship, float frametime)
    {
        float[3] requiredChange;
        switch (ship.mode)
        {
            default:
            case ShipComponent.Mode.wire:
                {
                    float[3] setSpeed = [
                        ship.throttle[0] * ship.setSpeed,
                        0,
                        0,
                    ];
                    float[3] relativeVelocity = position.orientation.directionToInternalSpace(movement.velocity);
                    requiredChange = setSpeed[] - relativeVelocity[];
                }
                break;
            case ShipComponent.Mode.manual:
                {
                    requiredChange = [
                        ship.throttle[0] > 0 ? ship.throttle[0] * ship.throttle[0] * ship.engineThrust : ship.throttle[0] * ship.lateralThrust,
                        ship.throttle[1] * ship.lateralThrust,
                        ship.throttle[2] * ship.lateralThrust,
                    ];
                }
                break;
        }

        // TODO: Reduce our forward thrust if we're using lateral thrust.
        // It'll stop making people have to strafe everywhere to be maximally efficient.
        requiredChange[0] = max(-ship.lateralThrust * frametime, min(ship.engineThrust * frametime, requiredChange[0]));
        for (int axis = 1; axis < 3; axis += 1)
        {
            requiredChange[axis] = max(-ship.lateralThrust * frametime, min(ship.lateralThrust * frametime, requiredChange[axis]));
        }

        movement.velocity[] += position.orientation.forward[] * requiredChange[0];
        movement.velocity[] += position.orientation.right[] * requiredChange[1];
        movement.velocity[] += position.orientation.up[] * requiredChange[2];
    }

    void applyRoll(PositionComponent* position, MovementComponent* movement, ShipComponent* ship, float frametime, Atmosphere[] atmospheres)
    {
        float[3] desiredRotation = ship.rotation;
        ship.autoLevelActive = false;
        if (ship.autoLevel != ShipComponent.AutoLevelMode.none)
        {
            desiredRotation = applyAutoLevel(desiredRotation, position, ship, atmospheres);
        }

        float[3] relativeAvelocity = position.orientation.directionToInternalSpace(movement.avelocity);
        for (int axis = 0; axis < 3; axis += 1)
        {
            float desired = max(-ship.turnSpeed, min(ship.turnSpeed, desiredRotation[axis]));
            float diff = desired - relativeAvelocity[axis];
            diff = max(-ship.turnAccel * frametime, min(ship.turnAccel * frametime, diff));
            relativeAvelocity[axis] += diff;
        }
        movement.avelocity = position.orientation.directionToExternalSpace(relativeAvelocity);
    }

    float[3] applyAutoLevel(float[3] desiredRotation, PositionComponent* position, ShipComponent* ship, Atmosphere[] atmospheres)
    {
        auto mode = ship.autoLevel;
        foreach (atmosphere; atmospheres)
        {
            float[3] dir = position.orientation.position[] - atmosphere.position[];
            if (dotProduct(dir, dir) > atmosphere.radiusSquared)
            {
                continue;
            }

            ship.autoLevelActive = true;

            float dist;
            dir = normalize(position.orientation.pointToInternalSpace(atmosphere.position), dist);

            // Blend by fraction of depth into atmosphere that we are.
            float shipAltitude = atmosphere.depth - (atmosphere.radius - dist);
            if (shipAltitude < 0)
            {
                shipAltitude = 0;
            }
            float blend = 1 - (shipAltitude / atmosphere.depth);

            if (abs(desiredRotation[0]) < 0.1 && (mode == ShipComponent.AutoLevelMode.roll || mode == ShipComponent.AutoLevelMode.both))
            {
                desiredRotation[0] = -dir[1] * blend;
            }
            if (abs(desiredRotation[1]) < 0.1 && (mode == ShipComponent.AutoLevelMode.pitch || mode == ShipComponent.AutoLevelMode.both))
            {
                desiredRotation[1] = dir[0] * blend;
            }

            // We can only be inside one atmosphere, so don't bother checking the others.
            break;
        }

        return desiredRotation;
    }
}

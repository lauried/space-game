module space.engine.systems.health;

import space.engine.entity;
import space.engine.events;
import space.engine.events.collision;
import space.library.geometry.vector;
import entitysysd;
import std.algorithm;

import std.stdio;
import std.conv;

@component struct HealthComponent
{
    float health;
}

class HealthSystem : System, IReceiver!CollisionEvent
{
    private Events events;

    this(Events events)
    {
        this.events = events;
        events.manager.subscribe!CollisionEvent(this);
    }

    void receive(CollisionEvent collision)
    {
        float speed = -dotProduct(collision.normal, collision.velocity);
        float damage = max(0, speed) * 0.02;

        auto healthComponent = collision.mover.component!HealthComponent;
        healthComponent.health = max(0, healthComponent.health - damage);
    }

    override void run(EntityManager entities, EventManager events, Duration dt)
    {
    }
}
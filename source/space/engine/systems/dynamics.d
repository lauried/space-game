module space.engine.systems.dynamics;

import space.engine.entity;
import space.engine.events;
import space.engine.events.collision;
import space.engine.systems.atmosphere;
import space.library.geometry.intersection;
import space.library.geometry.vector;
import entitysysd;
import std.conv;
import std.math;
import std.algorithm;

class DynamicsSystem : System
{
    private Events events;
    private AtmosphereSystem atmosphereSystem;

    this(Events events, AtmosphereSystem atmosphereSystem)
    {
        this.events = events;
        this.atmosphereSystem = atmosphereSystem;
    }

    override void run(EntityManager entities, EventManager events, Duration dt)
    {
        processDynamics(entities, dt);
        processAtmosphericDrag(entities, dt);
        processSpaceDrag(entities, dt);
    }

    void processDynamics(EntityManager entities, Duration dt)
    {
        auto usecs = dt.total!"usecs";
        float frametime = usecs / 1_000_000.0f;

        // Move entities.
        foreach (entity, position, movement; entities.entitiesWith!(PositionComponent, MovementComponent))
        {
            if (movement.velocity[0] || movement.velocity[1] || movement.velocity[2])
            {
                position.orientation.position[] += movement.velocity[] * frametime;
            }

            if (movement.avelocity[0] || movement.avelocity[1] || movement.avelocity[2])
            {
                float angle = length(movement.avelocity) * frametime;
                float[3] axis = normalize(movement.avelocity);

                auto rotate = RotateVectorAroundAxis!float(axis);
                position.orientation.forward = rotate.rotateVector(position.orientation.forward, angle);
                position.orientation.right = rotate.rotateVector(position.orientation.right, angle);
                position.orientation.up = rotate.rotateVector(position.orientation.up, angle);
            }
        }

        // Get list of colliders.
        struct Collider {
            PositionComponent* position;
            ColliderComponent* collider;
        }

        Collider[] colliders;

        // A list of all the colliders.
        foreach (entity, position, collider; entities.entitiesWith!(PositionComponent, ColliderComponent))
        {
            colliders ~= Collider(position, collider);
        }

        // Do collision with colliders.
        foreach (entity, position, movement, dynamics; entities.entitiesWith!(PositionComponent, MovementComponent, DynamicsComponent))
        {
            float entRadiusSquared = dynamics.radius * dynamics.radius;
            foreach (collider; colliders)
            {
                // Bounds check using square of distance.
                float colliderRadius = collider.collider.collider.getBoundingRadius();
                float[3] dir = collider.position.orientation.position[] - position.orientation.position[];
                if (dotProduct!float(dir, dir) > entRadiusSquared + colliderRadius * colliderRadius)
                {
                    continue;
                }

                float[3] colliderSpacePosition = collider.position.orientation.pointToInternalSpace(position.orientation.position);
                float[3] intersection = collider.collider.collider.collideSphere(colliderSpacePosition, dynamics.radius);
                if (intersection[0] == 0 && intersection[1] == 0 && intersection[2] == 0)
                {
                    continue;
                }

                intersection = collider.position.orientation.directionToExternalSpace(intersection);
                // De-intersect model
                position.orientation.position[] += intersection[];
                // Reflect velocity so we don't reintersect
                float[3] oldVelocity = movement.velocity[];
                intersection = normalize!float(intersection);
                float dot = dotProduct(intersection, movement.velocity);
                if (dot < 0)
                {
                    movement.velocity[] += dot * -2 * intersection[];
                }

                events.manager.emit!CollisionEvent(
                    entity,
                    position.orientation.position,
                    intersection,
                    oldVelocity,
                );
            }
        }
    }

    void processAtmosphericDrag(EntityManager entities, Duration dt)
    {
        auto usecs = dt.total!"usecs";
        float frametime = usecs / 1_000_000.0f;
        Atmosphere[] atmospheres = atmosphereSystem.getAtmosphereList(entities);

        // Note: dynamics is just for selecting entities to have drag applied to them,
        // we don't do anything with the component.
        foreach (entity, dynamics, position, movement;
            entities.entitiesWith!(DynamicsComponent, PositionComponent, MovementComponent))
        {
            foreach (atmosphere; atmospheres)
            {
                if (!atmosphere.density)
                {
                    continue;
                }

                float[3] dir = position.orientation.position[] - atmosphere.position[];
                if (dotProduct(dir, dir) > atmosphere.radiusSquared)
                {
                    continue;
                }

                float depth = atmosphere.radius - length(dir);
                float drag = atmosphere.density * min(1, depth / atmosphere.depth);
                applyDrag(movement, drag, frametime);
            }
        }
    }

    void processSpaceDrag(EntityManager entities, Duration dt)
    {
        auto usecs = dt.total!"usecs";
        float frametime = usecs / 1_000_000.0f;

        // Note: dynamics is just for selecting entities to have drag applied to them.
        foreach (entity, dynamics, position, movement;
            entities.entitiesWith!(DynamicsComponent, PositionComponent, MovementComponent))
        {
            float drag = 0.0001;
            applyDrag(movement, drag, frametime);
        }
    }

    private void applyDrag(MovementComponent* movement, float drag, float frametime)
    {
        float speed;
        float[3] force = normalize(movement.velocity, speed);
        if (speed > 1)
        {
            movement.velocity[] += force[] * -1 * drag * speed * speed * frametime;
        }
    }
}

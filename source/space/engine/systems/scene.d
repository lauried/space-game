module space.engine.systems.scene;

import space.drawing.model;
import space.engine.entity;
import space.renderer.interfaces;
import space.library.geometry.orientation;
import space.library.geometry.matrix;
import space.library.geometry.frustum;
import space.library.geometry.vector;
import space.models.interfaces;
import space.models.atmosphere;
import entitysysd;
import std.container.rbtree;

/**
 * This unit deals with drawing the scene at a high level.
 * Things like depth and effect layering.
 */
class SceneSystem : System
{
    private RendererInterface renderer;
    private ModelDrawing modelDrawing;
    Orientation!float camera;
    float fovX, fovY;

    this(RendererInterface renderer, ModelDrawing modelDrawing)
    {
        this.renderer = renderer;
        this.modelDrawing = modelDrawing;
    }

    void setCamera(Orientation!float camera, float fovX, float fovY)
    {
        this.camera = camera;
        this.fovX = fovX;
        this.fovY = fovY;
    }

    override void run(EntityManager entities, EventManager events, Duration dt)
    {
        renderBackground(entities);
        renderer.clearDepth();
        renderScene(entities);
    }

    void renderBackground(EntityManager entities)
    {
        auto bgCamera = camera;
        bgCamera.position = [ 0, 0, 0 ];
        Light[] noLights = [];
        Fog noFog;
        renderer.setCamera(bgCamera.forward, bgCamera.right, bgCamera.up, bgCamera.position, fovX, fovY, 0.1, 4096);

        foreach (entity, position, model; entities.entitiesWith!(PositionComponent, BackgroundModelComponent))
        {
            if (model.model is null)
            {
                continue;
            }

            drawModel(model.model, position, noLights, noFog, &bgCamera);
        }
    }

    void renderScene(EntityManager entities)
    {
        // We use reverse depth, which means beyond the far plane things just don't depth test properly.
        // Most of the time this won't be an issue.

        // Try implementing an infinite projection matrix instead:
        // https://outerra.blogspot.com/2012/11/maximizing-depth-buffer-range-and.html
        // It's tricky when we can't examine the depth buffer.
        // Need to sit down and understand the maths for this.

        renderer.setCamera(camera.forward, camera.right, camera.up, camera.position, fovX, fovY, 0.1, 4096);

        float[3][] positions;
        Light[] lights;
        foreach (entity, position, light; entities.entitiesWith!(PositionComponent, LightingComponent))
        {
            auto i = positions.length;
            positions.length += 1;
            lights.length += 1;
            positions[i] = position.orientation.position;
            lights[i].light = light.light[0..3];
            lights[i].ambientLight = light.ambientLight[0..3];
        }

        // Get atmosphere models.
        // We also sort them for rendering them later.
        struct SortedAtmosphere
        {
            AtmosphereModelComponent* model;
            PositionComponent* position;
            float depth;
            float radiusSquared;
        }
        // Sort deepest first.
        auto atmosphereTree = new RedBlackTree!(SortedAtmosphere, "a.depth > b.depth")();
        foreach (entity, position, model; entities.entitiesWith!(PositionComponent, AtmosphereModelComponent))
        {
            float[3] relativePos = position.orientation.position[] - camera.position[];
            atmosphereTree.insert(SortedAtmosphere(
                model,
                position,
                dotProduct(relativePos, camera.forward),
                model.radius * model.radius
            ));
        }

        // Render regular models.
        foreach (entity, position, model; entities.entitiesWith!(PositionComponent, ModelComponent))
        {
            if (model.model is null)
            {
                continue;
            }

            for (auto i = 0; i < lights.length; i += 1)
            {
                lights[i].direction = positions[i][] - position.orientation.position[];
                lights[i].direction = normalize!float(lights[i].direction);
            }

            // Find whether there's an intersecting fog volume.
            Fog fog;
            foreach (atmosphere; atmosphereTree)
            {
                float[3] dir = position.orientation.position[] - atmosphere.position.orientation.position[];
                if (dotProduct(dir, dir) < atmosphere.radiusSquared)
                {
                    fog.enabled = true;
                    fog.radius = atmosphere.model.radius;
                    fog.colour = atmosphere.model.colour[0..3];
                    fog.density = atmosphere.model.density;
                    fog.position = atmosphere.position.orientation.position;
                    break;
                }
            }

            drawModel(model.model, position, lights, fog, &camera);
        }
        
        // Render atmospheres.
        Fog noFog;
        foreach (atmosphere; atmosphereTree)
        {
            auto position = atmosphere.position;
            auto model = atmosphere.model;

            for (auto i = 0; i < lights.length; i += 1)
            {
                lights[i].direction = positions[i][] - position.orientation.position[];
                lights[i].direction = normalize!float(lights[i].direction);
            }

            drawModel(model.model, position, lights, noFog, &camera);
        }
    }

    private void drawModel(
        ModelInstance model,
        PositionComponent* position,
        Light[] lights,
        Fog fog,
        Orientation!float* camera
    ) {
        auto toModel = makeCameraMatrix(position.orientation);
        auto cameraInModelSpace = translateOrientation(*camera, toModel);
        auto frustum = Frustum!float.fromOrientation(cameraInModelSpace, fovX, fovY, 0.1, 4096);
        auto meshes = model.getMeshes(cameraInModelSpace, frustum);
        foreach (mesh; meshes)
        {
            modelDrawing.drawModelMesh(mesh, position.orientation, lights, fog);
        }
    }

    pragma(inline, true) private Matrix!float makeCameraMatrix(Orientation!float orientation)
    {
        return Matrix!float.camera(
            orientation.forward,
            orientation.right,
            orientation.up,
            orientation.position,
        );
    }

    pragma(inline, true) private Orientation!float translateOrientation(Orientation!float orientation, Matrix!float matrix)
    {
        Orientation!float newOri;
        matrix.multiplyDir3(orientation.forward, newOri.forward);
        matrix.multiplyDir3(orientation.right, newOri.right);
        matrix.multiplyDir3(orientation.up, newOri.up);
        matrix.multiplyPos3(orientation.position, newOri.position);
        return newOri;
    }
}

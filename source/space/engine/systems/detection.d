module space.engine.systems.detection;

import space.engine.entity;
import space.library.geometry.orientation;
import entitysysd;

struct RadarItem
{
    enum Type
    {
        waypoint,
    }

    Type type;
    string name;
    float[3] position;
}

class DetectionSystem : System
{
    this()
    {
    }

    override void run(EntityManager entities, EventManager events, Duration dt)
    {
    }

    RadarItem[] getRadarItems(EntityManager entities, ref Orientation!float forOrientation)
    {
        RadarItem[] items;

        foreach (entity, waypoint, position; entities.entitiesWith!(WaypointComponent, PositionComponent))
        {
            items ~= RadarItem(
                RadarItem.Type.waypoint,
                waypoint.name,
                forOrientation.pointToInternalSpace(position.orientation.position),
            );
        }

        return items;
    }
}

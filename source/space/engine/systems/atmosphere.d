module space.engine.systems.atmosphere;

import space.engine.entity;
import entitysysd;

struct Atmosphere
{
    float[3] position;
    float radiusSquared;
    float radius;
    float depth;
    float density;
}

class AtmosphereSystem : System
{
    private Atmosphere[] atmospheres;

    this()
    {
        atmospheres = [];
    }

    override void run(EntityManager entities, EventManager events, Duration dt)
    {
        // Truncate the list but try to avoid reallocation.
        atmospheres.length = 0;
    }

    Atmosphere[] getAtmosphereList(EntityManager entities)
    {
        if (!atmospheres.length)
        {
            buildAtmosphereList(entities);
        }
        return atmospheres;
    }

    private void buildAtmosphereList(EntityManager entities)
    {
        foreach (entity, atmosphere, position;
            entities.entitiesWith!(AtmosphereComponent, PositionComponent))
        {
            atmospheres ~= Atmosphere(
                position.orientation.position,
                atmosphere.radius * atmosphere.radius,
                atmosphere.radius,
                atmosphere.depth,
                atmosphere.density
            );
        }
    }
}

module space.models.starfield;

import space.drawing.imagesource;
import space.library.geometry.frustum;
import space.library.geometry.orientation;
import space.library.geometry.vector;
import space.library.procedural.randomfield;
import space.models.interfaces;

class StarfieldFactory
{
    private TextureFactory textureFactory;
    private int texture;

    this(TextureFactory textureFactory)
    {
        this.textureFactory = textureFactory;

        texture = textureFactory.getTexture("#fff");
    }

    Starfield createStarfield()
    {
        return new Starfield(texture);
    }
}

class Starfield : ModelInstance
{
    private struct Star
    {
        float[3] position;
        float brightness;
    }

    private int texture;
    private Star[] stars;
    private ModelMesh[1] meshes;
    private ModelMesh* mesh;

    this(int texture)
    {
        mesh = &meshes[0];
        this.texture = texture;

        const numStars = 2048;
        RandomField field;

        stars.length = numStars;
        for (int i = 0; i < numStars; i += 1)
        {
            stars[i].position = [
                field.toFloat(field.calculate(i, 0)) - 0.5,
                field.toFloat(field.calculate(i, 1)) - 0.5,
                field.toFloat(field.calculate(i, 2)) - 0.5,
            ];
            stars[i].position = normalize!float(stars[i].position);
            stars[i].position[] *= 50;
            stars[i].brightness = field.toFloat(field.calculate(i, 3));
        }
    }

    ModelMesh[] getMeshes(Orientation!float camera, Frustum!float frustum)
    {
        const float size = 0.05;
        // 6 verts per star, but we draw half of them maximum.
        mesh.vertices.length = stars.length * 6;
        int realLength = 0;

        int j = 0;
        for (int i = 0; i < stars.length; i += 1)
        {
            if (!frustum.contains(stars[i].position, size, false))
            {
                continue;
            }

            // TL, clockwise.
            float[3] v1 = stars[i].position[] - camera.right[] * size + camera.up[] * size;
            float[3] v2 = stars[i].position[] + camera.right[] * size + camera.up[] * size;
            float[3] v3 = stars[i].position[] + camera.right[] * size - camera.up[] * size;
            float[3] v4 = stars[i].position[] - camera.right[] * size - camera.up[] * size;

            float[4] colour = [
                stars[i].brightness,
                stars[i].brightness,
                stars[i].brightness,
                1,
            ];

            buildVertex(&mesh.vertices[j++], v1, colour);
            buildVertex(&mesh.vertices[j++], v2, colour);
            buildVertex(&mesh.vertices[j++], v3, colour);
            buildVertex(&mesh.vertices[j++], v1, colour);
            buildVertex(&mesh.vertices[j++], v3, colour);
            buildVertex(&mesh.vertices[j++], v4, colour);

            realLength += 6;
        }

        mesh.vertices.length = realLength;
        mesh.fullbright = true;
        mesh.texture = texture;
        mesh.detailTexture = texture;
        return meshes;
    }

    pragma(inline, true) private void buildVertex(ModelVertex* vertex, float[3] position, float[4] colour)
    {
        vertex.position = position;
        vertex.colour = colour;
    }
}

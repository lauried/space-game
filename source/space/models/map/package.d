module space.models.map;

import space.drawing.imagesource;
import space.library.geometry.frustum;
import space.library.geometry.orientation;
import space.library.geometry.vector;
import space.models.interfaces;
import space.models.map.brush;
import space.filetypes.map;
import std.algorithm.sorting;
import std.math;
import std.stdio;

class MapModelFactory
{
    private TextureFactory textureFactory;
    private MapType mapType;
    private MapMeshBuilder mapMeshBuilder;

    this(TextureFactory textureFactory, MapType mapType, MapMeshBuilder mapMeshBuilder)
    {
        this.textureFactory = textureFactory;
        this.mapType = mapType;
        this.mapMeshBuilder = mapMeshBuilder;
    }

    MapModelInstance loadModel(string filename, float scale = 1.0f)
    {
        Map map = mapType.loadMap(filename);
        return new MapModelInstance(map, textureFactory, mapMeshBuilder, scale);
    }
}

class MapModelInstance : ModelInstance
{
    private ModelMesh[] meshes;
    private float scale;

    this(Map map, TextureFactory textureFactory, MapMeshBuilder mapMeshBuilder, float scale)
    {
        meshes = [];
        ModelMesh[string] meshMap;
        mapMeshBuilder.processMap(map, &meshMap);

        foreach (texture, mesh; meshMap)
        {
            mesh.texture = textureFactory.getTexture("data/textures/" ~ texture ~ ".png");
            scaleMesh(&mesh, 1.0f / scale);
            meshes ~= mesh;
        }
    }

    private void scaleMesh(ModelMesh* mesh, float scale)
    {
        for (int i = 0; i < mesh.vertices.length; i += 1)
        {
            mesh.vertices[i].position[] *= scale;
        }
    }

    ModelMesh[] getMeshes(Orientation!float camera, Frustum!float frustum)
    {
        return meshes;
    }
}

class MapMeshBuilder
{
    private BrushVerts brushVerts;

    this(BrushVerts brushVerts)
    {
        this.brushVerts = brushVerts;
    }

    void processMap(Map map, ModelMesh[string]* meshMap)
    {
        // TODO: Split into separate entities.
        foreach (entity; map.entities)
        {
            foreach (brush; entity.brushes)
            {
                processBrush(brush, meshMap);
            }
        }
    }

    private void processBrush(Brush brush, ModelMesh[string]* meshMap)
    {
        if (brush.surfaces.length < 1)
        {
            return;
        }

        SurfaceVert[] surfaceVerts = [];
        brushVerts.getBrushVerts(brush, surfaceVerts);

        for (int surfaceIndex = 0; surfaceIndex < brush.surfaces.length; surfaceIndex += 1)
        {
            Surface* surface = &brush.surfaces[surfaceIndex];

            // Get the verts, but put their S and T for sorting them.
            struct VertData
            {
                float[3] point;
                float[2] texcoords;
                float[2] surfaceCoords;
                float angle;
            }

            VertData[] verts = [];
            foreach (surfaceVert; surfaceVerts)
            {
                if (surfaceVert.surface == surfaceIndex)
                {
                    VertData vert;
                    vert.point = surfaceVert.point;
                    vert.texcoords = [
                        dotProduct(vert.point, surface.vs),
                        dotProduct(vert.point, surface.vt),
                    ];
                    verts ~= vert;
                }
            }

            if (!verts.length)
            {
                continue;
            }

            if (verts.length < 3)
            {
                writeln("Warning, got fewer than 3 verts on a surface");
                continue;
            }

            // Make our own vectors instead of relying on texcoords.
            float[3] v1 = verts[0].point[] - verts[1].point[];
            v1 = normalize(v1);
            float[3] v2 = normalize(crossProduct(surface.plane[0..3], v1));
            for (int i = 0; i < verts.length; i += 1)
            {
                verts[i].surfaceCoords = [
                    dotProduct(v1, verts[i].point),
                    dotProduct(v2, verts[i].point),
                ];
            }

            // Find centre S&T.
            float[2] centre = [ 0, 0 ];
            foreach (vert; verts)
            {
                centre[] += vert.surfaceCoords[];
            }
            float invNumElements = 1.0f / cast(float)verts.length;
            centre[] *= invNumElements;

            // Update each vert to instead store the angle around the centre.
            // It works because brush faces must be convex.
            for (int vertIndex = 0; vertIndex < verts.length; vertIndex += 1)
            {
                verts[vertIndex].angle = atan2(
                    verts[vertIndex].surfaceCoords[0] - centre[0],
                    verts[vertIndex].surfaceCoords[1] - centre[1],
                );
            }
            verts.sort!("a.angle > b.angle");

            // Add them to the mesh, giving them texture coordinates.
            ModelMesh* mesh = surface.texture in *meshMap;
            if (mesh is null)
            {
                (*meshMap)[surface.texture] = ModelMesh();
                mesh = surface.texture in *meshMap;
            }

            float[3] normal = surface.plane[0..3];
            float[4] colour = [ 1, 1, 1, 1 ];
            // We go 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5 etc. to make triangles.
            for (int vertIndex = 2; vertIndex < verts.length; vertIndex += 1)
            {
                mesh.vertices ~= ModelVertex(verts[0].point, normal, colour, verts[0].texcoords);
                mesh.vertices ~= ModelVertex(verts[vertIndex-1].point, normal, colour, verts[vertIndex-1].texcoords);
                mesh.vertices ~= ModelVertex(verts[vertIndex].point, normal, colour, verts[vertIndex].texcoords);
            }
        }
    }
}

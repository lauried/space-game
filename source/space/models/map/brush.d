module space.models.map.brush;

import space.filetypes.map;
import space.library.geometry.intersection;
import space.library.geometry.vector;

private const float BRUSH_PLANE_EPSILON = 1.0f / 65536.0f;

struct SurfaceVert
{
    float[3] point;
    int surface;
}

private struct SurfaceLine
{
    float[6] line;
    int surface1;
    int surface2;
}

class BrushVerts
{
    this()
    {
    }

    void getBrushVerts(ref Brush brush, ref SurfaceVert[] surfaceVerts)
    {
        SurfaceLine[] surfaceLines = [];

        for (int surfaceIndex = 1; surfaceIndex < brush.surfaces.length; surfaceIndex += 1)
        {
            Surface* surface = &brush.surfaces[surfaceIndex];

            // Cull existing verts against this plane
            for (int vertexIndex = 0; vertexIndex < surfaceVerts.length; vertexIndex += 1)
            {
                if (planeCheck(surfaceVerts[vertexIndex].point, surface.plane) > BRUSH_PLANE_EPSILON)
                {
                    surfaceVerts[vertexIndex] = surfaceVerts[surfaceVerts.length - 1];
                    surfaceVerts.length -= 1;
                    vertexIndex -=1; // re-check the one we pulled here from the end
                }
            }

            // Intersect already existing lines to create verts
            foreach (line; surfaceLines)
            {
                float[3] point;
                if (!linePlaneIntersection(line.line, surface.plane, point))
                {
                    continue;
                }

                // Cull the new verts against existing planes
                bool cullVert = false;
                for (int otherSurfaceIndex = 0; otherSurfaceIndex < surfaceIndex; otherSurfaceIndex += 1)
                {
                    if (planeCheck(point, brush.surfaces[otherSurfaceIndex].plane) > BRUSH_PLANE_EPSILON)
                    {
                        cullVert = true;
                        break;
                    }
                }

                if (cullVert)
                {
                    continue;
                }

                // Add the vert for each surface involved.
                surfaceVerts ~= SurfaceVert(point, surfaceIndex);
                surfaceVerts ~= SurfaceVert(point, line.surface1);
                surfaceVerts ~= SurfaceVert(point, line.surface2);
            }

            // Intersect each already-added plane to create lines
            for (int otherSurfaceIndex = 0; otherSurfaceIndex < surfaceIndex; otherSurfaceIndex += 1)
            {
                float[6] line = planePlaneIntersection(surface.plane, brush.surfaces[otherSurfaceIndex].plane);
                if (line[3] == 0 && line[4] == 0 && line[5] == 0)
                {
                    continue;
                }

                surfaceLines ~= SurfaceLine(line, surfaceIndex, otherSurfaceIndex);
            }
        }
    }
}

module space.models.atmosphere;

import space.models.interfaces;
import space.library.geometry.frustum;
import space.library.geometry.intersection;
import space.library.geometry.orientation;
import space.library.geometry.vector;
import std.algorithm;

class AtmosphereModel : ModelInstance
{
    static const int NUM_SUBDIVISION_LEVELS = 5;

    private ModelMesh[NUM_SUBDIVISION_LEVELS] meshes;
    private float radius;
    private float[4] colour;
    private float density;

    this(float radius, float[4] colour, float density)
    {
        this.radius = radius;
        this.colour = colour;
        this.density = density;

        makeOctahedron(&meshes[0]);
        for (int i = 1; i < NUM_SUBDIVISION_LEVELS; i += 1)
        {
            subdivideMesh(&meshes[i - 1], &meshes[i]);
        }
    }

    ModelMesh[] getMeshes(Orientation!float camera, Frustum!float frustum)
    {
        int meshNum = 2;
        float dist = length(camera.position);
        if (dist < radius * 5)
        {
            meshNum = 3;
        }
        if (dist < radius * 1.4 && dist > radius * 0.9)
        {
            meshNum = 4;
        }

        setupMesh(&meshes[meshNum], camera.position);
        return meshes[meshNum..meshNum+1];
    }

    private void setupMesh(ModelMesh* mesh, float[3] cameraPos)
    {
        float scale = 1.0f / radius;
        float[3] scaledCameraPos = cameraPos[] * scale;

        for (int i = 0; i < mesh.vertices.length; i += 1)
        {
            mesh.vertices[i].position[] = mesh.vertices[i].normal[] * radius;
            float[3] dir = mesh.vertices[i].position[] - cameraPos[];
            dir = normalize(dir);
            auto result = lineSphereIntersect(scaledCameraPos, dir);
            float depth = 0;
            if (result.numSolutions == 2)
            {
                depth = result.solutions[1] - max(0, result.solutions[0]);
                depth *= 0.5;
                depth = max(0, depth - 0.1);
            }
            mesh.vertices[i].colour[0..3] = colour[0..3];
            mesh.vertices[i].colour[3] = depth * density;
        }
    }

    private void subdivideMesh(ModelMesh* source, ModelMesh* dest)
    {
        dest.fullbright = source.fullbright;
        dest.texture = source.texture;
        dest.detailTexture = source.detailTexture;
        dest.vertices.length = source.vertices.length * 4;

        for (int i = 0; i < source.vertices.length; i += 3)
        {
            // Use triforce subdivision to turn 1 triangle into 4.
            //     A
            //     /\
            //  F /__\ D
            //   /\  /\
            //  /__\/__\
            // C   E    B
            int j = i * 4;

            float[3] a = source.vertices[i+0].position;
            float[3] b = source.vertices[i+1].position;
            float[3] c = source.vertices[i+2].position;

            float[3] d = (a[] + b[]) * 0.5;
            float[3] e = (b[] + c[]) * 0.5;
            float[3] f = (c[] + a[]) * 0.5;

            makeVertex(&dest.vertices[j+ 0], a);
            makeVertex(&dest.vertices[j+ 1], d);
            makeVertex(&dest.vertices[j+ 2], f);

            makeVertex(&dest.vertices[j+ 3], d);
            makeVertex(&dest.vertices[j+ 4], b);
            makeVertex(&dest.vertices[j+ 5], e);

            makeVertex(&dest.vertices[j+ 6], e);
            makeVertex(&dest.vertices[j+ 7], c);
            makeVertex(&dest.vertices[j+ 8], f);

            makeVertex(&dest.vertices[j+ 9], d);
            makeVertex(&dest.vertices[j+10], e);
            makeVertex(&dest.vertices[j+11], f);
        }
    }

    private void makeOctahedron(ModelMesh* mesh)
    {
        mesh.fullbright = true;
        mesh.texture = 0;
        mesh.detailTexture = 0;
        mesh.vertices.length = 24;

        // TOP looking down: CCW so that they are inward facing.
        //     +x
        //     /|\
        //    /D|A\
        //   /__|__\
        //   \ C|B / +y
        //    \ | /
        //     \|/

        // A
        makeVertex(&mesh.vertices[ 0], [  0,  0,  1 ]);
        makeVertex(&mesh.vertices[ 1], [  0,  1,  0 ]);
        makeVertex(&mesh.vertices[ 2], [  1,  0,  0 ]);
        // B
        makeVertex(&mesh.vertices[ 3], [  0,  0,  1 ]);
        makeVertex(&mesh.vertices[ 4], [ -1,  0,  0 ]);
        makeVertex(&mesh.vertices[ 5], [  0,  1,  0 ]);
        // C
        makeVertex(&mesh.vertices[ 6], [  0,  0,  1 ]);
        makeVertex(&mesh.vertices[ 7], [  0, -1,  0 ]);
        makeVertex(&mesh.vertices[ 8], [ -1,  0,  0 ]);
        // D
        makeVertex(&mesh.vertices[ 9], [  0,  0,  1 ]);
        makeVertex(&mesh.vertices[10], [  1,  0,  0 ]);
        makeVertex(&mesh.vertices[11], [  0, -1,  0 ]);

        // Bottom is the same but clockwise and -1 for z
        // A
        makeVertex(&mesh.vertices[12], [  0,  0, -1 ]);
        makeVertex(&mesh.vertices[13], [  1,  0,  0 ]);
        makeVertex(&mesh.vertices[14], [  0,  1,  0 ]);
        // B
        makeVertex(&mesh.vertices[15], [  0,  0, -1 ]);
        makeVertex(&mesh.vertices[16], [  0,  1,  0 ]);
        makeVertex(&mesh.vertices[17], [ -1,  0,  0 ]);
        // C
        makeVertex(&mesh.vertices[18], [  0,  0, -1 ]);
        makeVertex(&mesh.vertices[19], [ -1,  0,  0 ]);
        makeVertex(&mesh.vertices[20], [  0, -1,  0 ]);
        // D
        makeVertex(&mesh.vertices[21], [  0,  0, -1 ]);
        makeVertex(&mesh.vertices[22], [  0, -1,  0 ]);
        makeVertex(&mesh.vertices[23], [  1,  0,  0 ]);
    }

    private void makeVertex(ModelVertex* vert, float[3] position)
    {
        vert.normal = normalize(position);
        vert.position = normalize(position);
    }
}

module space.models.planet;

import space.drawing.imagesource;
import space.models.interfaces;
import space.models.planet.binarytriangle;
import space.models.planet.cubeterrain;
import space.library.geometry.frustum;
import space.library.geometry.intersection;
import space.library.geometry.orientation;
import space.library.geometry.vector;
import std.algorithm;
import std.conv;

import std.stdio;

class PlanetModelFactory
{
    private TextureFactory textureFactory;
    int detailTexture;

    this(TextureFactory textureFactory)
    {
        this.textureFactory = textureFactory;
        detailTexture = textureFactory.getTexture("data/textures/tileable128.png");
    }

    Planet createPlanet(float scale = 1.0f, string params = "", string textureParams = "", bool fullbright = false)
    {
        return new Planet(scale, params, textureParams, fullbright, detailTexture, textureFactory);
    }
}

class Planet : ModelInstance, ColliderInstance
{
    private static const int PLANET_MAX_SUBDIVIDE_LEVEL = 13;
    private static const int ATMOSPHERE_MAX_SUBDIVIDE_LEVEL = 12;

    private BinaryTriangleMesh planetTriangles;
    private float scale;
    private float atmosphereDepth;
    private float boundingRadius;
    private ModelMesh[6] planetMesh;
    private float[3] lastBuildOrigin = [ 0, 0, 0 ];

    this(float scale, string params, string textureParams, bool fullbright, int detailTexture, TextureFactory textureFactory)
    {
        this.scale = scale;

        planetTriangles = new BinaryTriangleMesh(params, textureParams);

        for (int i = 0; i < 6; i += 1)
        {
            planetMesh[i].detailTexture = detailTexture;
            planetMesh[i].fullbright = fullbright;

            // Have to wrap the variable otherwise it gets overwritten.
            auto del = delegate() {
                CubeSide side = cast(CubeSide)i;
                return delegate() { return planetTriangles.getTexture(side); };
            }();

            string texName = "planet:" ~ params ~ ":" ~ to!string(i);
            planetMesh[i].texture = textureFactory.getTexture(texName, del);
        }

        boundingRadius = (planetTriangles.maxTerrainHeight() + 1) * scale;
    }

    float getBoundingRadius()
    {
        return boundingRadius;
    }

    float[3] collideSphere(float[3] origin, float radius)
    {
        auto tri = planetTriangles.getTriangleUnderPoint(origin, PLANET_MAX_SUBDIVIDE_LEVEL);
        float[3] a = tri.t.v[] * (1 + tri.t.h) * scale;
        float[3] b = tri.r.v[] * (1 + tri.r.h) * scale;
        float[3] c = tri.l.v[] * (1 + tri.l.h) * scale;
        float[4] plane = planeForPoints(a, b, c);

        float check = planeCheck(origin, plane) - radius;
        if (check < 0)
        {
            float[3] dir = plane[0..3];
            dir[] *= -check;
            return dir;
        }

        return [ 0, 0, 0 ];
    }

    ModelMesh[] getMeshes(Orientation!float camera, Frustum!float frustum)
    {
        float viewDist = length(camera.position);
        float invScale = 1.0f / scale;
        camera.position[] *= invScale;
        frustum.position[] *= invScale;

        if (checkForRebuild(camera.position))
        {
            rebuildMeshes(&planetTriangles, frustum, PLANET_MAX_SUBDIVIDE_LEVEL, scale);
        }

        return planetMesh;
    }

    private bool checkForRebuild(float[3] position)
    {
        float[3] moved = position[] - lastBuildOrigin[];
        float dist = length(moved);
        float range = max(1, length(position));

        if (dist / range > 0.01)
        {
            lastBuildOrigin[] = position[];
            return true;
        }

        return false;
    }

    private void rebuildMeshes(
        BinaryTriangleMesh* binaryTriangles,
        Frustum!float frustum,
        int subdivideLevel,
        float radius
    ) {
        binaryTriangles.subdivideForView(frustum, subdivideLevel);
        frustum.position[] *= radius;

        int[6] reserve = [ 0, 0, 0, 0, 0, 0 ];
        foreach (triangle; binaryTriangles.mesh)
        {
            reserve[triangle.s] += 3;
        }
        for (int i = 0; i < 6; i += 1)
        {
            planetMesh[i].vertices.length = reserve[i];
        }

        int[6] shorten = [ 0, 0, 0, 0, 0, 0 ];
        int[6] pos = [ 0, 0, 0, 0, 0, 0 ];

        foreach (triangle; binaryTriangles.mesh)
        {
            CubeSide side = triangle.s;

            if (!triangle.show)
            {
                shorten[side] += 3;
                continue;
            }

            ModelMesh* mesh = &planetMesh[side];

            const float[3] t = getRealPosition(&triangle.t, radius);
            const float[3] l = getRealPosition(&triangle.l, radius);
            const float[3] r = getRealPosition(&triangle.r, radius);
            const float[3] ab = r[] - t[];
            const float[3] ac = l[] - t[];
            const float[3] normal = normalize(crossProduct!float(ab, ac));
            const float[3] dir = t[] - frustum.position[];
            if (dotProduct!float(normalize(dir), normal) > 0.707)
            {
                shorten[side] += 3;
                continue;
            }

            int j = pos[side];
            buildVertex(&mesh.vertices[j+0], t, normal, triangle.t.t);
            buildVertex(&mesh.vertices[j+1], r, normal, triangle.r.t);
            buildVertex(&mesh.vertices[j+2], l, normal, triangle.l.t);
            pos[side] = j + 3;
        }

        for (int i = 0; i < 6; i += 1)
        {
            planetMesh[i].vertices.length -= shorten[i];
        }
    }

    pragma(inline, true) private float[3] getRealPosition(BinaryTriangle.Point* p, float radius)
    {
        float[3] v = p.v[] * radius * (1 + p.h);
        return v;
    }

    pragma(inline, true) private void buildVertex(ModelVertex* mv, float[3] position, float[3] normal, float[2] texcoord)
    {
        mv.position = position;
        mv.normal = normal;
        mv.texcoord[] = texcoord[];
        mv.colour = [1, 1, 1, 1];
    }
}

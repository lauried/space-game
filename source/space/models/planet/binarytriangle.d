module space.models.planet.binarytriangle;

import space.models.planet.cubeterrain;
import space.models.planet.cubetextures;
import space.library.geometry.frustum;
import space.library.geometry.vector;
import space.renderer.interfaces;
import std.math;
import std.algorithm;

/**
 *           vt
 *           /\
 *    nl   /    \   nr
 *       /        \
 *     /____________\
 *   vl      nb      vr
 *
 * Top corner, vt, is meant to be a right angle when it's all on a flat surface.
 * Binary triangle meshes subdivide a grid by splitting themselves in two.
 * They can do this seamlessly without there being any triangles in a
 * half-subdivided state where detail levels change.
 */
struct BinaryTriangle
{
    struct Point
    {
        // Vertex
        float[3] v;
        // Texcoords
        float[2] t;
        // Terrain height.
        float h;
    }

    // Subdivision level.
    int sl;
    // Verts, texcoords, etc: top, left, right.
    // t is opposite nb, l is opposite nr, r is opposite nl.
    Point t, l, r;
    // Edge neighbour indices: base, left, right.
    int nb, nl, nr;
    // Face normal.
    float[3] normal;
    // Side
    CubeSide s;

    bool show;

    void setNormal()
    {
        float[3] ab = r.v[] - t.v[];
        float[3] ac = l.v[] - t.v[];
        normal = normalize(crossProduct!float(ab, ac));
    }

    // The purpose is to be able to use pointer syntax, so we can write:
    //   if (tri.pnb(mesh).pnb(mesh) === tri)
    // instead of:
    //   if (mesh[mesh[index].nb].nb === index)
    pragma(inline, true) BinaryTriangle* pnb(BinaryTriangle[] mesh)
    {
        return &mesh[nb];
    }

    pragma(inline, true) BinaryTriangle* pnl(BinaryTriangle[] mesh)
    {
        return &mesh[nl];
    }

    pragma(inline, true) BinaryTriangle* pnr(BinaryTriangle[] mesh)
    {
        return &mesh[nr];
    }

    pragma(inline, true) static BinaryTriangle* pt(BinaryTriangle[] mesh, int triangle)
    {
        return &mesh[triangle];
    }
}

class BinaryTriangleMesh
{
    private static BinaryTriangle[] cubeMesh = [];
    private CubeTerrain cubeTerrain;
    private CubeTextures cubeTextures;
    BinaryTriangle[] mesh;

    this(string params, string textureParams)
    {
        cubeTerrain = new CubeTerrain(params);
        cubeTextures = new CubeTextures(cubeTerrain, textureParams);
    }

    ImageData getTexture(CubeSide side)
    {
        return cubeTextures.getTexture(side);
    } 

    private void initCubeMesh()
    {
        if (cubeMesh.length)
        {
            return;
        }

        const int n = -1;
        const int o = 0;
        const int i = 1;

        const CubeSide px = CubeSide.px;
        const CubeSide py = CubeSide.py;
        const CubeSide pz = CubeSide.pz;
        const CubeSide nx = CubeSide.nx;
        const CubeSide ny = CubeSide.ny;
        const CubeSide nz = CubeSide.nz;

        BinaryTriangle[] newMesh = [
            { s:pz, sl:0, t:{v:[n,n,i], t:[o,o]}, l:{v:[n,i,i], t:[o,i]}, r:{v:[i,n,i], t:[i,o]}, nb: 1, nl: 6, nr: 8 }, // u1 = 0
            { s:pz, sl:0, t:{v:[i,i,i], t:[i,i]}, l:{v:[i,n,i], t:[i,o]}, r:{v:[n,i,i], t:[o,i]}, nb: 0, nl: 2, nr: 4 }, // u2 = 1

            { s:px, sl:0, t:{v:[i,i,i], t:[i,i]}, l:{v:[i,i,n], t:[i,o]}, r:{v:[i,n,i], t:[o,i]}, nb: 3, nl: 4, nr: 1 }, // f1 = 2
            { s:px, sl:0, t:{v:[i,n,n], t:[o,o]}, l:{v:[i,n,i], t:[o,i]}, r:{v:[i,i,n], t:[i,o]}, nb: 2, nl: 8, nr:11 }, // f2 = 3

            { s:py, sl:0, t:{v:[i,i,i], t:[i,i]}, l:{v:[n,i,i], t:[o,i]}, r:{v:[i,i,n], t:[i,o]}, nb: 5, nl: 1, nr: 2 }, // r1 = 4
            { s:py, sl:0, t:{v:[n,i,n], t:[o,o]}, l:{v:[i,i,n], t:[i,o]}, r:{v:[n,i,i], t:[o,i]}, nb: 4, nl:11, nr: 6 }, // r2 = 5

            { s:nx, sl:0, t:{v:[n,i,i], t:[i,i]}, l:{v:[n,n,i], t:[o,i]}, r:{v:[n,i,n], t:[i,o]}, nb: 7, nl: 0, nr: 5 }, // b1 = 6
            { s:nx, sl:0, t:{v:[n,n,n], t:[o,o]}, l:{v:[n,i,n], t:[i,o]}, r:{v:[n,n,i], t:[o,i]}, nb: 6, nl:10, nr: 9 }, // b2 = 7

            { s:ny, sl:0, t:{v:[i,n,i], t:[i,i]}, l:{v:[i,n,n], t:[i,o]}, r:{v:[n,n,i], t:[o,i]}, nb: 9, nl: 3, nr: 0 }, // l1 = 8
            { s:ny, sl:0, t:{v:[n,n,n], t:[o,o]}, l:{v:[n,n,i], t:[o,i]}, r:{v:[i,n,n], t:[i,o]}, nb: 8, nl: 7, nr:10 }, // l2 = 9

            { s:nz, sl:0, t:{v:[n,n,n], t:[o,o]}, l:{v:[i,n,n], t:[i,o]}, r:{v:[n,i,n], t:[o,i]}, nb:11, nl: 9, nr: 7 }, // d1 = 10
            { s:nz, sl:0, t:{v:[i,i,n], t:[i,i]}, l:{v:[n,i,n], t:[o,i]}, r:{v:[i,n,n], t:[i,o]}, nb:10, nl: 5, nr: 3 }, // d2 = 11
        ];

        for (auto index = 0; index < newMesh.length; index += 1)
        {
            initTriangle(&newMesh[index]);
        }

        cubeMesh = newMesh;
    }

    pragma(inline, true) private void initTriangle(BinaryTriangle *tri)
    {
        tri.t.v = normalize!float(tri.t.v);
        tri.l.v = normalize!float(tri.l.v);
        tri.r.v = normalize!float(tri.r.v);
        tri.setNormal();
        tri.t.h = terrainHeight(tri.t.t, tri.s);
        tri.l.h = terrainHeight(tri.l.t, tri.s);
        tri.r.h = terrainHeight(tri.r.t, tri.s);
        tri.show = true;
    }

    float maxTerrainHeight()
    {
        return cubeTerrain.maxTerrainHeight();
    }

    /**
     * Subdivides down the triangle under the point to give us the maximally
     * subdivided triangle under the given point.
     * (Or over, if the point is below the surface).
     */
    BinaryTriangle getTriangleUnderPoint(float[3] point, int maxLevel)
    {
        initCubeMesh();

        BinaryTriangle tri = cubeMesh[findTriangleContainingPoint(cubeMesh, point)];

        for (int i = 0; i < maxLevel; i += 1)
        {
            BinaryTriangle.Point mid = getMidpoint(tri);
            if (pointBehindEdge(point, tri.t.v, mid.v))
            {
                // Left
                tri.r = tri.l;
                tri.l = tri.t;
                tri.t = mid;
            }
            else
            {
                // Right
                tri.l = tri.r;
                tri.r = tri.t;
                tri.t = mid;
            }
            tri.sl += 1;
        }

        tri.setNormal();
        return tri;
    }

    /**
     * Picks the first triangle out of a given existing mesh that contains the given point.
     */
    int findTriangleContainingPoint(BinaryTriangle[] tris, float[3] point)
    {
        for (int i = 0; i < cubeMesh.length; i += 1)
        {
            if (pointInsideTriangle(point, cubeMesh[i].t.v, cubeMesh[i].r.v, cubeMesh[i].l.v))
            {
                return i;
            }
        }
        return 0;
    }

    /**
     * Returns true if a line from origin to P to infinity passes through the triangle ABC.
     */
    bool pointInsideTriangle(float[3] p, float[3] a, float[3] b, float[3] c)
    {
        return pointBehindEdge(p, a, b)
            && pointBehindEdge(p, b, c)
            && pointBehindEdge(p, c, a);
    }

    bool pointBehindEdge(float[3] p, float[3] a, float[3] b)
    {
        return planeCheck!float(p, planeForPoints!float(a, [ 0, 0, 0 ], b)) <= 0;
    }

    /**
     * @todo Frustum should be in model space.
     * @todo Deal with the scale here.
     * maxLevel also helps as a safety guard in case some edge-case makes us
     * keep splitting the same triangle.
     */
    void subdivideForView(Frustum!float frustum, int maxLevel)
    {
        initCubeMesh();
        mesh = cubeMesh.dup;

        float distance = length!float(frustum.position);
        bool outside = distance > 1;

        float[4] horizonPlane;
        horizonPlane[0..3] = normalize!float(frustum.position);
        horizonPlane[3] = 1 / distance;

        // Adjust for terrain height.
        horizonPlane[3] = cos(atan(cubeTerrain.maxTerrainHeight()) + acos(horizonPlane[3]));

        for (auto triangle = 0; triangle < mesh.length; triangle += 1)
        {
            auto tri = BinaryTriangle.pt(mesh, triangle);

            if (tri.sl >= maxLevel)
            {
                continue;
            }

            float bulge = getTriangleBulge(tri) + cubeTerrain.maxTerrainHeight();

            // We're not just checking the horizon but within the circle too.
            int pointsAboveHorizon = triangleIntersectsHorizon(tri, horizonPlane, bulge);
            if (outside && tri.sl && !pointsAboveHorizon)
            {
                continue;
            }

            // if (outside && tri.sl && !triangleInsideFrustum(tri, frustum, bulge))
            // {
            //     continue;
            // }

            // If it's on the horizon we do a distance based scale.
            // This keeps the surface of the planet pretty smooth.
            const float THRESH = 0.1;
            float threshold = (pointsAboveHorizon < 3) ? min(THRESH, THRESH / distance) : THRESH;
            if (outside && tri.sl && !triangleSizeMeetsThreshold(tri, horizonPlane[0..3], distance, threshold))
            {
                continue;
            }

            splitTriangle(triangle);
            // Make us re-do the same triangle until it's subdivided enough for us.
            triangle -= 1;
        }
    }

    private bool triangleInsideFrustum(BinaryTriangle* tri, Frustum!float frustum, float bulge)
    {
        for (auto face = 0; face < 4; face += 1)
        {
            if (dotProduct!float(tri.t.v, frustum.faces[face][0..3]) + bulge > frustum.faces[face][3])
            {
                return true;
            }
            if (dotProduct!float(tri.r.v, frustum.faces[face][0..3]) + bulge > frustum.faces[face][3])
            {
                return true;
            }
            if (dotProduct!float(tri.l.v, frustum.faces[face][0..3]) + bulge > frustum.faces[face][3])
            {
                return true;
            }
        }
        return false;
    }

    private bool triangleSizeMeetsThreshold(BinaryTriangle* tri, float[3] viewDir, float distance, float threshold)
    {
        float[3] viewOrg = viewDir[] * distance;

        float[3] dirT = tri.t.v[] - viewOrg[];
        float[3] dirR = tri.r.v[] - viewOrg[];
        float[3] dirL = tri.l.v[] - viewOrg[];

        // Get closest distance to compare by.
        float distT = length!float(dirT);
        float distR = length!float(dirR);
        float distL = length!float(dirL);
        float bestDist = distT < distR && distT < distL ? distT : distR < distL ? distR : distL;

        // Doesn't matter which direction viewDir points.
        float[3] flatT = tri.t.v[] - dotProduct!float(tri.t.v, viewDir) * viewDir[];
        float[3] flatR = tri.r.v[] - dotProduct!float(tri.r.v, viewDir) * viewDir[];
        float[3] flatL = tri.l.v[] - dotProduct!float(tri.l.v, viewDir) * viewDir[];

        // Get average length.
        float[3] s1 = flatT[] - flatR[];
        float[3] s2 = flatR[] - flatL[];
        float[3] s3 = flatL[] - flatR[];
        float[3] lengths = [
            length!float(s1),
            length!float(s2),
            length!float(s3),
        ];
        float max = max(lengths[2], max(lengths[0], lengths[1]));

        return (max / bestDist) > threshold;
    }

    /**
     * Number of triangle points that are above the horizon line.
     */
    private int triangleIntersectsHorizon(BinaryTriangle *tri, float[4] horizonPlane, float bulge)
    {
        float[3] perpendicular;

        float[3] dots = [
            dotProduct!float(tri.t.v, horizonPlane[0..3]),
            dotProduct!float(tri.r.v, horizonPlane[0..3]),
            dotProduct!float(tri.l.v, horizonPlane[0..3]),
        ];

        float thresh = horizonPlane[3] - bulge;

        int count = 0;
        foreach (dot; dots)
        {
            if (dot >= thresh)
            {
                count += 1;
            }
        }

        // If any point is in front of the horizon plane, then the triangle
        // definitely intersects the horizon circle.
        // If count is 1 or 2, then the triangle intersects the horizon.
        return count;
    }

    pragma(inline, true) private float getTriangleBulge(BinaryTriangle *tri)
    {
        // Calculate the bulge in the middle of the triangle.
        // Having non-spherical triangles will mess up this calculation,
        // but we'll see if it actually affects things.
        return 1 - dotProduct(tri.t.v, tri.normal);
    }

    private void splitTriangle(int triangle)
    {
        auto tri = BinaryTriangle.pt(mesh, triangle);
        if (tri.pnb(mesh).nb != triangle)
        {
            splitTriangle(tri.nb);
        }

        splitPair(triangle);
    }

    /**
     * Gets us the midpoint texcoord for a triangle.
     */
    private float[2] getMidpointTexcoord(BinaryTriangle a)
    {
        float[2] t = (a.l.t[] + a.r.t[]) * 0.5;
        return t;
    }

    /**
     * Gets us the midpoint for the triangle.
     */
    private BinaryTriangle.Point getMidpoint(BinaryTriangle a)
    {
        BinaryTriangle.Point mid;
        mid.v[] = (a.l.v[] + a.r.v[]) * 0.5;
        mid.v = normalize!float(mid.v);
        mid.t[] = getMidpointTexcoord(a);
        mid.h = terrainHeight(mid.t, a.s);
        return mid;
    }

    /**
     * Splits a triangle and its base neighbour.
     */
    private void splitPair(int triangle)
    {
        // Take copies so that neigbour setup is safe.
        const BinaryTriangle a = *(BinaryTriangle.pt(mesh, triangle));

        int ia1 = triangle, ia = triangle;
        int ib1 = a.nb, ib = a.nb;

        int newLevel = a.sl + 1;
        // Split a, b into a1, a2, b1, b2.
        BinaryTriangle.Point mid = getMidpoint(a);

        // Add two extra triangles.
        int ia2 = cast(int) mesh.length;
        int ib2 = -1;
        if (ib > -1)
        {
            ib2 = cast(int) mesh.length + 1;
            mesh.length += 2;
        }
        else
        {
            mesh.length += 1;
        }

        // Care must be taken not to use pointers until we've resized the mesh.
        auto a1 = BinaryTriangle.pt(mesh, ia1);
        auto a2 = BinaryTriangle.pt(mesh, ia2);

        a1.show = true;
        a2.show = true;

        a1.s = a.s;
        a2.s = a.s;

        a1.sl = newLevel;
        a2.sl = newLevel;

        a1.t = mid;
        a1.r = a.l;
        a1.l = a.t;

        a2.t = mid;
        a2.l = a.r;
        a2.r = a.t;

        replaceNeighbour(a.nl, ia, ia1);
        replaceNeighbour(a.nr, ia, ia2);

        a1.nl = ia2; a1.nr = ib2;
        a2.nl = ib1; a2.nr = ia1;

        a1.nb = a.nl;
        a2.nb = a.nr;

        a1.setNormal();
        a2.setNormal();

        if (ib > -1)
        {
            const BinaryTriangle b = *(BinaryTriangle.pt(mesh, a.nb));
            // B has to use its own texcoords cause they're on another plane.
            mid.t = getMidpointTexcoord(b);

            auto b1 = BinaryTriangle.pt(mesh, ib1);
            auto b2 = BinaryTriangle.pt(mesh, ib2);

            b1.show = true;
            b2.show = true;

            b1.sl = newLevel;
            b2.sl = newLevel;

            b1.s = b.s;
            b2.s = b.s;

            b1.t = mid;
            b1.r = b.l;
            b1.l = b.t;

            b2.t = mid;
            b2.l = b.r;
            b2.r = b.t;

            replaceNeighbour(b.nl, ib, ib1);
            replaceNeighbour(b.nr, ib, ib2);

            b1.nl = ib2; b1.nr = ia2;
            b2.nl = ia1; b2.nr = ib1;

            b1.nb = b.nl;
            b2.nb = b.nr;

            b1.setNormal();
            b2.setNormal();
        }
    }

    private void replaceNeighbour(int target, int from, int to)
    {
        auto tri = BinaryTriangle.pt(mesh, target);
        if (tri.nb == from)
        {
            tri.nb = to;
        }
        else if (tri.nl == from)
        {
            tri.nl = to;
        }
        else if (tri.nr == from)
        {
            tri.nr = to;
        }
    }

    pragma(inline, true) private float terrainHeight(float[2] texcoords, CubeSide side)
    {
        return cubeTerrain.terrainHeight(texcoords, side);
    }
}

unittest
{
    BinaryTriangleMesh bt = new BinaryTriangleMesh("", "");
    assert(bt.pointInsideTriangle([ 0.25, 0.25, 1 ], [ 0, 0, 1 ], [ 1, 0, 1 ], [ 0, 1, 1 ]), "Expected point inside triangle");
    assert(!bt.pointInsideTriangle([ 0.51, 0.51, 1 ], [ 0, 0, 1 ], [ 1, 0, 1 ], [ 0, 1, 1 ]), "Expected point outside triangle");
}
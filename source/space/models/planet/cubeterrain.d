module space.models.planet.cubeterrain;

import space.library.containers.stack;
import space.library.procedural.randomfield;
import space.models.planet.heightmap;
import space.models.planet.heightmapinstruction;
import std.algorithm;
import std.math;

enum CubeSide : int
{
    px = 0,
    nx = 1,
    py = 2,
    ny = 3,
    pz = 4,
    nz = 5,
}

/**
 * Represent terrain mapped to a cube.
 * We sample from a 3D noise field so that the edges and corners of different
 * cube faces are actually sampling the same value.
 */
class CubeTerrain
{
    HeightmapInstruction[] instructions;
    RandomField field;
    Heightmap[6] heightmaps;

    this(string data)
    {
        const int size = 129;
        instructions = HeightmapInstruction.fromString(data);

        for (int i = 0; i < 6; i += 1)
        {
            heightmaps[i].width = heightmaps[i].height = size;
            heightmaps[i].data.length = size * size;
            buildHeightmap(cast(CubeSide)i);
        }
    }

    float maxTerrainHeight()
    {
        Stack!float stack;
        float height =  0;
        foreach (instruction; instructions)
        {
            switch (instruction.type)
            {
                case HeightmapInstruction.Type.Noise:
                    height += instruction.param2;
                    break;

                case HeightmapInstruction.Type.NoiseRange:
                    height += instruction.param3 * (floor(instruction.param2) - floor(instruction.param1));
                    break;

                case HeightmapInstruction.Type.Limit:
                    height = min(max(instruction.param1, height), instruction.param2) - instruction.param1;
                    break;

                case HeightmapInstruction.Type.Scale:
                    height *= instruction.param1;
                    break;

                case HeightmapInstruction.Type.Push:
                    stack.push(height);
                    height = 0;
                    break;

                case HeightmapInstruction.Type.PopMul:
                    height *= stack.pop();
                    break;

                case HeightmapInstruction.Type.PopAdd:
                    height += stack.pop();
                    break;

                default:
                    break;
            }
        }

        return height;
    }

    /**
     * Takes 'texcoords', i.e. floating point coordinates from 0 to 1 inclusive,
     * on the surface of the cube.
     * Returns terrain height above 'sea' level, e.g. the base radius of the planet.
     */
    float terrainHeight(float[2] texcoords, CubeSide side)
    {
        int[2] size = [
            heightmaps[side].width,
            heightmaps[side].height,
        ];

        float[2] frac = [
            texcoords[0] * (size[0] - 1),
            texcoords[1] * (size[1] - 1),
        ];
        float[2] coords = [ floor(frac[0]), floor(frac[1]) ];
        frac[] -= coords[];

        float[2] ifrac = 1.0f - frac[];

        int[2] mins = [
            cast(int)coords[0],
            cast(int)coords[1],
        ];
        // Let them wrap round.
        // We only get to wrapping when a texcoord is 1.0f, and for the cube
        // the fraction is always 0.0f.
        int[2] maxs = (mins[] + 1) % size[];

        // Lerp horizontally first.
        float[2] values = [
            heightmaps[side].data[mins[1] * size[0] + mins[0]] * ifrac[0] + heightmaps[side].data[mins[1] * size[0] + maxs[0]] * frac[0],
            heightmaps[side].data[maxs[1] * size[0] + mins[0]] * ifrac[0] + heightmaps[side].data[maxs[1] * size[0] + maxs[0]] * frac[0],
        ];

        // Lerp vertically.
        return values[0] * ifrac[1] + values[1] * frac[1];
    }

    Heightmap getHeightmap(CubeSide side)
    {
        return heightmaps[side];
    }

    private void buildHeightmap(CubeSide side)
    {
        struct HeightState
        {
            float[] data;
            float maxHeight;
        }

        Stack!HeightState stack;

        int texSize = heightmaps[side].width;
        int dataSize = heightmaps[side].width * heightmaps[side].height;

        heightmaps[side].data[] = 0;
        float maxHeight = 0;
        foreach (instruction; instructions)
        {
            switch (instruction.type)
            {
                case HeightmapInstruction.Type.Noise:
                    blendNoise(1 << cast(int)(instruction.param1 - 1), instruction.param2, side);
                    maxHeight += instruction.param2;
                    break;

                case HeightmapInstruction.Type.NoiseRange:
                    int min = cast(int)instruction.param1;
                    int max = cast(int)instruction.param2;

                    // Calculate scaling factor so we can normalize the sum of the range.
                    float powScaleCorrection = 0;
                    float powScaleValue = 1;
                    for (int fl = max; fl > min; fl -= 1)
                    {
                        powScaleValue *= 0.5;
                        powScaleCorrection += powScaleValue;
                    }
                    powScaleCorrection = 1.0f / powScaleCorrection;

                    float powScale = 1;
                    for (int fl = max; fl > min; fl -= 1)
                    {
                        powScale *= 0.5;
                        blendNoise(1 << (fl - 1), instruction.param3 * powScale * powScaleCorrection, side);
                    }
                    maxHeight += instruction.param3;
                    break;

                case HeightmapInstruction.Type.Limit:
                    for (int i = 0; i < dataSize; i += 1)
                    {
                        float height = heightmaps[side].data[i];
                        heightmaps[side].data[i] = min(max(instruction.param1, height), instruction.param2) - instruction.param1;
                    }
                    maxHeight = instruction.param2 - instruction.param1;
                    break;

                case HeightmapInstruction.Type.Scale:
                    heightmaps[side].data[] *= instruction.param1;
                    maxHeight *= instruction.param1;
                    break;

                case HeightmapInstruction.Type.Curve:
                    for (int i = 0; i < dataSize; i += 1)
                    {
                        float height = heightmaps[side].data[i];
                        height = (height / maxHeight) * 2 - 1;
                        height = pow(abs(height), instruction.param1) * sgn(height);
                        height = (height + 1) * maxHeight * 0.5;
                        heightmaps[side].data[i] = height;
                    }
                    break;

                case HeightmapInstruction.Type.Push:
                    stack.push(HeightState(heightmaps[side].data, maxHeight));
                    heightmaps[side].data = [];
                    heightmaps[side].data.length = dataSize;
                    heightmaps[side].data[] = 0;
                    maxHeight = 0;
                    break;

                case HeightmapInstruction.Type.PopMul:
                    auto top = stack.pop();
                    heightmaps[side].data[] *= top.data[];
                    maxHeight *= top.maxHeight;
                    break;

                case HeightmapInstruction.Type.PopAdd:
                    auto top = stack.pop();
                    heightmaps[side].data[] += top.data[];
                    maxHeight += top.maxHeight;
                    break;

                default:
                    break;
            }
        }
    }

    /**
     * Blend noise onto the cubeside at the given frequency.
     * @param noiseScale The scale from the noise to the final image.
     * @param heightScale How high the noise will be. It will range in height from 0 to heightScale.
     * @param side The side of the cube we're generating.
     */
    private void blendNoise(int noiseScale, float heightScale, CubeSide side)
    {
        Heightmap noise;
        noise.width = ((heightmaps[side].width - 1) / noiseScale) + 1;
        noise.height = ((heightmaps[side].height - 1) / noiseScale) + 1;
        noise.data.length = noise.width * noise.height;

        for (int noiseY = 0; noiseY < noise.height; noiseY += 1)
        {
            for (int noiseX = 0; noiseX < noise.width; noiseX += 1)
            {
                int[3] noiseFieldCoords = swizzleTexcoords(noiseX, noiseY, side, heightmaps[side].width);
                noise.data[noiseY * noise.width + noiseX] = getNoise(noiseFieldCoords, noiseScale) * heightScale;
            }
        }

        interpolateAndAdd(noise, heightmaps[side], noiseScale);
    }

    pragma(inline, true) private float getNoise(int[3] coords, int level)
    {
        return field.toFloat(field.calculate(level, coords[0], coords[1], coords[2]));
    }

    /**
     * Our texcoords have to be set up to support this in the mesh.
     * Takes texcoords and side, and returns coordinates on the surface of a
     * cube with one corner at 0,0,0.
     * The side length of the cube is scale.
     * s and t must range from 0 to scale.
     * This is a constant scale though.
     */
    pragma(inline, true) T[3] swizzleTexcoords(T)(T s, T t, CubeSide side, T scale)
    {
        switch (side)
        {
            case CubeSide.px:
                return [ scale, s, t ];
            case CubeSide.nx:
                return [ 0, s, t ];

            case CubeSide.py:
                return [ s, scale, t ];
            case CubeSide.ny:
                return [ s, 0, t ];

            case CubeSide.pz:
                return [ s, t, scale ];
            case CubeSide.nz:
                return [ s, t, 0 ];
            default:
                return [ 0, 0, 0 ];
        }
    }
}

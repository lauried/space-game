module space.models.planet.heightmap;

debug
{
    import std.conv;
}

struct Heightmap
{
    float[] data;
    int width;
    int height;
}

void interpolateAndAdd(Heightmap source, ref Heightmap dest, int scale)
{
    debug
    {
        assert(
            ((source.width - 1) * scale) + 1 == dest.width,
            "Invalid source-dest width relationship: scale " ~ to!string(scale)
            ~ " src " ~ to!string(source.width) ~ " dest " ~ to!string(dest.width)
        );
        assert(
            ((source.height - 1) * scale) + 1 == dest.height,
            "Invalid source-dest height relationship: scale " ~ to!string(scale)
            ~ " src " ~ to!string(source.height) ~ " dest " ~ to!string(dest.height)
        );
    }

    // Buffer for two rows of just the source interpolated horizontally.
    float[][2] buffers;
    buffers[0].length = dest.width;
    buffers[1].length = dest.width;

    float step = 1.0f / cast(float)scale;

    // Fill the first buffer.
    lerpRow(&source.data[0], &buffers[0][0], source.width, scale, step);

    int sourceEnd = source.height - 1;
    for (int srcY = 0; srcY < sourceEnd; srcY += 1)
    {
        // This makes it so we swap buffers each row of soure.
        int buffer0 = srcY & 1;
        int buffer1 = (buffer0 + 1) & 1;

        // Fill the second buffer.
        // We pre-filled the very first, and from then on swap this to be the first afterward.
        lerpRow(&source.data[(srcY + 1) * source.width], &buffers[buffer1][0], source.width, scale, step);

        // Lerp vertically between the two buffers.
        // We don't go right up to the source row because that happens next iteration of srcY.
        int destYend = (srcY + 1) * scale;
        float frac = 0;
        for (int destY = srcY * scale; destY < destYend; destY += 1, frac += step)
        {
            float ifrac = 1 - frac;
            for (int destX = 0; destX < dest.width; destX += 1)
            {
                dest.data[destY * dest.width + destX] += buffers[buffer0][destX] * ifrac + buffers[buffer1][destX] * frac;
            }
        }
    }

    // Do the last source row.
    int destY = dest.height - 1;
    int bufferLast = sourceEnd & 1;
    for (int destX = 0; destX < dest.width; destX += 1)
    {
        dest.data[destY * dest.width + destX] += buffers[bufferLast][destX];
    }
}

private void lerpRow(float* source, float* dest, int sourceWidth, int scale, float step)
{
    int sourceEnd = sourceWidth - 1;
    for (int srcX = 0; srcX < sourceEnd; srcX += 1)
    {
        int destEnd = (srcX + 1) * scale;
        float frac = 0;
        for (int destX = srcX * scale; destX < destEnd; destX += 1, frac += step)
        {
            dest[destX] = source[srcX] * (1 - frac) + source[srcX + 1] * frac;
        }
    }

    dest[sourceEnd * scale] = source[sourceEnd];
}

unittest
{
    import std.conv;

    Heightmap source;
    source.width = 3;
    source.height = 3;
    source.data = [
        0.0f, 1.0f, 2.0f,
        3.0f, 4.0f, 5.0f,
        6.0f, 7.0f, 8.0f,
    ];

    Heightmap dest;
    dest.width = 5;
    dest.height = 5;
    dest.data = [
        1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    ];

    interpolateAndAdd(source, dest, 2);

    float[] expectedData = [
        1.0f, 1.5f, 2.0f, 2.5f, 3.0f,
        2.5f, 3.0f, 3.5f, 4.0f, 4.5f,
        4.0f, 4.5f, 5.0f, 5.5f, 6.0f,
        5.5f, 6.0f, 6.5f, 7.0f, 7.5f,
        7.0f, 7.5f, 8.0f, 8.5f, 9.0f,
    ];
    
    string failMessage = "Destination data incorrect: expected " ~ to!string(expectedData) ~ " got " ~ to!string(dest.data);
    for (int i = 0; i < expectedData.length; i += 1)
    {
        assert(expectedData[i] == dest.data[i], failMessage);
    }
}

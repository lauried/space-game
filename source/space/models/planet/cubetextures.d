module space.models.planet.cubetextures;

import space.library.colour;
import space.library.text.quicktokenizer;
import space.models.planet.cubeterrain;
import space.models.planet.heightmap;
import space.renderer.interfaces;
import std.algorithm;
import std.conv;

class CubeTextures
{
    private CubeTerrain terrain;
    private ImageData[6] textures;

    private float[4] colourLow = [ 0.5, 0.3, 0.2, 1.0 ];
    private float[4] colourHigh = [ 0.9, 0.9, 0.9, 1.0 ];
    private float[4] colourSteep = [ 0.3, 0.3, 0.1, 1.0 ];
    private float levelLow = 0.1;
    private float levelHigh = 0.9;
    private float levelSteep = 0.5;

    this(CubeTerrain terrain, string params = "")
    {
        this.terrain = terrain;
        setupColours(params);
        makeTextures();
    }

    ImageData getTexture(CubeSide side)
    {
        return textures[side];
    }

    private void setupColours(string data)
    {
        QuickTokenizer tokenizer;
        tokenizer.tokenize(data);

        while (!tokenizer.end())
        {
            string instruction = tokenizer.pop();

            switch (instruction)
            {
                case "lhs":
                    colourLow = parseColour(tokenizer.pop());
                    levelLow = to!float(tokenizer.pop());
                    colourHigh = parseColour(tokenizer.pop());
                    levelHigh = to!float(tokenizer.pop());
                    colourSteep = parseColour(tokenizer.pop());
                    levelSteep = to!float(tokenizer.pop());
                    break;

                default:
                    break;
            }
        }
    }

    private void makeTextures()
    {
        for (int i = 0; i < 6; i += 1)
        {
            auto heightmap = terrain.getHeightmap(cast(CubeSide)i);
            textures[i].width = heightmap.width - 1;
            textures[i].height = heightmap.height - 1;
            textures[i].data.length = textures[i].width * textures[i].height * 4;
            buildCubeTexture(textures[i], cast(CubeSide)i);
        }
    }

    private void buildCubeTexture(ref ImageData texture, CubeSide side)
    {
        int width = texture.width;
        int height = texture.height;

        float heightScale = 1.0f / terrain.maxTerrainHeight();

        for (int y = 0; y < height; y += 1)
        {
            for (int x = 0; x < width; x += 1)
            {
                int ofs = (y * width + x) * 4;

                float[4] heights = [
                    getHeight(x,     y,     side),
                    getHeight(x + 1, y,     side),
                    getHeight(x,     y + 1, side),
                    getHeight(x + 1, y + 1, side),
                ];

                float meanHeight = sum(heights[0..4]) * heightScale;
                float meanGradient = heights[0..4].maxElement - heights[0..4].minElement * heightScale;

                meanHeight = min(1, max(0, meanHeight));
                meanGradient = min(1, max(0, meanGradient));

                float[4] colour = lerpColour(colourHigh, colourLow, normalizeRange(meanHeight, levelLow, levelHigh));
                colour = lerpColour(colourSteep, colour, normalizeRange(meanGradient, 0, levelSteep));

                ubyte[4] byteColour = [
                    cast(int)(colour[0] * 255) & 0xff,
                    cast(int)(colour[1] * 255) & 0xff,
                    cast(int)(colour[2] * 255) & 0xff,
                    cast(int)(colour[3] * 255) & 0xff,
                ];

                texture.data[ofs..ofs+4] = byteColour;
            }
        }
    }

    pragma(inline, true) float getHeight(int x, int y, CubeSide side)
    {
        auto heightmap = terrain.getHeightmap(side);
        return heightmap.data[y * heightmap.width + x];
    }

    pragma(inline, true) float normalizeRange(float value, float low, float high)
    {
        return (max(low, min(high, value)) - low) / (high - low);
    }
}
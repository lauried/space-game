module space.models.planet.heightmapinstruction;

import space.library.text.quicktokenizer;
import std.conv;
import std.string;

struct HeightmapInstruction
{
    enum Type {
        Noise,
        NoiseRange,
        Limit,
        Scale,
        Curve,
        Push,
        PopMul,
        PopAdd,
    }

    Type type;
    float param1, param2, param3;

    static HeightmapInstruction noise(float frequency, float scale)
    {
        return HeightmapInstruction(Type.Noise, frequency, scale, 0);
    }

    static HeightmapInstruction noiseRange(float minFreq, float maxFreq, float scale)
    {
        return HeightmapInstruction(Type.NoiseRange, minFreq, maxFreq, scale);
    }

    static HeightmapInstruction limit(float min, float max)
    {
        return HeightmapInstruction(Type.Limit, min, max, 0);
    }

    static HeightmapInstruction scale(float scale)
    {
        return HeightmapInstruction(Type.Scale, scale, 0, 0);
    }

    static HeightmapInstruction curve(float power)
    {
        return HeightmapInstruction(Type.Curve, power, 0, 0);
    }

    static HeightmapInstruction push()
    {
        return HeightmapInstruction(Type.Push, 0, 0, 0);
    }

    static HeightmapInstruction popmul()
    {
        return HeightmapInstruction(Type.PopMul, 0, 0, 0);
    }

    static HeightmapInstruction popadd()
    {
        return HeightmapInstruction(Type.PopAdd, 0, 0, 0);
    }

    static HeightmapInstruction[] fromString(string data)
    {
        HeightmapInstruction[] instructions;
        QuickTokenizer tokenizer;
        tokenizer.tokenize(data);

        while (!tokenizer.end())
        {
            string instruction = tokenizer.pop();

            // TODO: Parsing code is messy here and could use a proper tokenizer/parser interface.
            switch (instruction)
            {
                case "noise":
                case "n":
                    {
                        float frequency = 0;
                        float scale = 0;
                        string s = tokenizer.peek();
                        if (isNumeric(s))
                        {
                            frequency = to!float(s);
                            tokenizer.pop();
                            s = tokenizer.peek();
                            if (isNumeric(s))
                            {
                                scale = to!float(s);
                                tokenizer.pop();
                            }
                        }
                        instructions ~= HeightmapInstruction.noise(frequency, scale);
                    }
                    break;

                case "noiseRange":
                case "nr":
                    {
                        float frequencyMin = 0;
                        float frequencyMax = 0;
                        float scale = 0;
                        string s = tokenizer.peek();
                        if (isNumeric(s))
                        {
                            frequencyMin = to!float(s);
                            tokenizer.pop();
                            s = tokenizer.peek();
                            if (isNumeric(s))
                            {
                                frequencyMax = to!float(s);
                                tokenizer.pop();
                                s = tokenizer.peek();
                                if (isNumeric(s))
                                {
                                    scale = to!float(s);
                                    tokenizer.pop();
                                }
                            }
                        }
                        instructions ~= HeightmapInstruction.noiseRange(frequencyMin, frequencyMax, scale);
                    }
                    break;

                case "limit":
                case "l":
                    {
                        float min = 0;
                        float max = 0;
                        string s = tokenizer.peek();
                        if (isNumeric(s))
                        {
                            min = to!float(s);
                            tokenizer.pop();
                            s = tokenizer.peek();
                            if (isNumeric(s))
                            {
                                max = to!float(s);
                                tokenizer.pop();
                            }
                        }
                        instructions ~= HeightmapInstruction.limit(min, max);
                    }
                    break;

                case "scale":
                case "s":
                    {
                        float scale = 0;
                        string s = tokenizer.peek();
                        if (isNumeric(s))
                        {
                            scale = to!float(s);
                            tokenizer.pop();
                        }
                        instructions ~= HeightmapInstruction.scale(scale);
                    }
                    break;

                case "curve":
                case "c":
                    {
                        float power = 1;
                        string s = tokenizer.peek();
                        if (isNumeric(s))
                        {
                            power = to!float(s);
                            tokenizer.pop();
                        }
                        instructions ~= HeightmapInstruction.curve(power);
                    }
                    break;

                case "push":
                case "u":
                    instructions ~= HeightmapInstruction.push();
                    break;

                case "popmul":
                case "pm":
                    instructions ~= HeightmapInstruction.popmul();
                    break;
                
                case "popadd":
                case "pa":
                    instructions ~= HeightmapInstruction.popadd();
                    break;

                default:
                    break;
            }
        }

        return instructions;
    }
}

unittest
{
    auto instructions = HeightmapInstruction.fromString("nr 4 12 2 l 0.85 1.85 s 0.025");

    assert(instructions.length == 3);
}

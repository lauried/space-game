module space.models.interfaces;

import space.library.geometry.orientation;
import space.library.geometry.frustum;
import space.engine.entity;
import entitysysd;

struct ModelVertex
{
    float[3] position;
    float[3] normal;
    float[4] colour;
    float[2] texcoord;
}

struct ModelMesh
{
    ModelVertex[] vertices;
    bool fullbright;
    int texture;
    int detailTexture;
}

interface ColliderInstance
{
    /**
     * Returns a radius which contains the entire model.
     */
    float getBoundingRadius();

    /**
     * If the sphere intersects, return a vector to move it that would de-intersect it.
     */
    float[3] collideSphere(float[3] origin, float radius);
}

interface ModelInstance
{
    /**
     * Returns the mesh in model space, given the position of the camera in model space.
     */
    ModelMesh[] getMeshes(Orientation!float camera, Frustum!float frustum);
}

struct Collider
{
    float[4][4] planes;
}

interface StaticColliderProvider
{
    // TODO: An iterable would be nicer to pass, because then we don't need to allocate memory all over the place.
    /**
     * Returns at least all the colliders that intersect the given region of space.
     * @todo: Parameters and return type.
     */
    Collider[] getColliders(float[3] mins, float[3] maxs);
}
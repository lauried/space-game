module space.time;

import std.datetime.stopwatch;
import core.thread;
import core.time;

class FrameTimer
{
    private StopWatch stopWatch;
    private long lastTime, frameTime;

    this()
    {
        lastTime = 0;
        frameTime = 30; // some dummy first frame that's sensible
        stopWatch.start();
    }

    void frame()
    {
        const long currentTime = stopWatch.peek().total!"msecs";
        frameTime = currentTime - lastTime;
        lastTime = currentTime;
    }

    long getFrameTime()
    {
        return frameTime;
    }

    long getTotalTime()
    {
        return lastTime;
    }

    /**
     * If the current frame hasn't taken long enough, delay it until it has.
     * Don't call this immediately after frame(), but rather after all the frame's tasks have been done.
     */
    void delayFrame(int time)
    {
        long elapsed = stopWatch.peek().total!"msecs" - lastTime;
        if (time > elapsed)
        {
            Thread.sleep(msecs(time - elapsed));
        }
    }
}

/**
 * Times how long subsystems take.
 * We let this be pretty free-form so we can measure nested and overlapping
 * periods if we want.
 */
class SubSystemTimer(E)
{
    private StopWatch stopWatch;
    private long[E.max - E.min + 1][2] times;

    this()
    {
        stopWatch.start();
    }

    void startSystem(E system)
    {
        times[system - E.min][0] = stopWatch.peek().total!"usecs";
    }

    void finishSystem(E system)
    {
        times[system - E.min][1] = stopWatch.peek().total!"usecs";
    }

    long getSystemTime(E system)
    {
        return times[system - E.min][1] - times[system - E.min][0];
    }
}
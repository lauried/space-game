
module space.exceptions;

class LogicException : Exception
{
    this(string message)
    {
        super(message);
    }
}

class RuntimeException : Exception
{
    this(string message)
    {
        super(message);
    }
}

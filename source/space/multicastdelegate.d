
module space.multicastdelegate;

/**
 * A multicast delgate allows delegates to be added to and removed from it,
 * and when called will call all delegates it currently contains with the given
 * parameters.
 *
 * Declaration:
 *     auto onKeyPress = new MulticastDelegate!(string, bool);
 *
 * Registering:
       void myFunction(string s, bool b) {}
 *     onKeyPress.add(myFunction);
 *
 * Calling:
 *     onKeyPress.call("KEY_X", true);
 */
class MulticastDelegate(A...)
{
    alias Delegate = void delegate(A);

    private Delegate[] delegates;

    this()
    {
        delegates = [];
    }

    /**
     * Ensures that the MulticastDelegate contains the given delegate.
     */
    void add(Delegate delegate_)
    {
        foreach (d; delegates)
        {
            if (delegate_ == d)
            {
                return;
            }
        }
        const auto oldLength = delegates.length;
        delegates.length += 1;
        delegates[oldLength] = delegate_;
    }

    /**
     * Ensures that the MulticastDelegate does not contain the given delegate.
     */
    void remove(Delegate delegate_)
    {
        for (auto i = 0; i < delegates.length; i += 1)
        {
            if (delegates[i] == delegate_)
            {
                for (auto j = i; j + 1 < delegates.length; j += 1)
                {
                    delegates[j] = delegates[j + 1];
                }
                delegates.length -= 1;
                return;
            }
        }
    }

    /**
     * Calls each delegate in turn.
     */
    void call(A parms)
    {
        foreach (d; delegates)
        {
            d(parms);
        }
    }
}

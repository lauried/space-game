module space.drawing.widgets;

import std.algorithm;
import std.math;

import space.drawing.interfaces;
import space.drawing.widgetproperties;
import space.drawing.widgets.radar;
import space.drawing.widgets.waypoints;

class WidgetFactory
{
    this()
    {
    }

    WidgetInterface createWidget(string name)
    {
        switch (name)
        {
            case "Text":
                return new TextWidget();
            case "Bar":
                return new BarWidget();
            case "Waypoints":
                return new WaypointsWidget();
            case "Radar":
                return new RadarWidget();
            default:
                return null;
        }
    }
}

class NullWidget : WidgetInterface
{
    private WidgetPropertyList properties;

    this()
    {
        properties = new WidgetPropertyList();
    }

    WidgetPropertyList getProperties()
    {
        return properties;
    }

    void draw(float[2] size, DrawingInterface drawing)
    {
    }
}

class TextWidget : WidgetInterface
{
    private WidgetPropertyList properties;
    string text = "";
    bool shadow = false;
    float[4] bgColour = [ 0, 0, 0, 0 ];
    float[4] fgColour = [ 1, 1, 1, 1 ];

    this()
    {
        properties = new WidgetPropertyList();
        properties.add!string("text", &(this.text));
        properties.add!bool("shadow", &(this.shadow));
        properties.add!(float[4])("bg", &(this.bgColour));
        properties.add!(float[4])("fg", &(this.fgColour));
    }

    WidgetPropertyList getProperties() { return properties; }

    void draw(float[2] size, DrawingInterface drawing)
    {
        if (bgColour[3])
        {
            drawing.drawQuad([ 0, 0 ], size, bgColour);
        }
        float[2] textSize = drawing.getTextSize(text);
        float[2] pos = (size[] - textSize[]) * 0.5;
        if (shadow)
        {
            float[2] shadowPos = pos[] + 1;
            drawing.drawText(shadowPos, [ 0, 0, 0, fgColour[3] ], text);
        } 
        drawing.drawText(pos, fgColour, text);
    }
}

class BarWidget : WidgetInterface
{
    private WidgetPropertyList properties;
    float[4] bgColour = [ 0, 0, 0, 1 ];
    float[4] fgColour = [ 1, 1, 1, 1 ];
    float fraction = 0.5;

    this()
    {
        properties = new WidgetPropertyList();
        properties.add!float("fraction", &(this.fraction));
        properties.add!(float[4])("bg", &(this.bgColour));
        properties.add!(float[4])("fg", &(this.fgColour));
    }

    WidgetPropertyList getProperties() { return properties; }

    void draw(float[2] size, DrawingInterface drawing)
    {
        fraction = min(1.0f, max(-1.0f, fraction));

        float[2] fgPos = [ fraction > 0 ? 0 : (1 + fraction) * size[0], 0 ];
        float[2] bgPos = [ fraction > 0 ? fraction * size[0] : 0, 0 ];

        fraction = abs(fraction);
        if (fraction != 0)
        {
            drawing.drawQuad(fgPos, [ size[0] * fraction, size[1] ], fgColour);
        }
        if (fraction != 1 && fraction != -1)
        {
            drawing.drawQuad(bgPos, [ size[0] * (1 - fraction), size[1] ], bgColour);
        }
    }
}

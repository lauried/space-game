module space.drawing.widgets.waypoints;

import space.drawing.interfaces;
import space.drawing.widgetproperties;
import space.engine.systems.detection;
import std.algorithm;
import std.math;

class WaypointsWidget : WidgetInterface
{
    private WidgetPropertyList properties;
    RadarItem[] items;
    float[2] fov;

    this()
    {
        items = [];
        properties = new WidgetPropertyList();
        properties.add!(RadarItem[])("items", &(this.items));
        properties.add!(float[2])("fov", &(this.fov));
    }

    WidgetPropertyList getProperties() { return properties; }

    void draw(float[2] size, DrawingInterface drawing)
    {
        float[2] invFov = 1.0f / fov[];

        foreach (item; items)
        {
            if (item.type != RadarItem.Type.waypoint)
            {
                continue;
            }

            const float border = 16;

            float[3] point = item.position;
            float scale = 1.0f / max(point[0], 0.7071 * sqrt(point[1] * point[1] + point[2] * point[2]));
            point[] *= scale;

            // adjust to pixel coordinates
            float[2] screenDir = point[1..3] * invFov[];
            screenDir[1] *= -1;
            float[2] screenPos = (1 + screenDir[]) * 0.5 * size[];
            screenDir[] *= 1.0f / sqrt(screenDir[0] * screenDir[0] + screenDir[1] * screenDir[1]);

            float[4] colour = [ 1, 1, 1, 0.5 ];
            float[2] textSize = drawing.getTextSize(item.name);

            float[2] mins = [ border, border ];
            float[2] maxs = [ size[0] - border, size[1] - border - textSize[1] * 2.5 ];
            screenPos = clampToBox(screenPos, screenDir, mins, maxs);
            drawDot(screenPos, colour, drawing);

            float[2] textPos = [
                max(border + 8, min(size[0] - textSize[0] - border, screenPos[0] - textSize[0] * 0.5)),
                screenPos[1] + textSize[1] * 1.5,
            ];
            drawing.drawText(textPos, colour, item.name);
        }
    }

    private void drawDot(float[2] pos, float[4] colour, DrawingInterface drawing)
    {
        float[2] dotSize = [ 4, 4 ];
        float[2] dotPos = pos[] - dotSize[] * -0.5;
        drawing.drawQuad(dotPos, dotSize, colour);
    }

    private float[2] clampToBox(float[2] pos, float[2] dir, float[2] mins, float[2] maxs)
    {
        for (int i = 0; i < 2; i += 1)
        {
            float dist = 0;
            if (pos[i] < mins[i]) dist = pos[i] - mins[i];
            if (pos[i] > maxs[i]) dist = pos[i] - maxs[i];
            if (dist)
            {
                pos[] -= dir[] * (dist / dir[i]);
            }
        }

        return pos;
    }
}
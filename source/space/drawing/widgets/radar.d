module space.drawing.widgets.radar;

import space.drawing.interfaces;
import space.drawing.widgetproperties;
import space.engine.systems.detection;
import space.library.geometry.vector;
import std.algorithm;
import std.math;

import std.stdio;
import std.conv;

class RadarWidget : WidgetInterface
{
    private WidgetPropertyList properties;
    RadarItem[] items;
    float range;
    float[4] gridColour;

    this()
    {
        items = [];
        properties = new WidgetPropertyList();
        properties.add!(RadarItem[])("items", &(this.items));
        properties.add!float("range", &(this.range));
        range = 25000;
        properties.add!(float[4])("gridColour", &(this.gridColour));
        gridColour = [ 0, 1, 0, 0.5 ];
    }

    WidgetPropertyList getProperties() { return properties; }

    void draw(float[2] size, DrawingInterface drawing)
    {
        foreach (item; items)
        {
            if (item.position[2] < 0)
            {
                drawItem(item, size, drawing);
            }
        }

        drawing.drawQuad([ 0, size[1] * 0.25 ], [ size[0], 1 ], gridColour);
        drawing.drawQuad([ 0, size[1] * 0.75 ], [ size[0], 1 ], gridColour);
        drawing.drawQuad([ 0, size[1] * 0.25 ], [ 1, size[1] * 0.5 ], gridColour);
        drawing.drawQuad([ size[0], size[1] * 0.25 ], [ 1, size[1] * 0.5 ], gridColour);

        drawing.drawQuad([ 0, size[1] * 0.5 ], [ size[0], 1 ], gridColour);
        drawing.drawQuad([ size[0] * 0.5, size[1] * 0.25 ], [ 1, size[1] * 0.5 ], gridColour);

        foreach (item; items)
        {
            if (item.position[2] >= 0)
            {
                drawItem(item, size, drawing);
            }
        }
    }

    private void drawItem(RadarItem item, float[2] size, DrawingInterface drawing)
    {
        float[3] point = item.position;

        float dist;
        point = normalize(point, dist);
        dist /= range;
        dist = sqrt(dist);
        dist = min(1, dist);
        point[] *= dist;

        point[0] *= -1;
        point[2] *= -1;

        float[2] base = [
            (point[1] + 1) * size[0] * 0.5,
            size[1] * 0.25 + (point[0] + 1) * size[1] * 0.25,
        ];

        float height = point[2] * size[1] * 0.25;
        float[2] boxSize = [ 1, height ];

        if (height < 0)
        {
            base[1] += height;
            boxSize[1] *= -1;
            height = 0;
        }

        float[4] colour = [ 1, 1, 1, 1 ];

        drawing.drawQuad(base, boxSize, colour);
        drawDot([ base[0], base[1] + height ], colour, drawing);
    }

    private void drawDot(float[2] pos, float[4] colour, DrawingInterface drawing)
    {
        float[2] dotSize = [ 2, 2 ];
        float[2] dotPos = pos[] - dotSize[] * 0.5;
        drawing.drawQuad(dotPos, dotSize, colour);
    }
}
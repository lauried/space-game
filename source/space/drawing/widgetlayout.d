module space.drawing.widgetlayout;

import space.drawing.interfaces;
import space.drawing.widgetdrawing;
import std.algorithm;

enum HorizAlign
{
    left,
    middle,
    right,
}

enum VertAlign
{
    top,
    middle,
    bottom,
}

enum Alignment
{
    top,
    left,
    middle,
    bottom,
    right,
}

enum Layout
{
    left_to_right,
    right_to_left,
    top_to_bottom,
    bottom_to_top,
}

class LayoutElement
{
    string id;
    VertAlign valign = VertAlign.top;
    HorizAlign halign = HorizAlign.left;
    Layout layout = Layout.top_to_bottom;
    Alignment layout_align = Alignment.top;
    float layout_spacing = 0;
    float[2] position = [ 0, 0 ];
    bool position_auto = true;
    float[2] size = [ 0, 0 ];
    bool size_parent = false;
    bool size_auto = true;
    float font_size = 1;
    LayoutElement[] children = [];
    WidgetInterface widget;

    float[2] absolutePosition;
    float[2] absoluteSize;

    void setSize(float[2] parentSize)
    {
        // Grab some stuff while we're iterating child nodes.
        int numAutoChildren = 0;
        float[2] autoSizeSum = [ 0, 0 ];
        float[2] autoSizeMax = [ 0, 0 ];
        foreach (child; children)
        {
            if (child.size_parent)
            {
                continue;
            }

            child.setSize([ 0, 0 ]);
            if (child.position_auto)
            {
                numAutoChildren += 1;
                autoSizeSum[] += child.absoluteSize[];
                autoSizeMax[0] = max(autoSizeMax[0], child.absoluteSize[0]);
                autoSizeMax[1] = max(autoSizeMax[1], child.absoluteSize[1]);
            }
        }

        if (size_auto)
        {
            // Auto size fits to our auto positioned children in our given layout direction.
            if (layout == Layout.top_to_bottom || layout == Layout.bottom_to_top)
            {
                absoluteSize = [
                    autoSizeMax[0],
                    autoSizeSum[1] + layout_spacing * min(1, numAutoChildren - 1),
                ];
            }
            else
            {
                absoluteSize = [
                    autoSizeSum[0] + layout_spacing * min(1, numAutoChildren - 1),
                    autoSizeMax[1],
                ];
            }
        }
        else if (size_parent)
        {
            absoluteSize[] = parentSize[];
        }
        else
        {
            absoluteSize[] = size[];
        }

        foreach (child; children)
        {
            if (child.size_parent)
            {
                child.setSize(absoluteSize);
            }
        }
    }
    
    void setPosition(float[2] parentPos, float[2] parentSize)
    {
        if (!position_auto)
        {
            switch (halign)
            {
                default:
                case HorizAlign.left:
                    absolutePosition[0] = position[0];
                    break;
                case HorizAlign.middle:
                    absolutePosition[0] = (parentSize[0] - absoluteSize[0]) * 0.5;
                    break;
                case HorizAlign.right:
                    absolutePosition[0] = (parentSize[0] - absoluteSize[0]) - position[0];
                    break;
            }
            switch (valign)
            {
                default:
                case VertAlign.top:
                    absolutePosition[1] = position[1];
                    break;
                case VertAlign.middle:
                    absolutePosition[1] = (parentSize[1] - absoluteSize[1]) * 0.5;
                    break;
                case VertAlign.bottom:
                    absolutePosition[1] = (parentSize[1] - absoluteSize[1]) - position[1];
                    break;
            }
            absolutePosition[] += parentPos[];
        }

        float[2] currentPos = [ 0, 0 ];
        float direction = 1;
        int layoutAxis = 0;
        int alignAxis = 1;
        switch (layout)
        {
            default:
            case Layout.left_to_right:
                break;
            case Layout.right_to_left:
                direction = -1;
                currentPos[0] = absoluteSize[0];
                break;
            case Layout.top_to_bottom:
                layoutAxis = 1;
                alignAxis = 0;
                break;
            case Layout.bottom_to_top:
                layoutAxis = 1;
                alignAxis = 0;
                direction = -1;
                currentPos[1] = absoluteSize[1];
                break;
        }

        foreach (child; children)
        {
            if (!child.position_auto)
            {
                continue;
            }

            child.absolutePosition[] = currentPos[];
            child.absolutePosition[layoutAxis] += child.absoluteSize[layoutAxis] * min(0, direction);

            switch (layout_align)
            {
                default:
                case Alignment.top:
                case Alignment.left:
                    break;
                case Alignment.middle:
                    child.absolutePosition[alignAxis] = (absoluteSize[alignAxis] - child.absoluteSize[alignAxis]) * 0.5;
                    break;
                case Alignment.right:
                    child.absolutePosition[alignAxis] = absoluteSize[alignAxis] - child.absoluteSize[alignAxis];
                    break;
            }

            child.absolutePosition[] += absolutePosition[];
            currentPos[layoutAxis] += (child.absoluteSize[layoutAxis] + layout_spacing) * direction;
        }

        foreach (child; children)
        {
            child.setPosition(absolutePosition, absoluteSize);
        }
    }

    void getWidgets(WidgetList widgets)
    {
        if (widget !is null)
        {
            widgets.addItem(id, widget, absolutePosition, absoluteSize);
        }

        foreach (child; children)
        {
            child.getWidgets(widgets);
        }
    }
}

module space.drawing.primitives;

import space.renderer.interfaces;
import space.drawing.imagesource;

/**
 * Draw primitives.
 */
class PrimitiveDrawing
{
    private RendererInterface renderer;
    private int blankTexture;

    this(TextureFactory textureFactory, RendererInterface renderer)
    {
        this.renderer = renderer;
        // TODO: Add some rendering helper layer so that we already have these textures.
        // Or update renderer so we can set textures to blank.
        blankTexture = textureFactory.getTexture("#ffff");
    }

    void drawQuad(float[2] pos, float[2] size, float[4] colour)
    {
        float[2] max = pos[] + size[];
        RendererInterface.Vertex[] mesh;
        mesh.length = 6;
        makeVertex(&mesh[0], pos[0], pos[1], colour);
        makeVertex(&mesh[1], max[0], pos[1], colour);
        makeVertex(&mesh[2], max[0], max[1], colour);
        makeVertex(&mesh[3], pos[0], pos[1], colour);
        makeVertex(&mesh[4], max[0], max[1], colour);
        makeVertex(&mesh[5], pos[0], max[1], colour);
        renderer.setTexture(0, blankTexture);
        renderer.setTexture(1, blankTexture);
        renderer.drawMesh(mesh);
    }

    private void makeVertex(RendererInterface.Vertex* vert, float x, float y, float[4] colour)
    {
        vert.position = [ x, y, 0 ];
        vert.texcoord = [ 0, 0 ];
        vert.colour = colour;
    }
}

module space.drawing.gui;

import space.drawing.interfaces;
import space.drawing.widgetdrawing;
import space.drawing.widgetlayout;
import space.drawing.widgets;
import space.library.text.parser;
import std.conv;
import std.regex;
import std.stdio;
import std.string;

class GuiManager
{
    private WidgetDrawing widgetDrawing;
    private Gui gui;
    private float[2] screenSize;

    this(WidgetDrawing widgetDrawing)
    {
        this.widgetDrawing = widgetDrawing;
        widgetDrawing.scale = 2;
    }

    void setScreenSize(float[2] size)
    {
        screenSize = size[] / widgetDrawing.scale;
        if (gui !is null)
        {
            gui.setPosition([ 0, 0 ], screenSize);
        }
    }

    void setGui(Gui gui)
    {
        this.gui = gui;
        gui.setPosition([ 0, 0 ], screenSize);
    }

    void draw()
    {
        gui.draw(widgetDrawing);
    }
}

class GuiFactory
{
    private GuiParser parser;

    this(GuiParser parser)
    {
        this.parser = parser;
    }

    Gui createGui(string text)
    {
        try
        {
            return this.parser.parseGui(text);
        }
        catch (Exception e)
        {
            writeln("Error parsing gui: " ~ e.msg);
            return new Gui(new LayoutElement());
        }
    }
}

class Gui
{
    private LayoutElement root;
    private WidgetList widgets;

    this(LayoutElement root)
    {
        this.root = root;
        widgets = new WidgetList();
    }

    void setPosition(float[2] pos, float[2] size)
    {
        this.root.absolutePosition = [ 0, 0 ];
        this.root.position_auto = false;
        this.root.size_auto = false;
        this.root.size_parent = true;
        this.root.setSize(size);
        this.root.setPosition(pos, size);
        widgets.clear();
        this.root.getWidgets(widgets);
    }

    void draw(WidgetDrawing drawing)
    {
        widgets.drawItems(drawing);
    }

    WidgetInterface getWidget(string id)
    {
        return widgets.getWidget(id);
    }
}

class GuiParser
{
    private WidgetFactory widgetFactory;

    this(WidgetFactory widgetFactory)
    {
        this.widgetFactory = widgetFactory;
    }

    Gui parseGui(string text)
    {
        Parser parser = makeParser();
        parser.startParsing(text);
        return new Gui(parseElement(parser));
    }

    LayoutElement parseElement(Parser parser)
    {
        LayoutElement element = new LayoutElement();
        parser.requireTokenType("openbrace");
        while (parser.peekToken().type != "closebrace")
        {
            if (parser.peekToken().type == "openbrace")
            {
                element.children ~= parseElement(parser);
                continue;
            }

            string key = parser.requireTokenType("identifier").value;
            parser.requireTokenType("equals");
            string value = strip(parser.requireTokenType("string").value, "\"");

            auto pattern = regex(`\s+`);
            switch (key)
            {
                case "id": // <string>
                    element.id = value;
                    break;
                case "size": // <num> <num>
                    if (value == "auto")
                    {
                        element.size_auto = true;
                    }
                    else if (value == "fill")
                    {
                        element.size_parent = true;
                        element.size_auto = false;
                    }
                    else
                    {
                        element.size_auto = false;
                        element.size_parent = false;
                        auto parts = value.splitter(pattern);
                        element.size[0] = parts.empty ? 0 : to!float(parts.front);
                        parts.popFront();
                        element.size[1] = parts.empty ? 0 : to!float(parts.front);
                    }
                    break;
                case "font_size": // <num>
                    element.font_size = to!float(value);
                    break;
                case "position": // <num> <num>
                    if (value == "auto")
                    {
                        element.position_auto = true;
                    }
                    else
                    {
                        element.position_auto = false;
                        auto parts = value.splitter(pattern);
                        element.position[0] = parts.empty ? 0 : to!float(parts.front);
                        parts.popFront();
                        element.position[1] = parts.empty ? 0 : to!float(parts.front);
                        
                    }
                    break;
                case "align": // <Alignment> <Alignment>
                    {
                        auto parts = value.splitter(pattern);
                        element.halign = parts.empty ? HorizAlign.left : to!HorizAlign(parts.front);
                        parts.popFront();
                        element.valign = parts.empty ? VertAlign.top : to!VertAlign(parts.front);
                    }
                    break;
                case "layout": // <Layout> <Alignment>
                    {
                        auto parts = value.splitter(pattern);
                        element.layout = parts.empty ? Layout.top_to_bottom : to!Layout(parts.front);
                        parts.popFront();
                        element.layout_align = parts.empty ? Alignment.top : to!Alignment(parts.front);
                    }
                    break;
                case "layout_spacing": // <num>
                    element.layout_spacing = to!float(value);
                    break;
                case "widget": // <widget>
                    element.widget = widgetFactory.createWidget(value);
                    break;
                default:
                    break;
            }

            if (element.widget !is null)
            {
                element.widget.getProperties().set(key, value);
            }
        }
        parser.requireTokenType("closebrace");
        return element;
    }

    private Parser makeParser()
    {
        return new Parser(new BasicParser([
            BasicParser.TokenType("newline", new NewlineToken(), true),
            BasicParser.TokenType("whitespace", new RegexToken(regex(`\s+`))),
            BasicParser.TokenType("comment", new RegexToken(regex(`\/\/[^\n\r]+`))),
            BasicParser.TokenType("openbrace", new MatchToken("{")),
            BasicParser.TokenType("closebrace", new MatchToken("}")),
            BasicParser.TokenType("equals", new MatchToken("=")),
            BasicParser.TokenType("string", new QuotedStringToken()),
            BasicParser.TokenType("identifier", new RegexToken(regex(`[a-zA-Z0-9-_]+`))),
        ]), [ "newline", "whitespace", "comment" ]);
    }
}

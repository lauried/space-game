module space.drawing.widgetproperties;

import std.array;
import std.conv;
import std.regex;

interface WidgetPropertyInterface
{
    void set(string value);
}

class WidgetProperty(T) : WidgetPropertyInterface
{
    T* target;

    this(T* target)
    {
        this.target = target;
    }

    void set(string value)
    {
        *target = to!T(value);
    }
}

class WidgetProperty(T : T[n], size_t n) : WidgetPropertyInterface
{
    T[n]* target;
    
    this(T[n]* target)
    {
        this.target = target;
    }

    void set(string value)
    {
        try
        {
            auto pattern = regex(`\s+`);
            *target = to!(T[n])("[" ~ value.splitter(pattern).join(",") ~ "]");
        }
        catch (ConvException)
        {
        }
    }
}

class WidgetProperty(T : T[]) : WidgetPropertyInterface
{
    T[]* target;

    this(T[]* target)
    {
        this.target = target;
    }

    void set(string value)
    {
    }
}

class WidgetPropertyList
{
    private struct Entry
    {
        string name;
        WidgetPropertyInterface property;
    }

    private Entry[] properties = [];

    this() {}

    void add(T)(string name, T* target)
    {
        Entry entry;
        entry.name = name;
        entry.property = new WidgetProperty!T(target);
        properties ~= entry;
    }

    void set(string key, string value)
    {
        foreach (entry; properties)
        {
            if (entry.name == key)
            {
                entry.property.set(value);
                return;
            }
        }
    }

    T* get(T)(string key)
    {
        static T nullValue;

        foreach (entry; properties)
        {
            if (entry.name == key)
            {
                auto property = cast(WidgetProperty!T)entry.property;
                if (property is null)
                {
                    return &nullValue;
                }
                return property.target;
            }
        }

        return &nullValue;
    }
}

// This is just checking that we can do stuff
unittest
{
    string s = "1 2 3";
    float[3] v;
    try
    {
        auto pattern = regex(`\s+`);
        v = to!(float[3])("[" ~ s.splitter(pattern).join(",") ~ "]");
    }
    catch (ConvException)
    {
    }

    assert(to!string(v) == "[1, 2, 3]", "Expect result '" ~ to!string(v) ~ "' to equal '[1, 2, 3]'");
}
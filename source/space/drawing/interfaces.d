module space.drawing.interfaces;

import space.drawing.widgetproperties;

/**
 * Provides methods for widgets to draw their basic elements, without having to
 * understand low-level things like polygons.
 */
interface DrawingInterface
{
    void drawQuad(float[2] pos, float[2] size, float[4] colour);
    float[2] getTextSize(string text);
    void drawText(float[2] pos, float[4] colour, string text);
}

interface WidgetInterface
{
    WidgetPropertyList getProperties();

    /**
     * A widget currently should make sure it doesn't draw outside of the
     * bounds 0, 0 -> size, but we might relax this later using viewports or
     * other kinds of truncation within the drawing interface.
     */
    void draw(float[2] size, DrawingInterface drawing);
}

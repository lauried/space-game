module space.drawing.fonts;

import space.renderer.interfaces;
import space.drawing.imagesource;

class FontDrawing
{
    private struct FontData
    {
        int texture;
        int cols;
        int rows;
        float charWidth;
        float charHeight;
    }

    private TextureFactory textureFactory;
    private RendererInterface renderer;
    private int blankTexture;
    private FontData[] fonts;

    this(TextureFactory textureFactory, RendererInterface renderer)
    {
        this.textureFactory = textureFactory;
        this.renderer = renderer;

        loadFont("data/font8x8-white_c32_r4-square.png", 32, 4, 8.0, 8.0);
        blankTexture = textureFactory.getTexture("#fff0");
    }

    float[2] getTextSize(string text, int fontIndex = 0)
    {
        auto font = &fonts[fontIndex];

        int runLength = 0;
        float maxWidth = 0;
        float maxHeight = 0;

        foreach (c; text)
        {
            if (c != '\n')
            {
                runLength += 1;
            }
            else
            {
                maxHeight += font.charHeight;
                float newWidth = runLength * font.charWidth;
                if (newWidth > maxWidth)
                {
                    maxWidth = newWidth;
                }
            }
        }

        maxHeight += font.charHeight;
        float newWidth = runLength * font.charWidth;
        if (newWidth > maxWidth)
        {
            maxWidth = newWidth;
        }

        return [ maxWidth, maxHeight ];
    }

    void drawText(string text, float[2] pos, float[4] colour, float scale = 1, int fontIndex = 0)
    {
        auto font = &fonts[fontIndex];
        RendererInterface.Vertex[] mesh;

        mesh.length = text.length * 6;

        int meshIndex = 0;
        int shrink = 0;

        float[2] cur = pos;
        float[2] ts = [
            1.0 / cast(float) font.cols,
            1.0 / cast(float) font.rows,
        ];

        foreach (c; text)
        {
            if (c == '\n')
            {
                cur[0] = pos[0];
                cur[1] += font.charHeight * scale;
                shrink += 6;
            }

            int cint = cast(int)c % (font.rows * font.cols);
            int col = cint % font.cols;
            int row = (cint - col) / font.cols;
            float[2] tc = [
                cast(float)col / font.cols,
                cast(float)row / font.rows,
            ];
            float[2] tm = tc[] + ts[];
            float[2] max = [
                cur[0] + font.charWidth * scale,
                cur[1] + font.charHeight * scale,
            ];

            makeVertex(&mesh[meshIndex  ], cur[0], cur[1], tc[0], tc[1], colour);
            makeVertex(&mesh[meshIndex+1], max[0], cur[1], tm[0], tc[1], colour);
            makeVertex(&mesh[meshIndex+2], max[0], max[1], tm[0], tm[1], colour);
            makeVertex(&mesh[meshIndex+3], cur[0], cur[1], tc[0], tc[1], colour);
            makeVertex(&mesh[meshIndex+4], max[0], max[1], tm[0], tm[1], colour);
            makeVertex(&mesh[meshIndex+5], cur[0], max[1], tc[0], tm[1], colour);

            cur[0] += font.charWidth * scale;
            meshIndex += 6;
        }

        mesh.length -= shrink;

        renderer.setTexture(0, font.texture);
        renderer.setTexture(1, blankTexture);
        renderer.drawMesh(mesh);
    }

    private void makeVertex(RendererInterface.Vertex* vert, float x, float y, float s, float t, float[4] colour)
    {
        vert.position = [ x, y, 0 ];
        vert.texcoord = [ s, t ];
        vert.colour = colour;
    }

    private int loadFont(string filename, int cols, int rows, float charWidth, float charHeight)
    {
        int index = cast(int)fonts.length;

        fonts.length += 1;
        auto font = &fonts[index];
        font.texture = textureFactory.getTexture(filename);
        font.cols = cols;
        font.rows = rows;
        font.charWidth = charWidth;
        font.charHeight = charHeight;

        return index;
    }
}
module space.drawing.model;

import space.models.interfaces;
import space.library.geometry.intersection;
import space.library.geometry.matrix;
import space.library.geometry.orientation;
import space.library.geometry.vector;
import space.renderer.interfaces;
import std.algorithm;
import std.math;

struct Light
{
    float[3] direction;
    float[3] light;
    float[3] ambientLight;
}

struct Fog
{
    bool enabled = false;
    float[3] position;
    float radius;
    float[3] colour;
    float density;
}

class ModelDrawing
{
    private RendererInterface renderer;

    this (RendererInterface renderer)
    {
        this.renderer = renderer;
    }

    void drawModelMesh(const ModelMesh model, Orientation!float orientation, Light[] lights, Fog fog)
    {
        if (model.fullbright)
        {
            drawModelMesh(model, orientation);
            return;
        }
        
        RendererInterface.Vertex[] mesh;
        auto matrix = Matrix!float.model(orientation.forward, orientation.right, orientation.up, orientation.position);

        mesh.length = model.vertices.length;

        for (auto i = 0; i < model.vertices.length; i += 1)
        {
            matrix.multiplyPos3(model.vertices[i].position, mesh[i].position);
            mesh[i].texcoord = model.vertices[i].texcoord;
            mesh[i].colour = [ 0, 0, 0, 1];

            foreach (light; lights)
            {
                float dot = dotProduct!float(light.direction, model.vertices[i].normal);
                float main = max(0, dot);
                mesh[i].colour[0..3][] += light.light[] * main;
                mesh[i].colour[0..3][] += light.ambientLight[];
            }

            mesh[i].colour[] *= model.vertices[i].colour[];
        }

        if (fog.enabled)
        {
            float invRadius = 1.0f / fog.radius;
            float[3] cameraPosInFogSpace = (orientation.position[] - fog.position[]) * invRadius;
            for (auto i = 0; i < model.vertices.length; i += 1)
            {
                float[3] modelVertInFogSpace = (model.vertices[i].position[] - fog.position[]) * invRadius;
                float[3] dir = modelVertInFogSpace[] - cameraPosInFogSpace[];
                // We're measuring distance to the point here, rather than the
                // back of the sphere, so we use this instead of the second
                // solution.
                float dist;
                dir = normalize(dir, dist);
                auto result = lineSphereIntersect(cameraPosInFogSpace, dir);
                float depth = 0;
                if (result.numSolutions == 2)
                {
                    depth = min(dist, result.solutions[1]) - max(0, result.solutions[0]);
                    depth *= 0.5;
                    depth = max(0, depth - 0.1);
                }
                
                mesh[i].fog[0..3] = fog.colour;
                mesh[i].fog[3] = depth * fog.density;
            }
        }

        drawModelMesh(model, mesh);
    }

    void drawModelMesh(const ModelMesh model, Orientation!float orientation)
    {
        RendererInterface.Vertex[] mesh;
        auto matrix = Matrix!float.model(orientation.forward, orientation.right, orientation.up, orientation.position);

        mesh.length = model.vertices.length;

        for (auto i = 0; i < model.vertices.length; i += 1)
        {
            matrix.multiplyPos3(model.vertices[i].position, mesh[i].position);
            mesh[i].texcoord = model.vertices[i].texcoord;
            mesh[i].colour = model.vertices[i].colour;
        }

        drawModelMesh(model, mesh);
    }

    private void drawModelMesh(const ModelMesh model, RendererInterface.Vertex[] mesh)
    {
        renderer.setTexture(0, model.texture);
        renderer.setTexture(1, model.detailTexture);
        renderer.drawMesh(mesh);
    }
}
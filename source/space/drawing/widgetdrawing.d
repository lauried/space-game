module space.drawing.widgetdrawing;

import space.drawing.interfaces;
import space.drawing.widgets;
import space.drawing.fonts;
import space.drawing.primitives;

class WidgetDrawing : DrawingInterface
{
    private FontDrawing fontDrawing;
    private PrimitiveDrawing primitives;

    float[2] offset = [ 0, 0 ];
    float scale = 1;

    this(FontDrawing fontDrawing, PrimitiveDrawing primitives)
    {
        this.fontDrawing = fontDrawing;
        this.primitives = primitives;
    }

    void drawQuad(float[2] pos, float[2] size, float[4] colour)
    {
        pos[] += offset[];
        pos[] *= scale;
        size[] *= scale;
        primitives.drawQuad(pos, size, colour);
    }

    float[2] getTextSize(string text)
    {
        // When our scaling is hidden from the widget, we return the non-scaled
        // size.
        return fontDrawing.getTextSize(text);
    }

    void drawText(float[2] pos, float[4] colour, string text)
    {
        pos[] += offset[];
        pos[] *= scale;
        fontDrawing.drawText(text, pos, colour, scale);
    }
}

class WidgetList
{
    private struct Item
    {
        string id;
        float[2] pos;
        float[2] size;
        WidgetInterface widget;
    }

    private Item[] items = [];
    private WidgetInterface nullWidget = new NullWidget();

    this()
    {
    }

    void clear()
    {
        items.length = 0;
    }

    /**
     * Duplicate IDs will result in only the first item with the given ID being found.
     */
    void addItem(string id, WidgetInterface widget, float[2] pos, float[2] size)
    {
        Item item;
        item.id = id;
        item.pos = pos;
        item.size = size;
        item.widget = widget;
        items ~= item;
    }

    void drawItems(WidgetDrawing drawing)
    {
        foreach (item; items)
        {
            drawing.offset = item.pos;
            item.widget.draw(item.size, drawing);
        }
    }

    WidgetInterface getWidget(string id)
    {
        foreach (item; items)
        {
            if (item.id == id)
            {
                return item.widget;
            }
        }
        return nullWidget;
    }
}

module space.drawing.imagesource;

import space.library.colour;
import space.renderer.interfaces;
import space.filetypes.image;
import std.functional;

interface DataSourceFactoryInterface
{
    DataSource getDataSource(string filename);
}

class ImageLoaderFactory : DataSourceFactoryInterface
{
    class ImageLoader
    {
        private ImageType imageType;
        private string filename;
        private Image image;

        this(ImageType imageType, string filename)
        {
            this.imageType = imageType;
            this.filename = filename;
        }

        ImageData getData()
        {
            if (image is null)
            {
                image = imageType.loadImage(filename);
            }

            return ImageData(image.data, image.width, image.height);
        }
    }

    private ImageType imageType;

    this(ImageType imageType)
    {
        this.imageType = imageType;
    }

    DataSource getDataSource(string filename)
    {
        auto imageLoader = new ImageLoader(imageType, filename);
        return delegate ImageData() { return imageLoader.getData(); };
    }
}

class SolidColourFactory : DataSourceFactoryInterface
{
    this()
    {
    }

    DataSource getDataSource(string colourString)
    {
        return delegate() {
            float[4] colour = parseColour(colourString);
            ubyte[] data;
            data.length = 4;
            data[0] = cast(ubyte)(colour[0] * 255);
            data[1] = cast(ubyte)(colour[1] * 255);
            data[2] = cast(ubyte)(colour[2] * 255);
            data[3] = cast(ubyte)(colour[3] * 255);
            return ImageData(data, 1, 1);
        };
    }
}

class TextureFactory
{
    private RendererInterface renderer;
    private ImageLoaderFactory imageLoaderFactory;
    private SolidColourFactory solidColourFactory;

    private int[string] textures;

    this(RendererInterface renderer, ImageLoaderFactory imageLoaderFactory, SolidColourFactory solidColourFactory)
    {
        this.renderer = renderer;
        this.imageLoaderFactory = imageLoaderFactory;
        this.solidColourFactory = solidColourFactory;
    }

    int getTexture(string identifier)
    {
        int* pointer = identifier in textures;
        if (pointer !is null)
        {
            return *pointer;
        }

        int texture = renderer.allocateTexture();
        renderer.setTextureDataSource(texture, factoryForIdentifier(identifier).getDataSource(identifier));
        textures[identifier] = texture;

        return texture;
    }

    int getTexture(string identifier, ImageData delegate() source)
    {
        int* pointer = identifier in textures;
        if (pointer !is null)
        {
            return *pointer;
        }

        int texture = renderer.allocateTexture();
        renderer.setTextureDataSource(texture, source);
        textures[identifier] = texture;

        return texture;
    }

    private DataSourceFactoryInterface factoryForIdentifier(string identifier)
    {
        if (identifier.length && identifier[0] == '#')
        {
            return solidColourFactory;
        }
        return imageLoaderFactory;
    }
}
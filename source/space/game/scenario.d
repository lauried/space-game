module space.game.scenario;

import space.engine.entity;
import space.filetypes.eav;
import space.game.entities.interfaces;
import space.library.geometry.orientation;
import space.library.text.conversion;

class ScenarioLoader
{
    private EavType eavType;
    private EntityFactoryInterface[string] entityFactories;

    this(EavType eavType)
    {
        this.eavType = eavType;
    }

    void addEntity(string name, EntityFactoryInterface factory)
    {
        entityFactories[name] = factory;
    }

    void loadScenario(string filename, Ecs ecs)
    {
        auto eav = eavType.loadEav(filename);

        Orientation!float orientation = Orientation!float.identity();
        loadEav(eav, ecs, orientation);
    }

    private void loadEav(Eav eav, Ecs ecs, const ref Orientation!float parentOrientation)
    {
        Orientation!float orientation = Orientation!float.identity();

        auto originString = "origin" in eav.data;
        if (originString !is null)
        {
            orientation.position = stringToVector!(float[3])(*originString);
        }

        orientation = parentOrientation.orientationToExternalSpace(orientation);

        if (eav.type.length)
        {
            auto factory = eav.type in entityFactories;
            if (factory !is null)
            {
                factory.createEntity(eav.type, eav.data, ecs, orientation);
            }
        }

        foreach (childEav; eav.children)
        {
            loadEav(childEav, ecs, orientation);
        }
    }
}

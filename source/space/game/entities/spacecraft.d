module space.game.entities.spacecraft;

import space.engine.entity;
import space.game.entities.interfaces;
import space.library.geometry.orientation;
import space.models.map;
import std.conv;

class SpacecraftFactory : EntityFactoryInterface
{
    private MapModelFactory mapModelFactory;

    this(MapModelFactory mapModelFactory)
    {
        this.mapModelFactory = mapModelFactory;
    }

    void createEntity(string type, string[string] data, Ecs ecs, const ref Orientation!float orientation)
    {
        string model = data.get("model", "");
        if (!model.length)
        {
            return;
        }

        float scale = to!float(data.get("scale", "128"));

        auto ent = ecs.entities.create();
        ent.register!PositionComponent(orientation);
        // Catch conversion failure.
        ent.register!ModelComponent(mapModelFactory.loadModel(model, scale));
    }
}

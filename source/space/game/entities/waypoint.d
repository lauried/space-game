module space.game.entities.waypoint;

import space.engine.entity;
import space.game.entities.interfaces;
import space.library.geometry.orientation;

class WaypointFactory : EntityFactoryInterface
{
    this()
    {
    }

    void createEntity(string type, string[string] data, Ecs ecs, const ref Orientation!float orientation)
    {
        string name = data.get("name", "");
        if (!name.length)
        {
            return;
        }

        auto ent = ecs.entities.create();
        ent.register!PositionComponent(orientation);
        ent.register!WaypointComponent(name);
    }
}

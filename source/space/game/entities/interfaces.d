module space.game.entities.interfaces;

import space.engine.entity;
import space.library.geometry.orientation;
import entitysysd;

interface EntityFactoryInterface
{
    void createEntity(string type, string[string] data, Ecs ecs, const ref Orientation!float orientation);
}

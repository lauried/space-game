module space.game.entities.planet;

import space.engine.entity;
import space.game.entities.interfaces;
import space.library.colour;
import space.library.geometry.orientation;
import space.library.text.conversion;
import space.models.atmosphere;
import space.models.planet;
import std.conv;

class PlanetFactory : EntityFactoryInterface
{
    private PlanetModelFactory planetModelFactory;

    this(PlanetModelFactory planetModelFactory)
    {
        this.planetModelFactory = planetModelFactory;
    }

    void createEntity(string type, string[string] data, Ecs ecs, const ref Orientation!float orientation)
    {
        auto entity = ecs.entities.create();
        entity.register!PositionComponent(orientation);

        bool fullbright = false;
        if (auto light = "light" in data)
        {
            string ambient = data.get("ambientLight", "#000");
            entity.register!LightingComponent(parseColour(*light), parseColour(ambient));
            fullbright = true;
        }

        float radius = to!float(data.get("radius", "1000"));
        string terrain = data.get("terrain", "");
        string texture = data.get("texture", "");
        auto model = planetModelFactory.createPlanet(radius, terrain, texture, fullbright);
        entity.register!ModelComponent(model);
        entity.register!ColliderComponent(model);

        if (auto atmosphere = "atmosphere" in data)
        {
            TokenArray tokens = TokenArray(*atmosphere);
            float depth = tokens.pop!float(0);
            float density = tokens.pop!float(0);
            float[4] colour = parseColour(tokens.pop!string("#fff"));
            float colourDensity = tokens.pop!float(0);

            entity.register!AtmosphereComponent(radius + depth, depth, density);
            auto atmosphereModel = new AtmosphereModel(radius + depth, colour, colourDensity);
            entity.register!AtmosphereModelComponent(atmosphereModel, radius + depth, colour, colourDensity);
        }
        else if (auto autoRightDepth = "autoRightDepth" in data)
        {
            entity.register!AtmosphereComponent(radius, radius + to!float(*autoRightDepth), 0);
        }
    }
}

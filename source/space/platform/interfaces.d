module space.platform.interfaces;

import space.multicastdelegate;

struct GameController
{
    string name;
    int instanceId;
    bool isGameController; // as opposed to a joystick
    bool connected;
}

enum GameControllerAxis
{
    Invalid = -1,
    LeftX = 0,
    LeftY = 1,
    RightX = 2,
    RightY = 3,
    TriggerLeft = 4,
    TriggerRight = 5,
}

enum GameControllerButton
{
    Invalid = -1,
    A = 0,
    B = 1,
    X = 2,
    Y = 3,
    Back = 4,
    Guide = 5,
    Start = 6,
    LeftStick = 7,
    RightStick = 8,
    LeftShoulder = 9,
    RightShoulder = 10,
    DpadUp = 11,
    DpadDown = 12,
    DpadLeft = 13,
    DpadRight = 14,
    Misc1 = 15,
    Paddle1 = 16,
    Paddle2 = 17,
    Paddle3 = 18,
    Paddle4 = 19,
    Touchpad = 20,
}

/**
 * A platform consists of window, video, audio, networking and so on.
 * Any of these subsystems may have a null implementation if necessary.
 */
interface PlatformInterface
{
    void setWindowTitle(string title);
    void startup();
    void shutdown();
    void finishRenderFrame();
    void handleEvents();

    MulticastDelegate!() getAfterInit();
    MulticastDelegate!() getBeforeShutdown();
    MulticastDelegate!() getOnQuit();
    MulticastDelegate!(int, int, bool) getOnVideoMode();
    MulticastDelegate!(string, bool) getOnKeyPress();
    MulticastDelegate!(int, int, bool) getOnMouseMotion();
    MulticastDelegate!(int, int, int, bool) getOnMouseButton();
    MulticastDelegate!(GameController, GameControllerAxis, float) getOnControllerAxis();
    MulticastDelegate!(GameController, GameControllerButton, bool) getOnControllerButton();
    MulticastDelegate!(string) getOnTextInput();
}

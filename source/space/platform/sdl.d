module space.platform.sdl;

import space.platform.interfaces;
import space.multicastdelegate;
import derelict.sdl2.sdl;
import derelict.opengl;
import std.algorithm;
import std.conv;
import std.stdio;
import std.string;

class SdlPlatform : PlatformInterface
{
    private SdlPlatformControllers controllers;

    private SDL_Window *sdlWindow = null;
    private SDL_GLContext glContext;
    private string windowTitle = "";
    private bool textInput = false;
    private bool mouseGrab = false;
    private int windowMiddleX, windowMiddleY;

    private auto afterInit = new MulticastDelegate!();
    private auto beforeShutdown = new MulticastDelegate!();
    private auto onQuit = new MulticastDelegate!();
    private auto onVideoMode = new MulticastDelegate!(int, int, bool);
    private auto onKeyPress = new MulticastDelegate!(string, bool);
    private auto onTextInput = new MulticastDelegate!(string);
    private auto onMouseMotion = new MulticastDelegate!(int, int, bool);
    private auto onMouseButton = new MulticastDelegate!(int, int, int, bool);
    private auto onControllerAxis = new MulticastDelegate!(GameController, GameControllerAxis, float);
    private auto onControllerButton = new MulticastDelegate!(GameController, GameControllerButton, bool);

    MulticastDelegate!() getAfterInit() { return afterInit; }
    MulticastDelegate!() getBeforeShutdown() { return beforeShutdown; }
    MulticastDelegate!() getOnQuit() { return onQuit; }
    MulticastDelegate!(int, int, bool) getOnVideoMode() { return onVideoMode; }
    MulticastDelegate!(string, bool) getOnKeyPress() { return onKeyPress; }
    MulticastDelegate!(string) getOnTextInput() { return onTextInput; }
    MulticastDelegate!(int, int, bool) getOnMouseMotion() { return onMouseMotion; }
    MulticastDelegate!(int, int, int, bool) getOnMouseButton() { return onMouseButton; }
    MulticastDelegate!(GameController, GameControllerAxis, float) getOnControllerAxis() { return onControllerAxis; }
    MulticastDelegate!(GameController, GameControllerButton, bool) getOnControllerButton() { return onControllerButton; }

    this(SdlPlatformControllers controllers)
    {
        this.controllers = controllers;

        DerelictSDL2.load();
        DerelictGL3.load();
    }

    void startup()
    {
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_JOYSTICK) < 0)
        {
            throw new Exception("Failed to initialize SDL: " ~ to!string(SDL_GetError()));
        }

        controllers.refreshControllers();

        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

        SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1");
        SDL_GL_SetSwapInterval(1);

        const int width = 320;
        const int height = 240;
        int[2] screenSize = [ width, height ];

        SDL_DisplayMode mode;
        if (SDL_GetDesktopDisplayMode(0, &mode) == 0)
        {
            int[2] modeSize = [ mode.w, mode.h ];
            screenSize = biggestFittingMultiple(screenSize, modeSize);
        }

        setVideoModeInternal(screenSize[0], screenSize[1], false);
        afterInit.call();
        onVideoMode.call(screenSize[0], screenSize[1], false);
    }

    void setWindowTitle(string title)
    {
        windowTitle = title;
        if (sdlWindow != null)
        {
            SDL_SetWindowTitle(sdlWindow, windowTitle.toStringz());
        }
    }

    void setVideoMode(int width, int height, bool fullscreen)
    {
        setVideoModeInternal(width, height, fullscreen);
        onVideoMode.call(width, height, false);
    }

    void setVideoModeInternal(int width, int height, bool fullscreen)
    {
        sdlWindow = SDL_CreateWindow(
            windowTitle.toStringz(),
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            width,
            height,
            SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
        );

        if (!sdlWindow)
        {
            throw new Exception("Failed to create an SDL window: " ~ to!string(SDL_GetError()));
        }

        glContext = SDL_GL_CreateContext(sdlWindow);
        if (! glContext)
        {
            throw new Exception("Failed to create an OpenGL context: " ~ to!string(SDL_GetError()));
        }

        windowMiddleX = width / 2;
        windowMiddleY = height / 2;
        if (mouseGrab)
        {
            SDL_WarpMouseInWindow(sdlWindow, windowMiddleX, windowMiddleY);
        }

        SDL_SetWindowResizable(sdlWindow, SDL_TRUE);

        DerelictGL3.reload();
    }

    void shutdown()
    {
        beforeShutdown.call();

        SDL_GL_DeleteContext(glContext);
        SDL_DestroyWindow(sdlWindow);
        SDL_Quit();
    }

    void finishRenderFrame()
    {
        SDL_GL_SwapWindow(sdlWindow);
    }

    void handleEvents()
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_WINDOWEVENT:
                handleWindowEvent(&event.window);
                break;
            case SDL_QUIT:
                onQuit.call();
                break;
            // Keyboard
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                if (!textInput || event.type == SDL_KEYUP)
                {
                    onKeyPress.call(
                        to!string(SDL_GetKeyName(event.key.keysym.sym)),
                        event.key.state == SDL_PRESSED
                    );
                }
                break;
            case SDL_TEXTINPUT:
                onTextInput.call(to!string(event.text.text));
                break;
            // Mouse
            case SDL_MOUSEMOTION:
                if (!mouseGrab || event.motion.x != windowMiddleX || event.motion.y != windowMiddleY)
                {
                    if (mouseGrab)
                    {
                        onMouseMotion.call(event.motion.xrel, event.motion.yrel, true);
                        SDL_WarpMouseInWindow(sdlWindow, windowMiddleX, windowMiddleY);
                    }
                    else
                    {
                        onMouseMotion.call(event.motion.x, event.motion.y, false);
                    }
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
                onMouseButton.call(event.button.x, event.button.y, event.button.button, event.type == SDL_MOUSEBUTTONDOWN);
                break;
            // Controllers added/removed
            case SDL_CONTROLLERDEVICEADDED:
            case SDL_CONTROLLERDEVICEREMOVED:
            case SDL_CONTROLLERDEVICEREMAPPED:
            case SDL_JOYDEVICEADDED:
            case SDL_JOYDEVICEREMOVED:
                controllers.refreshControllers();
                break;
            // Controller input
            case SDL_CONTROLLERAXISMOTION:
                onControllerAxis.call(
                    controllers.findController(event.caxis.which),
                    cast(GameControllerAxis)event.caxis.axis,
                    min(1.0f, max(-1.0f, cast(float)event.caxis.value / 32768.0f)),
                );
                break;
            case SDL_CONTROLLERBUTTONDOWN:
            case SDL_CONTROLLERBUTTONUP:
                onControllerButton.call(
                    controllers.findController(event.cbutton.which),
                    cast(GameControllerButton)event.cbutton.button,
                    event.type == SDL_CONTROLLERBUTTONDOWN
                );
                break;
            // TODO: Regular joysticks? We handle these slightly differently from a controller.
            // Ignore the rest.
            default:
                break;
            }
        }
    }

    void handleWindowEvent(const SDL_WindowEvent *window)
    {
        switch (window.event)
        {
        case SDL_WINDOWEVENT_RESIZED:
            onVideoMode.call(window.data1, window.data2, false);
            break;
        default:
            break;
        }
    }

    const(const(GameController)[]) getControllers()
    {
        return controllers.getControllers();
    }

    void setTextInput(bool enabled)
    {
        textInput = enabled;
        if (textInput)
        {
            SDL_StartTextInput();
        }
        else
        {
            SDL_StopTextInput();
        }
    }

    void setMouseGrab(bool grab)
    {
        mouseGrab = grab;
        if (grab)
        {
            SDL_CaptureMouse(SDL_TRUE);
            SDL_ShowCursor(SDL_FALSE);
            SDL_WarpMouseInWindow(sdlWindow, windowMiddleX, windowMiddleY);
        }
        else
        {
            SDL_CaptureMouse(SDL_FALSE);
            SDL_ShowCursor(SDL_TRUE);
        }
    }

    private int[2] biggestFittingMultiple(int[2] original, int[2] bounds)
    {
        while (original[0] * 2 < bounds[0] && original[1] * 2 < bounds[1])
        {
            original[] *= 2;
        }
        return original;
    }
}

/**
 * Detects and maintains a list of game controllers.
 */
class SdlPlatformControllers
{
    struct SdlController
    {
        string guid;
        SDL_Joystick* joystick;
        SDL_GameController* gamepad;
        SDL_JoystickID instanceId;
        string name;
        bool connected;
    }

    private SdlController[] controllers;
    private GameController[] interfaceControllers;

    this()
    {
    }

    const(const(GameController)[]) getControllers()
    {
        return interfaceControllers;
    }

    void refreshControllers()
    {
        writeln("Gamepad devices update:");

        for (int i = 0; i < controllers.length; i += 1)
        {
            controllers[i].connected = false;
        }

        int num = SDL_NumJoysticks();
        for (int i = 0; i < num; i += 1)
        {
            string guid = getControllerGuid(i);
            auto controller = findController(guid);
            if (controller !is null)
            {
                controller.connected = true;
            }
            else
            {
                controllers ~= createController(guid, i);
            }
        }

        for (int i = 0; i < controllers.length; i += 1)
        {
            controllers[i].instanceId = SDL_JoystickInstanceID(controllers[i].joystick);
        }

        writeln(to!string(num) ~ " controllers connected");
        foreach (controller; controllers)
        {
            write(to!string(controller.guid));
            if (controller.gamepad !is null)
            {
                write(": [controller] \"");
            }
            else
            {
                write(": \"");
            }
            write(controller.name);
            write("\"");
            if (controller.connected)
            {
                write(" connected");
            }
            write("\n");
        }

        interfaceControllers = [];
        foreach (controller; controllers)
        {
            GameController interfaceController = {
                controller.name,
                controller.instanceId,
                controller.gamepad !is null,
                controller.connected,
            };
            interfaceControllers ~= interfaceController;
        }
    }

    private char hexChar(ubyte i)
    {
        if (i <= 10)
        {
            return cast(char)('0' + i);
        }
        return cast(char)('a' + i - 10);
    }

    private string getControllerGuid(int index)
    {
        auto guid = SDL_JoystickGetDeviceGUID(index);
        char[] buffer;
        for (int i = 0; i < 16; i += 1)
        {
            buffer ~= hexChar((guid.data[i] & 0xf0) >> 4);
            buffer ~= hexChar(guid.data[i] & 0x0f);
        }
        return to!string(buffer);
    }

    private SdlController* findController(string guid)
    {
        for (int i = 0; i < controllers.length; i += 1)
        {
            if (controllers[i].guid == guid)
            {
                return &controllers[i];
            }
        }
        return null;
    }

    private GameController findController(int instanceId)
    {
        for (int i = 0; i < interfaceControllers.length; i += 1)
        {
            if (interfaceControllers[i].instanceId == instanceId)
            {
                return interfaceControllers[i];
            }
        }

        GameController nullController;
        return nullController;
    }

    private SdlController createController(string guid, int joystick_index)
    {
        SdlController controller;
        controller.guid = guid;
        if (SDL_IsGameController(joystick_index))
        {
            controller.joystick = SDL_JoystickOpen(joystick_index);
            controller.gamepad = SDL_GameControllerOpen(joystick_index);
            controller.name = to!string(SDL_GameControllerName(controller.gamepad));
        }
        else
        {
            controller.joystick = SDL_JoystickOpen(joystick_index);
            controller.gamepad = null;
            controller.name = to!string(SDL_JoystickName(controller.joystick));
        }
        controller.connected = true;

        return controller;
    }
}

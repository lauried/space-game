module space.input.keybinds;

import std.digest.crc;

/**
 * Key names are combined with a context name, and that combination maps to an action.
 * This allows multiple keys to have the same action, allowiwng for multiple input methods and other conveniences,
 * but also lets one key trigger multiple actions, but only in different contexts.
 */
class KeyBinds
{
    struct Bindable
    {
        string key;
        string context;

        extern (D) size_t toHash() const nothrow @safe
        {
            return hashString(key) ^ hashString(context);
        }

        bool opEquals(ref const Bindable other) const
        {
            return other.key == this.key && other.context == this.context;
        }

        private size_t hashString(string str) const nothrow @safe
        {
            if (str is null)
            {
                return 0;
            }
            ubyte[4] hash = crc32Of(str);
            return ((hash[0] << 24) | (hash[1] << 16) | (hash[2] << 8) | hash[3]);
        }
    }

    private string[Bindable] binds;

    this()
    {
    }

    void bind(string key, string context, string action)
    {
        Bindable index = Bindable(key, context);
        binds[index] = action;
    }

    void unbind(string key, string context)
    {
        Bindable index = Bindable(key, context);
        binds.remove(index);
    }

    string getBoundAction(string key, string context)
    {
        Bindable index = Bindable(key, context);
        if (index in binds)
        {
            return binds[index];
        }
        return "";
    }

    string[] getBoundActions(string key)
    {
        string[] actions = [];
        foreach (pair; binds.byKeyValue())
        {
            if (pair.key.key == key)
            {
                actions.length += 1;
                actions[actions.length - 1] = pair.value;
            }
        }
        return actions;
    }

    Bindable[] getBoundKeys(string action)
    {
        Bindable[] keys = [];
        foreach (pair; binds.byKeyValue())
        {
            if (pair.value == action)
            {
                keys.length += 1;
                keys[keys.length - 1] = pair.key;
            }
        }
        return keys;
    }
}
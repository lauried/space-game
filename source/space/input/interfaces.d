module space.input.interfaces;

import space.platform.interfaces;

/**
 * Translates from device inputs into something meaningful for a type of
 * control scheme, e.g. first person shooter, racing, flying, etc.
 * Deals with keyboard bindings and various control options.
 */
interface ControlSchemeInterface
{
    void onControlValue(string name, float value);

    void endFrame();
    void onActive();
    void onInactive();
}

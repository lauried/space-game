module space.input.actionstates;

/**
 * For keeping track of the state of a key on a key press event, or an abstract thing like an action bound to a key.
 * This can be generalised later to deal with all input types.
 */
class ActionStates
{
    // TODO: Add delegates for impulse events.
    struct State
    {
        bool pressed = false;
        bool changed = false;
        float value = 0.0f;
    }

    private State*[string] states;

    this()
    {
    }

    void addState(string name, State* state)
    {
        states[name] = state;
    }

    void setState(string name, float value)
    {
        if (name in states)
        {
            auto state = states[name];
            state.value = value;
            bool pressed = value > 0.5;
            state.changed = state.pressed != pressed;
            state.pressed = pressed;
        }
    }

    void clearStateChangedFlags()
    {
        foreach (state; states.byValue())
        {
            state.changed = false;
        }
    }
}

module space.input.shipcontrolscheme;

import space.input.interfaces;
import space.input.controlhandler;
import space.input.actionstates;
import space.input.keybinds;
import space.platform.interfaces;

class ShipControlScheme : ControlSchemeInterface
{
    private ActionStates actionStates;
    private KeyBinds keyBinds;
    private ControlHandler controlHandler;

    ActionStates.State pitchDown;
    ActionStates.State pitchUp;
    ActionStates.State turnLeft;
    ActionStates.State turnRight;
    ActionStates.State rollLeft;
    ActionStates.State rollRight;
    ActionStates.State lookLeft;
    ActionStates.State lookRight;
    ActionStates.State lookUp;
    ActionStates.State lookDown;
    ActionStates.State throttleUp;
    ActionStates.State throttleDown;
    ActionStates.State toggleThrustMode;
    ActionStates.State cycleAutoLevel;

    this()
    {
        actionStates = new ActionStates();
        keyBinds = new KeyBinds();
        controlHandler = new ControlHandler(keyBinds, actionStates);

        actionStates.addState("pitch_down", &pitchDown);
        actionStates.addState("pitch_up", &pitchUp);
        actionStates.addState("turn_left", &turnLeft);
        actionStates.addState("turn_right", &turnRight);
        actionStates.addState("roll_left", &rollLeft);
        actionStates.addState("roll_right", &rollRight);
        actionStates.addState("look_left", &lookLeft);
        actionStates.addState("look_right", &lookRight);
        actionStates.addState("look_up", &lookUp);
        actionStates.addState("look_down", &lookDown);
        actionStates.addState("throttle_up", &throttleUp);
        actionStates.addState("throttle_down", &throttleDown);
        actionStates.addState("toggle_thrust_mode", &toggleThrustMode);
        actionStates.addState("cycle_auto_level", &cycleAutoLevel);

        keyBinds.bind("GamePadLeftStickUp", "", "pitch_down");
        keyBinds.bind("GamePadLeftStickDown", "", "pitch_up");
        keyBinds.bind("GamePadLeftStickLeft", "", "turn_left");
        keyBinds.bind("GamePadLeftStickRight", "", "turn_right");
        // keyBinds.bind("GamePadRightStickUp", "", "look_up");
        // keyBinds.bind("GamePadRightStickDown", "", "look_down");
        // keyBinds.bind("GamePadRightStickLeft", "", "look_left");
        // keyBinds.bind("GamePadRightStickRight", "", "look_right");
        keyBinds.bind("GamePadRightShoulder", "", "roll_right");
        keyBinds.bind("GamePadLeftShoulder", "", "roll_left");
        keyBinds.bind("GamePadTriggerRight", "", "throttle_up");
        keyBinds.bind("GamePadTriggerLeft", "", "throttle_down");
        keyBinds.bind("GamePadY", "", "cycle_auto_level");
        keyBinds.bind("GamePadX", "", "toggle_thrust_mode");

        keyBinds.bind("W", "", "pitch_down");
        keyBinds.bind("S", "", "pitch_up");
        keyBinds.bind("A", "", "turn_left");
        keyBinds.bind("D", "", "turn_right");
        keyBinds.bind("Q", "", "roll_left");
        keyBinds.bind("E", "", "roll_right");
        // keyBinds.bind("J", "", "look_left");
        // keyBinds.bind("L", "", "look_right");
        // keyBinds.bind("I", "", "look_up");
        // keyBinds.bind("K", "", "look_down");
        keyBinds.bind("I", "", "throttle_up");
        keyBinds.bind("K", "", "throttle_down");
        keyBinds.bind("O", "", "toggle_thrust_mode");
        keyBinds.bind("U", "", "cycle_auto_level");
    }

    void onControlValue(string name, float value)
    {
        controlHandler.onControlValue(name, value);
    }

    void endFrame()
    {
        actionStates.clearStateChangedFlags();
    }

    void onActive()
    {
    }

    void onInactive()
    {
        // TODO: Zero all our controls.
    }
}

module space.input.inputcontroller;

import space.input.interfaces;
import space.platform.interfaces;

import std.algorithm;
import std.conv;

/**
 * Detects and filters different devices.
 * This is where we know what input methods are being used.
 *
 * @todo Device activity detection.
 */
class InputController
{
    NullControlScheme nullScheme;
    ControlSchemeInterface scheme;

    this(PlatformInterface platform)
    {
        scheme = nullScheme = new NullControlScheme();

        platform.getOnKeyPress().add(delegate(string key, bool pressed) { onKeyPress(key, pressed); });
        platform.getOnMouseMotion().add(delegate(int x, int y, bool relative) { onMouseMotion(x, y, relative); });
        platform.getOnMouseButton().add(delegate(int x, int y, int button, bool pressed) { onMouseButton(x, y, button, pressed); });
        platform.getOnControllerAxis().add(delegate(GameController controller, GameControllerAxis axis, float value) { onControllerAxis(controller, axis, value); });
        platform.getOnControllerButton().add(delegate(GameController controller, GameControllerButton button, bool pressed) { onControllerButton(controller, button, pressed); });
        platform.getOnTextInput().add(delegate(string text) { onTextInput(text); });
    }

    void setControlScheme(ControlSchemeInterface scheme)
    {
        if (scheme is null)
        {
            scheme = nullScheme;
        }
        this.scheme.endFrame();
        this.scheme.onInactive();
        scheme.onActive();
        this.scheme = scheme;
    }

    void endFrame()
    {
        scheme.endFrame();
    }

    void onKeyPress(string key, bool pressed)
    {
        scheme.onControlValue(key, pressed ? 1.0f : 0.0f);
    }

    void onMouseMotion(int x, int y, bool relative)
    {
    }

    void onMouseButton(int x, int y, int button, bool pressed)
    {
    }

    void onControllerAxis(GameController controller, GameControllerAxis axis, float value)
    {
        value = deadZone(axis, value);
        float positiveValue;
        float negativeValue;
        if (value > 0)
        {
            positiveValue = value;
            negativeValue = 0;
        }
        else
        {
            positiveValue = 0;
            negativeValue = -value;
        }
        scheme.onControlValue(positiveAxisName(axis), positiveValue);
        scheme.onControlValue(negativeAxisName(axis), negativeValue);
    }

    private float deadZone(GameControllerAxis axis, float value)
    {
        if (value < 0.1 && value > -0.1)
        {
            return 0;
        }
        if (value > 0)
        {
            return min(1.0f, max(-1.0f, value * 1.11 - 0.1));
        }
        return min(1.0f, max(-1.0f, value * 1.11 + 0.1));
    }

    private string positiveAxisName(GameControllerAxis axis)
    {
        switch (axis)
        {
            case GameControllerAxis.LeftX:
                return "GamePadLeftStickRight";
            case GameControllerAxis.LeftY:
                return "GamePadLeftStickDown";
            case GameControllerAxis.RightX:
                return "GamePadRightStickRight";
            case GameControllerAxis.RightY:
                return "GamePadRightStickDown";
            case GameControllerAxis.TriggerLeft:
                return "GamePadTriggerLeft";
            case GameControllerAxis.TriggerRight:
                return "GamePadTriggerRight";
            default:
                return "Unknown";
        }
    }

    private string negativeAxisName(GameControllerAxis axis)
    {
        switch (axis)
        {
            case GameControllerAxis.LeftX:
                return "GamePadLeftStickLeft";
            case GameControllerAxis.LeftY:
                return "GamePadLeftStickUp";
            case GameControllerAxis.RightX:
                return "GamePadRightStickLeft";
            case GameControllerAxis.RightY:
                return "GamePadRightStickUp";
            case GameControllerAxis.TriggerLeft:
                return "GamePadTriggerLeftImpossible";
            case GameControllerAxis.TriggerRight:
                return "GamePadTriggerRightImpossible";
            default:
                return "Unknown";
        }
    }

    void onControllerButton(GameController controller, GameControllerButton button, bool pressed)
    {
        scheme.onControlValue("GamePad" ~ to!string(button), pressed ? 1.0f : 0.0f);
    }

    void onTextInput(string text)
    {
    }
}

class NullControlScheme : ControlSchemeInterface
{
    void onControlValue(string name, float value) {}

    void endFrame() {}
    void onActive() {}
    void onInactive() {}
}

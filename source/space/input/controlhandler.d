module space.input.controlhandler;

import space.platform.interfaces;
import space.input.actionstates;
import space.input.keybinds;

class ControlHandler
{
    private KeyBinds keybinds;
    private ActionStates actionstates;
    private string context = "";

    this(KeyBinds keybinds, ActionStates actionstates)
    {
        this.keybinds = keybinds;
        this.actionstates = actionstates;
    }

    void setContext(string context)
    {
        this.context = context;
    }

    void onControlValue(string name, float value)
    {
        // TODO: Remove contexts.

        string[] actions;
        actions = [ keybinds.getBoundAction(name, context) ];
        
        foreach (action; actions)
        {
            actionstates.setState(action, value);
        }
    }
}

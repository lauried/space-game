
module space.dependency;

import space.exceptions;
import std.traits;

/**
 * Simple autowiring DI container.
 * Classes which are instantiated by the container must implement a constructor which accepts their dependencies.
 * The constructor must accept only objects (or interfaces) as parameters.
 * If a class has no dependencies, it must still implement a constructor in order to be instantiated.
 */
class DIContainer
{
    private interface InstantiatorInterface
    {
        string getTypeName();
        Object createInstance(DIContainer container);
    }

    private class Instantiator(T) : InstantiatorInterface
    {
        string getTypeName()
        {
            return fullyQualifiedName!T;
        }

        Object createInstance(DIContainer container)
        {
            static if (__traits(compiles, __traits(getOverloads, T, `__ctor`)))
            {
                bool foundConstructor = false;
                T instance;
                foreach (constructor;  __traits(getOverloads, T, `__ctor`))
                {
                    mixin(createImports!(Parameters!constructor));
                    mixin(
                        "import " ~ moduleName!T ~ "; " ~
                        "instance = new " ~ T.stringof ~ "(" ~ createArguments!(Parameters!constructor) ~ ");"
                    );
                    foundConstructor = true;
                    break;
                }
                if (foundConstructor)
                {
                    return instance;
                }
                throw new LogicException("None of the constructors worked for " ~ fullyQualifiedName!T);
            }
            else
            {
                throw new LogicException(
                    "Cannot instantiate " ~ fullyQualifiedName!T ~ ". Does it have a constructor?"
                );
            }
        }

        private static string createArguments(Params...)()
        {
            string arguments = "";
            foreach(param; Params)
            {
                if (arguments.length > 0)
                {
                    arguments ~= ", ";
                }
                arguments ~= "container.resolve!(" ~ param.stringof ~ ")";
            }
            return arguments;
        }

        private static string createImports(Params...)()
        {
            string imports;
            foreach (param; Params)
            {
                imports ~= "import " ~ moduleName!param ~ "; ";
            }
            return imports;
        }
    }

    private Object[string] instances;
    private InstantiatorInterface[string] instantiators;

    /**
     * Retrieves the instance of class or interface T.
     * The same instance is returned every time a T is requested.
     */
    T resolve(T)()
    {
        string name = fullyQualifiedName!T;

        if (name !in instantiators)
        {
            instantiators[name] = new Instantiator!(T)();
        }
        auto instantiator = instantiators[name];
        string instanceName = instantiator.getTypeName();
        if (instanceName !in instances)
        {
            instances[instanceName] = instantiator.createInstance(this);
        }
        return cast(T)instances[instanceName];
    }

    /**
     * Tells the container that whenever interface/superclass T is requested, to return the instance of U.
     */
    void register(T, U)()
    {
        string name = fullyQualifiedName!T;
        if (name in instantiators)
        {
            throw new LogicException(name ~ " is already registered");
        }
        instantiators[name] = new Instantiator!(U)();
    }
}

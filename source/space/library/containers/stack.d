module space.library.containers.stack;

struct Stack(T)
{
    T[] items;
    T nv;

    void push(T val)
    {
        items.length += 1;
        items[items.length - 1] = val;
    }

    T peek()
    {
        if (items.length < 1)
        {
            return nv;
        }
        return items[items.length - 1];
    }

    T pop()
    {
        T item = peek();
        items.length -= 1;
        return item;
    }

    ulong size()
    {
        return items.length;
    }
}

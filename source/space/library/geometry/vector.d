module space.library.geometry.vector;

import std.math;

version (unittest)
{
    import std.algorithm.iteration;
}

T length(T)(const T[3] v)
{
    return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

T[3] normalize(T)(const T[3] v)
{
    T scale = 1.0 / length(v);
    return [ v[0] * scale, v[1] * scale, v[2] * scale ];
}

T[3] normalize(T)(const T[3] v, ref T len)
{
    len = length(v);
    T scale = 1.0 / len;
    return [ v[0] * scale, v[1] * scale, v[2] * scale ];
}

T dotProduct(T)(const T[3] a, const T[3] b)
{
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

/**
 * '1 0 0' × '0 1 0' = '0 0 1'
 */
T[3] crossProduct(T)(const T[3] a, const T[3] b)
{
    return [
        a[1]*b[2] - a[2]*b[1],
        a[2]*b[0] - a[0]*b[2],
        a[0]*b[1] - a[1]*b[0],
    ];
}

/**
 * Returns the point's distance in front of the plane.
 * This will be zero for a point exactly on the plane (though floating point
 * accuracy makes checking for this in particular meaningless), and negative
 * if the point is behind the plane.
 */
T planeCheck(T)(const T[3] point, const T[4] plane)
{
    return dotProduct(point, plane[0..3]) - plane[3];
}

/**
 * Returns a plane whose normal is AB × BC.
 * See crossProduct function.
 */
T[4] planeForPoints(T)(const T[3] a, const T[3] b, const T[3] c)
{
    T[3] ab = b[] - a[];
    T[3] bc = c[] - b[];
    T[4] plane;
    plane[0..3] = normalize(crossProduct(ab, bc));
    plane[3] = dotProduct(plane[0..3], a);
    return plane;
}

/**
 * Returns a vector which is guaranteed not to be parallel to the given one.
 */
T[3] getNonParallelVector(T)(const T[3] vec)
{
    if (vec[2] != 0 || vec[1] != 0)
    {
        return [ 0, vec[2], -vec[1] ];
    }
    return [ -vec[2], 0, vec[0] ];
}

/**
 * An instance of this can be used to rotate vectors about another vector by
 * any angle.
 */
struct RotateVectorAroundAxis(T)
{
    T[3] forward, right, up;

    this(const T[3] axis)
    {
        // Set forward/right/up such that up is our axis.
        up = normalize(axis);
        // Find a different vector and flatten it so it's a point on the plane, then normalize it.
        forward = getNonParallelVector!T(up);
        forward[] -= dotProduct!T(forward, up) * up[];
        forward = normalize!T(forward);
        // Crossproduct to get the right vector.
        right = crossProduct!T(up, forward);
    }

    /**
     * Rotates a vector around the axis we were constructed with.
     * Clockwise while looking along the axis, or as though holding the positive axis and twisting counterclockwise.
     */
    T[3] rotateVector(const T[3] vec, T angle)
    {
        // To axis space.
        T[3] avec = [
            dotProduct!T(vec, forward),
            dotProduct!T(vec, right),
            dotProduct!T(vec, up),
        ];
        // Rotate in 2D around up.
        T c = cos(-angle);
        T s = sin(-angle);
        T[3] rotated = [
            c * avec[0] - s * avec[1],
            c * avec[1] + s * avec[0],
            avec[2],
        ];
        // Back to (probably) world space.
        T[3] result = forward[] * rotated[0] + right[] * rotated[1] + up[] * rotated[2];
        return result;
    }
}

struct SkewLineIntersect(T)
{
    float[3] p1, d1, p2, d2;
    float dist1, dist2;
    bool parallel;

    this(const float[3] p1, const float[3] dir1, const float[3] p2, const float[3] dir2)
    {
        d1 = normalize!T(dir1);
        d2 = normalize!T(dir2);

        T[3] n = crossProduct!T(d1, d2);
        T[3] n1 = crossProduct!T(d1, n);
        T[3] n2 = crossProduct!T(d2, n);

        T[3] p12 = p1[] - p2[];
        T[3] p21 = -p12[];

        float divisor1 = dotProduct!T(d1, n2);
        float divisor2 = dotProduct!T(d2, n1);

        if (divisor1 == 0 || divisor2 == 0)
        {
            dist1 = 0;
            dist2 = 0;
            parallel = true;
            return;
        }

        dist1 = dotProduct!T(p21, n2) / divisor1;
        dist2 = dotProduct!T(p12, n1) / divisor2;
    }

    float[3] getClosest1()
    {
        float[3] result = p1[] + (d1[] * dist1);
        return result;
    }
    
    float[3] getClosest2()
    {
        float[3] result = p2[] + (d2[] * dist2);
        return result;
    }
}

unittest
{
    void testRotate(float[3] axis, float angle, float[3] vector, float[3] result)
    {
        auto rotate = RotateVectorAroundAxis!float(axis);
        float[3] gotResult = rotate.rotateVector(vector, angle);
        float[3] diff = gotResult[] - result[];
        assert(length(diff) < 0.01);
    }

    const float pi = 3.14159265359;
    // Rotation around vertical axis
    testRotate([ 0, 0, 1 ], pi * -0.5, [ 1, 0, 0 ], [ 0, 1, 0 ]);
    testRotate([ 0, 0, 1 ], pi * -0.25, [ 1, 0, 0 ], [ 0.707107, 0.707107, 0 ]);
    testRotate([ 0, 0, 1 ], pi * -0.125, [ 1, 0, 0 ], [ 0.92388, 0.382683, 0 ]);
    // Test points on the axis are treated correctly
    testRotate([ 0, 0, 1 ], pi * -0.5, [ 0, 0, 1 ], [ 0, 0, 1 ]);
}

unittest
{
    float[3] p1 = [ 1, 0, 2 ];
    float[3] p2 = [ 1, 1, 2 ];
    float[3] p3 = [ 0, 1, 2 ];
    float[4] plane = planeForPoints(p1, p2, p3);
    assert(plane[2] == 1, "Expected plane[2] to be 1");
    assert(plane[3] == 2, "Expected plane[3] to be 2");
}
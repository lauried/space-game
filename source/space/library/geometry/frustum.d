module space.library.geometry.frustum;

import space.library.geometry.orientation;
import space.library.geometry.vector;

struct Frustum(T)
{
    T[3] position;
    T[4][6] faces;

    enum Face : int
    {
        LEFT   = 0,
        RIGHT  = 1,
        BOTTOM = 2,
        TOP    = 3,
        NEAR   = 4,
        FAR    = 5,
    }

    bool contains(float[3] position, float radius = 0, bool noNearFar = false)
    {
        float maxFace = noNearFar ? 4 : 6;
        for (int face = 0; face < maxFace; face += 1)
        {
            if (dotProduct!float(position, faces[face][0..3]) + radius > faces[face][3])
            {
                return true;
            }
        }

        return false;
    }

    static Frustum!T fromOrientation(Orientation!T ori, float hFov, float vFov, float near, float far)
    {
        Frustum!T frustum;

        frustum.position = ori.position;

        frustum.faces[Face.LEFT][0..3] = ori.forward[] * hFov + ori.right[];
        frustum.faces[Face.LEFT][0..3] = normalize!T(frustum.faces[Face.LEFT][0..3]);
        frustum.faces[Face.LEFT][3] = dotProduct!T(frustum.faces[Face.LEFT][0..3], frustum.position);

        frustum.faces[Face.RIGHT][0..3] = ori.forward[] * hFov - ori.right[];
        frustum.faces[Face.RIGHT][0..3] = normalize!T(frustum.faces[Face.RIGHT][0..3]);
        frustum.faces[Face.RIGHT][3] = dotProduct!T(frustum.faces[Face.RIGHT][0..3], frustum.position);

        frustum.faces[Face.BOTTOM][0..3] = ori.forward[] * vFov + ori.up[];
        frustum.faces[Face.BOTTOM][0..3] = normalize!T(frustum.faces[Face.BOTTOM][0..3]);
        frustum.faces[Face.BOTTOM][3] = dotProduct!T(frustum.faces[Face.BOTTOM][0..3], frustum.position);

        frustum.faces[Face.TOP][0..3] = ori.forward[] * vFov - ori.up[];
        frustum.faces[Face.TOP][0..3] = normalize!T(frustum.faces[Face.TOP][0..3]);
        frustum.faces[Face.TOP][3] = dotProduct!T(frustum.faces[Face.TOP][0..3], frustum.position);

        frustum.faces[Face.NEAR][0..3] = ori.forward;
        frustum.faces[Face.NEAR][3] = dotProduct!T(frustum.faces[Face.NEAR][0..3], frustum.position) + near;

        frustum.faces[Face.FAR][0..3] = ori.forward[] * -1;
        frustum.faces[Face.FAR][3] = dotProduct!T(frustum.faces[Face.FAR][0..3], frustum.position) - far;

        return frustum;
    }
}


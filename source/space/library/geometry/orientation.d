module space.library.geometry.orientation;

import space.library.geometry.vector;
import std.math;

version (unittest)
{
    import std.algorithm.iteration;
}

struct Orientation(T)
{
    T[3] forward;
    T[3] right;
    T[3] up;
    T[3] position;

    /**
     * Returns an orientation whose axes match world space.
     */
    static Orientation!T identity()
    {
        Orientation ori;
        ori.forward = [ 1, 0, 0 ];
        ori.right = [ 0, 1, 0 ];
        ori.up = [ 0, 0, 1 ];
        ori.position = [ 0, 0, 0 ];
        return ori;
    }

    static Orientation!T fromPosition(float[3] position)
    {
        auto ori = Orientation!T.identity();
        ori.position = position;
        return ori;
    }

    static Orientation!T fromPositionEuler(float[3] position, float[3] euler)
    {
        auto ori = Orientation!T.fromPosition(position);
        ori.rotateZ(euler[2]);
        ori.rotateY(euler[1]);
        ori.rotateX(euler[0]);
        return ori;
    }

    /**
     * Rotate clockwise looking along Z axis or, if you could grab +Z, twiddle it counterclockwise.
     */
    void rotateZ(T angle)
    {
        const T s = sin(angle);
        const T c = cos(angle);
        const T[3] newForward = forward[] * c - right[] * s;
        forward = normalize!T(newForward);
        right = normalize!T(crossProduct!T(up, forward));
    }

    /**
     * Rotate clockwise looking along Y axis or, if you could grab +Y, twiddle it counterclockwise.
     */
    void rotateY(T angle)
    {
        const T s = sin(angle);
        const T c = cos(angle);
        const T[3] newForward = forward[] * c + up[] * s;
        forward = normalize!T(newForward);
        up = normalize!T(crossProduct!T(forward, right));
    }

    /**
     * Rotate clockwise looking along X axis or, if you could grab +X, twiddle it counterclockwise.
     */
    void rotateX(T angle)
    {
        const T s = sin(angle);
        const T c = cos(angle);
        const T[3] newRight = right[] * c - up[] * s;
        right = normalize!T(newRight);
        up = normalize!T(crossProduct!T(forward, right));
    }

    /**
     * Move in a direction in the orientation's space.
     */
    void moveRelative(T[3] direction)
    {
        position[] += (forward[] * direction[0]) + (right[] * direction[1]) + (up[] * direction[2]);
    }

    T[3] directionToInternalSpace(T[3] dir) const
    {
        return [
            dotProduct(dir, forward),
            dotProduct(dir, right),
            dotProduct(dir, up),
        ];
    }

    T[3] pointToInternalSpace(T[3] point) const
    {
        T[3] relative = point[] - position[];
        return directionToInternalSpace(relative);
    }

    T[3] directionToExternalSpace(T[3] dir) const
    {
        T[3] external = forward[] * dir[0] + right[] * dir[1] + up[] * dir[2];
        return external;
    }

    T[3] pointToExternalSpace(T[3] point) const
    {
        T[3] external = position[] + directionToExternalSpace(point)[];
        return external;
    }

    Orientation!T orientationToInternalSpace(Orientation!T ori) const
    {
        Orientation!T newOri;
        newOri.position = pointToInternalSpace(ori.position);
        newOri.forward = directionToInternalSpace(ori.forward);
        newOri.right = directionToInternalSpace(ori.right);
        newOri.up = directionToInternalSpace(ori.up);
        return newOri;
    }

    Orientation!T orientationToExternalSpace(Orientation!T ori) const
    {
        Orientation!T newOri;
        newOri.position = pointToExternalSpace(ori.position);
        newOri.forward = directionToExternalSpace(ori.forward);
        newOri.right = directionToExternalSpace(ori.right);
        newOri.up = directionToExternalSpace(ori.up);
        return newOri;
    }
}

unittest
{
    const float pi = 3.14159265358979;

    auto ori1 = Orientation!float.identity();

    ori1.moveRelative([ 1.0f, 1.0f, 1.0f ]);

    for (auto i = 0; i < 8; i += 1)
    {
        ori1.rotateZ(pi * 0.25);
    }

        for (auto i = 0; i < 8; i += 1)
    {
        ori1.rotateY(pi * 0.25);
    }

        for (auto i = 0; i < 8; i += 1)
    {
        ori1.rotateX(pi * 0.25);
    }

    float[3] diff;
    
    diff = ori1.forward[] - [ 1, 0, 0 ];
    assert(sum(diff[0..3]) < 0.001);

    diff = ori1.right[] - [ 0, 1, 0 ];
    assert(sum(diff[0..3]) < 0.001);

    diff = ori1.up[] - [ 0, 0, 1 ];
    assert(sum(diff[0..3]) < 0.001);

    diff = ori1.position[] - [ 1, 1, 1 ];
    assert(sum(diff[0..3]) < 0.001);
}
module space.library.geometry.matrix;

version (unittest)
{
    import std.algorithm.iteration;
}

/*
Stupid complicated matrix maths.
It's because it can be flipped in notation, memory, and the GPU and CPU
notations.

In memory, OpenGL and DirectX both expect the axis vectors one after each
other, followed by the translation vector.

Somewhere said 4D vectors can be treated as having a w component which is 0 for
a direction and 1 for a position.

Now our matrix can look like:
x.x  x.y  x.z  0
y.x  y.y  y.z  0
z.x  z.y  z.z  0
p.x  p.y  p.z  1
And in memory:
{ x.x, x.y, x.z, 0, y.x, y.y, y.z, 0, z.x, z.y, z.z, 0, p.x, p.y, p.z, 1 }

Our output vector has to be the sum of columns, because that's how it gets the
translation added to it if it's a position.

That means the first three rows are vectors that get scaled by the first
three components of the input vector respectively, and added into the output
vector, meaning that the four rows represent the orientation and position of
the space we're transforming from, relative to the space we're transforming to.

The other confusing thing is that if matrices are multiplied together, the one
multiplied leftmost is applied first.

I suspect that transposing the three components and negating the translation
would create a matrix that does the opposite, because columns of the components
are dot-producted with the vector.

The only problem to solve now is how to implement it.
It'll be nice to just have it in memory. We could return slices to individual
rows or something, to get coordinates.

We could just implement arrays however we like, and copy it out into memory
to send to OpenGL, because we should rarely actually send them to OpenGL.

Multiplication:
For each target cell, dotproduct the corresponding row of one with the column
of the other, but which is which depends on the way our matrix is represented.
An example used rows of the first with columns of the second.

We can check if we cleverly craft some matrices to see what order things are
performed in, say if we do a scale and a translation:

Here we'd expect the result to translate by 1 and then scale by 2.
All we need to check is the translate figure.

scale      translate       rows×cols       cols×rows
2 0 0 0    1 0 0 0         
0 2 0 0    0 1 0 0         
0 0 2 0    0 0 1 0         
0 0 0 1    0 1 0 1         0 1 0 1         0 2 0 1

Therefore, it seems like columns of the first followed by rows of the last,
which means our matrices are oriented the right way round in notation.

Now we can do the maths where we like. We could theoretically render
homogeneous coordinates, if we did all the matrix maths in our single vector.

We could do model+camera and put projection into OpenGL, never having to change
projection.

We could put camera into OpenGL every frame, because it wouldn't be that
costly, and it means we only do model transformations on the CPU.

Best of all, we can have representations of all the matrices for various uses,
such as culling or projecting 3D coordinates into 2D.

Need to figure out the most modern way of storing them.
We can let our matrix ops act immutably, because that's simple.
Our vector ops might want to use pointers or slices, because we might want to
modify or copy arrays.
*/

struct Matrix(T)
{
    // Addressed [row][column]
    T[4][4] m;

    static Matrix!T zero()
    {
        Matrix!T mat;
        mat.m = [
            [ 0.0, 0.0, 0.0, 0.0 ],
            [ 0.0, 0.0, 0.0, 0.0 ],
            [ 0.0, 0.0, 0.0, 0.0 ],
            [ 0.0, 0.0, 0.0, 0.0 ],
        ];
        return mat;
    }

    static Matrix!T identity()
    {
        Matrix!T mat;
        mat.m = [
            [ 1.0, 0.0, 0.0, 0.0 ],
            [ 0.0, 1.0, 0.0, 0.0 ],
            [ 0.0, 0.0, 1.0, 0.0 ],
            [ 0.0, 0.0, 0.0, 1.0 ],
        ];
        return mat;
    }

    /**
     * Takes the orientation of a model.
     * Returns a matrix that transforms from model space into world space.
     * It's the complement of a camera matrix.
     */
    static Matrix!T model(const T[] forward, const T[] right, const T[] up, const T[] origin)
    {
        Matrix!T mat;
        mat.m = [
            [ forward[0], forward[1], forward[2], 0.0 ],
            [ right[0],   right[1],   right[2],   0.0 ],
            [ up[0],      up[1],      up[2],      0.0 ],
            [ origin[0],  origin[1],  origin[2],  1.0 ],
        ];
        return mat;
    }

    /**
     * Takes the orientation of a camera in world space.
     * Returns a matrix that transforms from world space to camera space.
     * It's the complement of a model matrix.
     */
    static Matrix!T camera(const T[] forward, const T[] right, const T[] up, const T[] origin)
    {
        Matrix!float translate;
        translate.m = [
            [ 1.0, 0.0, 0.0, 0.0 ],
            [ 0.0, 1.0, 0.0, 0.0 ],
            [ 0.0, 0.0, 1.0, 0.0 ],
            [ -origin[0], -origin[1], -origin[2], 1.0 ],
        ];

        Matrix!float rotate;
        rotate.m = [
            [ forward[0], right[0], up[0], 0.0 ],
            [ forward[1], right[1], up[1], 0.0 ],
            [ forward[2], right[2], up[2], 0.0 ],
            [ 0.0, 0.0, 0.0, 1.0 ],
        ];

        return rotate.multiply(translate);
    }

    // Document style orthographic, where 0, 0 is top left.
    static Matrix!T orthographicDocument(float left, float right, float top, float bottom, float near, float far)
    {
        float midX = (left + right) / 2;
        float midY = (top + bottom) / 2;
        float midZ = (near + far) / 2;

        float scaleX = 2.0 / (right - left);
        float scaleY = 2.0 / (bottom - top);
        float scaleZ = 2.0 / (far - near);

        Matrix!float ortho;
        ortho.m = [
            [ scaleX, 0, 0, 0 ],
            [ 0, -scaleY, 0, 0 ],
            [ 0, 0, scaleZ, 0 ],
            [ -1.0f, 1.0f, 0, 1 ],
        ];

        return ortho;
    }
    
    static Matrix!T perspective(const T hFov, const T vFov, const float nearDist, const float farDist)
    {
        Matrix!T mat;

        const float depth = farDist - nearDist;
        const float ihFov = 1 / hFov;
        const float ivFov = 1 / vFov;
        const float fn1 = -(farDist + nearDist) / depth;
        const float fn2 = (-2 * farDist * nearDist) / depth;

        mat.m = [
            [ ihFov, 0.0,   0.0,  0.0 ],
            [ 0.0,   ivFov, 0.0,  0.0 ],
            [ 0.0,   0.0,   fn1, -1.0 ],
            [ 0.0,   0.0,   fn2,  0.0 ],
        ];

        return mat;
    }

    static Matrix!T reversePerspective(const T hFov, const T vFov, const float nearDist, const float farDist)
    {
        Matrix!T mat;

        const float depth = farDist - nearDist;
        const float ihFov = 1 / hFov;
        const float ivFov = 1 / vFov;
        const float fn1 = nearDist / depth;
        const float fn2 = (farDist * nearDist) / depth;

        mat.m = [
            [ ihFov, 0.0,   0.0,  0.0 ],
            [ 0.0,   ivFov, 0.0,  0.0 ],
            [ 0.0,   0.0,   fn1, -1.0 ],
            [ 0.0,   0.0,   fn2,  0.0 ],
        ];

        return mat;
    }

    Matrix!T transpose()
    {
        Matrix!T dest;
        dest.m = [
            [ m[0][0], m[1][0], m[2][0], m[3][0] ],
            [ m[0][1], m[1][1], m[2][1], m[3][1] ],
            [ m[0][2], m[1][2], m[2][2], m[3][2] ],
            [ m[0][3], m[1][3], m[2][3], m[3][3] ],
        ];

        return dest;
    }

    Matrix!T multiply(const Matrix!T b)
    {
        Matrix!T dest = zero();

        for (auto dr = 0; dr < 4; dr += 1) {
            for (auto dc = 0; dc < 4; dc += 1) {
                for (auto i = 0; i < 4; i += 1) {
                    dest.m[dr][dc] += b.m[dr][i] * m[i][dc];
                }
            }
        }

        return dest;
    }

    void multiplyVec4(const T[] vIn, T[] vOut)
    {
        vOut[0] = vIn[0] * m[0][0] + vIn[1] * m[1][0] + vIn[2] * m[2][0] + vIn[3] * m[3][0];
        vOut[1] = vIn[0] * m[0][1] + vIn[1] * m[1][1] + vIn[2] * m[2][1] + vIn[3] * m[3][1];
        vOut[2] = vIn[0] * m[0][2] + vIn[1] * m[1][2] + vIn[2] * m[2][2] + vIn[3] * m[3][2];
        vOut[3] = vIn[0] * m[0][3] + vIn[1] * m[1][3] + vIn[2] * m[2][3] + vIn[3] * m[3][3];
    }

    void multiplyPos3(const T[] vIn, T[] vOut)
    {
        vOut[0] = vIn[0] * m[0][0] + vIn[1] * m[1][0] + vIn[2] * m[2][0] + m[3][0];
        vOut[1] = vIn[0] * m[0][1] + vIn[1] * m[1][1] + vIn[2] * m[2][1] + m[3][1];
        vOut[2] = vIn[0] * m[0][2] + vIn[1] * m[1][2] + vIn[2] * m[2][2] + m[3][2];
    }

    void multiplyDir3(const T[] vIn, T[] vOut)
    {
        vOut[0] = vIn[0] * m[0][0] + vIn[1] * m[1][0] + vIn[2] * m[2][0];
        vOut[1] = vIn[0] * m[0][1] + vIn[1] * m[1][1] + vIn[2] * m[2][1];
        vOut[2] = vIn[0] * m[0][2] + vIn[1] * m[1][2] + vIn[2] * m[2][2];
    }

    T[16] toFlat()
    {
        T[16] vOut;

        size_t i = 0;
        for (auto r = 0; r < 4; r += 1) {
            for (auto c = 0; c < 4; c += 1) {
                vOut[i] = m[r][c];
                i += 1;
            }
        }

        return vOut;
    }
}

unittest
{
    // Test camera matrix translates the right way
    // Using a 3-4-5 triangle...
    float[3] forward = [ 0.6, 0.8, 0 ];
    float[3] right = [ 0.8, -0.6, 0 ];
    float[3] up = [ 0, 0, 1 ];
    float[3] origin = [ 1, 1, 1 ];
    auto camera = Matrix!float.camera(forward, right, up, origin);

    float[3] test1In = [ 2.2, 2.2, 1.1 ];
    float[3] test1Out;
    camera.multiplyPos3(test1In, test1Out);

    float[3] diff = test1Out[] - [ 1.68f, 0.24f, 0.1f ];
    assert(diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2] < 0.001);
}

unittest
{
    // Test swizzle
    Matrix!float viewMatrix;
    viewMatrix.m = [
        [ 0, 0, -1, 0 ], // forward
        [ 1, 0, 0, 0 ], // right
        [ 0, 1, 0, 0 ], // up
        [ 0, 0, 0, 1 ],
        // x, y, z, w
    ];

    float[4] vIn1 = [ 1, 2, 3, 4 ];
    float[4] vIn2 = [ 1.0, 0.1, 0.2, 1];

    float[4] vOut1, vOut2;

    viewMatrix.multiplyVec4(vIn1, vOut1);
    viewMatrix.multiplyVec4(vIn2, vOut2);

    assert(vOut1 == [ 2.0f, 3.0f, -1.0f, 4.0f ]);
    assert(vOut2 == [ 0.1f, 0.2f, -1.0f, 1.0f ]);
}

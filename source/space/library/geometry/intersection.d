module space.library.geometry.intersection;

import space.library.geometry.vector;
import space.models.interfaces;
import std.math;
import std.math.constants;
import std.algorithm;

import std.stdio;
import std.conv;

struct SphereIntersectResult(T)
{
    int numSolutions;
    T[2] solutions;
}

/**
 * Intersects a line against a unit sphere at the origin.
 * A lot of figures cancel out.
 * See: http://paulbourke.net/geometry/circlesphere/#linesphere
 * Our line is point = u * dir, so if dir is normalized then our solutions are
 * just distances.
 * @todo fix
 */
SphereIntersectResult!T lineSphereIntersect(T)(T[3] point, T[3] dir)
{
    SphereIntersectResult!T result;

    T a = dotProduct(dir, dir);
    T b = 2 * dotProduct(dir, point);
    T c = dotProduct(point, point) - 1;

    T insideRoot = b * b - 4 * a * c;

    if (insideRoot < 0)
    {
        result.numSolutions = 0;
        return result;
    }

    if (insideRoot == 1)
    {
        result.numSolutions = 1;
        result.solutions[] = -b / (2 * a);
        return result;
    }

    T root = sqrt(insideRoot);
    T inv2a = 1.0f / (2 * a);

    result.numSolutions = 2;
    result.solutions[0] = (-b - root) * inv2a;
    result.solutions[1] = (-b + root) * inv2a;
    return result;
}

unittest
{
    import std.conv;
    import std.stdio;

    struct LsiCase
    {
        float[3] point;
        float[3] dir;
        int expectedNum;
        float[2] expectedSolutions;
    }

    LsiCase[] lsiCases = [
        LsiCase([ 0, 0, 0 ], [ 1, 0, 0 ], 2, [ -1, 1 ]),
        LsiCase([ -2, 0, 0 ], [ 1, 0, 0 ], 2, [ 1, 3 ]),
        LsiCase([ -4, 0, 0 ], [ 1, 0, 0 ], 2, [ 3, 5 ]),
        LsiCase([ -1, -SQRT1_2, 0 ], [ 1, 0, 0 ], 2, [ 1 - SQRT1_2, 1 + SQRT1_2 ]),
        LsiCase([ 0, 0, -0.5 ], [ 0, 0, 1 ], 2, [ -0.5, 1.5 ]),
        LsiCase([ 0.5, 0, 0 ], [ 1, 0, 0 ], 2, [ -1.5, 0.5 ]),
    ];

    foreach (lsiCase; lsiCases)
    {
        auto result = lineSphereIntersect(lsiCase.point, lsiCase.dir);
        assert(result.numSolutions == lsiCase.expectedNum,
            "Unexpected numSolutions in " ~ to!string(result) ~ " for " ~ to!string(lsiCase));
        for (int i = 0; i < 2; i += 1)
        {
            assert(result.solutions[i] == lsiCase.expectedSolutions[i],
                "Unexpected solutions in " ~ to!string(result) ~ " for " ~ to!string(lsiCase));
        }
    }
}

/**
 * Returns the point at which a line intersects a plane.
 */
bool linePlaneIntersection(T)(T[6] line, T[4] plane, out T[3] result)
{
    T dot = dotProduct(line[3..6], plane[0..3]);
    if (dot == 0)
    {
        return false;
    }

    T dist = planeCheck(line[0..3], plane) / dot;
    result[] = line[0..3][] - line[3..6][] * dist;
    return true;
}

/**
 * For lines and planes that do intersect, check that the intersect point is
 * what we expect.
 */
unittest
{
    struct IntersectCase
    {
        float[6] line;
        float[4] plane;
        float[3] point;
    }

    IntersectCase[] intersectCases = [
        IntersectCase([0, 0, 0, 0, 0, 1], [ 0, 0, 1, -32 ], [ 0, 0, -32 ]),
        IntersectCase([0, 96, 32, 1, 0, 0], [-0.707107, 0, 0.707107, 0], [ 32, 96, 32 ]),
    ];

    foreach (intersectCase; intersectCases)
    {
        float[3] result;
        bool intersect = linePlaneIntersection(intersectCase.line, intersectCase.plane, result);
        assert(intersect, "No intersection for " ~ to!string(intersectCase));
        
        bool match = result[0] == intersectCase.point[0]
            && result[1] == intersectCase.point[1]
            && result[2] == intersectCase.point[2];

        assert(match, "Points don't match for " ~ to!string(intersectCase) ~ ", got " ~ to!string(result));
    }
}

/**
 * Returns the intersecting line between two planes.
 * Result is all zero if they're parallel.
 * The first 3 values of the result are a point on the line, and the next three
 * are the direction of the line.
 */
T[6] planePlaneIntersection(T)(T[4] planeA, T[4] planeB)
{
    T[6] result = [ 0, 0, 0, 0, 0, 0 ];

    T[3] cross = crossProduct(planeA[0..3], planeB[0..3]);
    if (dotProduct(cross, cross) == 0)
    {
        return result;
    }
    result[3..6] = normalize(cross);

    float[3] absDir = [ abs(result[3]), abs(result[4]), abs(result[5]) ];
    if (absDir[0] > absDir[1] && absDir[0] > absDir[2])
    {
        result[0..3] = solveIntersection(0, 1, 2, planeA, planeB);
    }
    else if (absDir[1] > absDir[2])
    {
        result[0..3] = solveIntersection(1, 0, 2, planeA, planeB);
    }
    else
    {
        result[0..3] = solveIntersection(2, 0, 1, planeA, planeB);
    }

    return result;
}

private T[3] solveIntersection(T)(int axisZ, int axisA, int axisB, T[4] plane1, T[4] plane2)
{
    T a1 = plane1[axisA];
    T b1 = plane1[axisB];
    T d1 = plane1[3];

    T a2 = plane2[axisA];
    T b2 = plane2[axisB];
    T d2 = plane2[3];

    T a0 = ((b2 * d1) - (b1 * d2)) / ((a1 * b2 - a2 * b1));
    T b0 = ((a1 * d2) - (a2 * d1)) / ((a1 * b2 - a2 * b1));

    T[3] result;
    result[axisZ] = 0;
    result[axisA] = a0;
    result[axisB] = b0;

    return result;
}

/**
 * For planes that do intersect, check that the created line's point is on
 * both planes, and that a point further along the line's direction is also
 * on both planes.
 */
unittest
{
    import std.conv;

    bool epsilonInBounds(T)(T value)
    {
        T epsilon = cast(T)1 / cast(T)65535;
        return value > -epsilon && value < epsilon;
    }

    struct PlaneIntersectCase
    {
        float[4] p1;
        float[4] p2;
    }

    PlaneIntersectCase[] planeIntersectCases = [
        PlaneIntersectCase([ 1.0f, 0.0f, 0.0f, 0.0f ], [ 0.0f, 1.0f, 0.0f, 0.0f ]),
        PlaneIntersectCase([ 1.0f, 0.0f, 0.0f, 8.0f ], [ 0.0f, 1.0f, 0.0f, 8.0f ]),
        PlaneIntersectCase([ 0.0f, 0.0f, 1.0f, 100.0f ], [ 1.0f, 1.0f, 1.0f, -100.0f ]),
        PlaneIntersectCase([-1, 0, 0, 248], [-0.242536, 0, 0.970143, 67.91]),
        PlaneIntersectCase([0, 0, -1, 0], [0, 0.707107, 0.707107, 90.5097]),
        PlaneIntersectCase([0, 0, -1, 0], [0.707107, 0, 0.707107, 90.5097]),
    ];

    foreach (planeIntersectCase; planeIntersectCases)
    {
        planeIntersectCase.p1[0..3] = normalize(planeIntersectCase.p1[0..3]);
        planeIntersectCase.p2[0..3] = normalize(planeIntersectCase.p2[0..3]);

        float[6] result = planePlaneIntersection(planeIntersectCase.p1, planeIntersectCase.p2);

        string caseDescription = to!string(planeIntersectCase.p1) ~ " " ~ to!string(planeIntersectCase.p2) ~ " -> " ~ to!string(result);
        assert(epsilonInBounds(planeCheck(result[0..3], planeIntersectCase.p1)), "Result point not on plane 1: " ~ caseDescription);
        assert(epsilonInBounds(planeCheck(result[0..3], planeIntersectCase.p2)), "Result point not on plane 2: " ~ caseDescription);

        float[3] lineP2 = result[0..3][] + result[3..6][] * 8;
        assert(epsilonInBounds(planeCheck(lineP2, planeIntersectCase.p1)), "Result direction not on plane 1: " ~ caseDescription);
        assert(epsilonInBounds(planeCheck(lineP2, planeIntersectCase.p2)), "Result direction not on plane 2: " ~ caseDescription);
    }
}
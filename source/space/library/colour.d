module space.library.colour;

float[4] lerpColour(float[4] a, float[4] b, float frac, float ifrac)
{
    float[4] result = a[] * frac + b[] * ifrac;
    return result;
}

float[4] lerpColour(float[4] a, float[4] b, float frac)
{
    return lerpColour(a, b, frac, 1 - frac);
}

float[4] parseColour(string str)
{
    if (!str.length)
    {
        return [ 0, 0, 0, 0 ];
    }

    if (str[0] == '#')
    {
        return parseColour(str[1..$]);
    }

    if (str.length == 8)
    {
        return [
            parseColour2(str[0..2]),
            parseColour2(str[2..4]),
            parseColour2(str[4..6]),
            parseColour2(str[6..8]),
        ];
    }
    if (str.length == 6)
    {
        return [
            parseColour2(str[0..2]),
            parseColour2(str[2..4]),
            parseColour2(str[4..6]),
            1.0f,
        ];
    }
    if (str.length == 4)
    {
        return [
            parseColour1(str[0..1]),
            parseColour1(str[1..2]),
            parseColour1(str[2..3]),
            parseColour1(str[3..4]),
        ];
    }
    if (str.length == 3)
    {
        return [
            parseColour1(str[0..1]),
            parseColour1(str[1..2]),
            parseColour1(str[2..3]),
            1.0f,
        ];
    }

    return [ 0, 0, 0, 0 ];
}

private int hexDigitToInt(string str)
{
    if (str[0] >= 'a' && str[0] <= 'f')
    {
        return 10 + str[0] - 'a';
    }
    if (str[0] >= 'A' && str[0] <= 'F')
    {
        return 10 + str[0] - 'A';
    }
    if (str[0] >= '0' && str[0] <= '9')
    {
        return str[0] - '0';
    }
    return 0;
}

private float parseColour1(string str)
{
    return cast(float)hexDigitToInt(str) / 15.0f;
}

private float parseColour2(string str)
{
    return cast(float)(hexDigitToInt(str[0..1]) * 16 + hexDigitToInt(str[1..2])) / 255.0f;
}

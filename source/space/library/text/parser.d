module space.library.text.parser;

import std.conv;
import std.regex;

struct Token
{
    string value;
    string type;
    int lineNum;
}

interface TokenParser
{
    // Return the number of characters at the start of string that fully match
    // the token, zero if no complete match exists.
    int parseToken(string str);
}

class QuotedStringToken : TokenParser
{
    int parseToken(string str)
    {
        if (str[0] != '"')
        {
            return 0;
        }
        bool escape = false;
        for (int i = 1; i < str.length; i += 1)
        {
            if (!escape && str[i] == '"')
            {
                return i + 1;
            }
            escape = str[i] == '\\';
        }
        return 0;
    }
}

class MatchToken : TokenParser
{
    private string match;

    this(string match)
    {
        this.match = match;
    }

    int parseToken(string str)
    {
        if (str.length < match.length)
        {
            return 0;
        }
        for (int i = 0; i < match.length; i += 1)
        {
            if (str[i] != match[i])
            {
                return 0;
            }
        }
        return cast(int)match.length;
    }
}

class NewlineToken : TokenParser
{
    int parseToken(string str)
    {
        if (str.length > 1 && (str[0..2] == "\r\n" || str[0..2] == "\n\r"))
        {
            return 2;
        }
        if (str[0..1] == "\n" || str[0..1] == "\r")
        {
            return 1;
        }
        return 0;
    }
}

class RegexToken : TokenParser
{
    private Regex!char regex;

    this(Regex!char regex)
    {
        this.regex = regex;
    }

    int parseToken(string str)
    {
        auto captures = matchFirst(str, regex);
        if (captures.empty)
        {
            return 0;
        }
        string match = captures[0];
        // is there no better way to check if the match was at the start?
        for (int i = 0; i < match.length; i += 1)
        {
            if (str[i] != match[i])
            {
                return 0;
            }
        }
        return cast(int)match.length;
    }
}

class BasicParser
{
    struct TokenType
    {
        string name;
        TokenParser parser;
        bool doNewlines;

        this(string name, TokenParser parser, bool doNewlines = false)
        {
            this.name = name;
            this.parser = parser;
            this.doNewlines = doNewlines;
        }
    }

    private TokenType[] tokenTypes;
    private string str;
    private uint pos;
    private uint lineNum;

    this(TokenType[] tokenTypes)
    {
        this.tokenTypes = tokenTypes;
    }

    void startParsing(string str)
    {
        this.str = str;
        pos = 0;
        lineNum = 0;
    }

    Token getNextToken()
    {
        foreach (TokenType tokenType; tokenTypes)
        {
            int len = tokenType.parser.parseToken(str[pos..$]);
            if (len)
            {
                Token token;
                token.value = str[pos..pos+len];
                token.type = tokenType.name;
                token.lineNum = lineNum;
                if (tokenType.doNewlines)
                {
                    lineNum += 1;
                }
                pos += len;
                return token;
            }
        }

        throw new Exception("Could not parse a token at line " ~ to!string(lineNum) ~ " character " ~ to!string(pos));
    }

    bool eof()
    {
        return pos >= str.length;
    }
}

class Parser
{
    BasicParser parser;
    string[] skipTypes;
    Token next;
    bool eofNext;
    bool eofNow;

    this(BasicParser parser, string[] skipTypes = [])
    {
        this.parser = parser;
        this.skipTypes = skipTypes;
        eofNext = true;
        eofNow = true;
    }

    void startParsing(string str)
    {
        parser.startParsing(str);
        eofNow = false;
        eofNext = false;
        seekNext();
    }

    private void seekNext()
    {
        if (parser.eof)
        {
            eofNow = true;
            eofNext = true;
            return;
        }

        next = parser.getNextToken();

        if (eofNext)
        {
            eofNow = true;
            return;
        }

        if (parser.eof())
        {
            eofNext = true;
        }

        foreach (string type; skipTypes)
        {
            if (next.type == type)
            {
                seekNext();
            }
        }
    }

    Token peekToken()
    {
        return next;
    }

    Token popToken()
    {
        Token t = next;
        seekNext();
        return t;
    }

    // Doesn't advance parsing if we don't get the token we want, meaning we
    // can catch this during parsing.
    Token requireTokenType(string type)
    {
        if (eof)
        {
            throw new Exception("Expected token type " ~ type ~ ", but EOF was reached");
        }

        Token t = next;
        if (t.type != type)
        {
            throw new Exception(
                "At line " ~ to!string(t.lineNum) ~ ": Expected token type " ~ type
                ~ ", got '" ~ t.value ~ "' (" ~ t.type ~ ")");
        }
        seekNext();
        return t;
    }

    Token requireToken(string value)
    {
        if (eof)
        {
            throw new Exception("Expected token value " ~ value ~ ", but EOF was reached");
        }

        Token t = next;
        if (t.value != value)
        {
            throw new Exception(
                "At line " ~ to!string(t.lineNum) ~ ": Expected token value " ~ value
                ~ ", got '" ~ t.value ~ "' (" ~ t.type ~ ")");
        }
        seekNext();
        return t;
    }

    bool eof()
    {
        return eofNow;
    }
}

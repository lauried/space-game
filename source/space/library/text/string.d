module space.library.text.string;

import std.regex;

string decodeQuotedString(string str)
{
    if (str.length < 2)
    {
        return "";
    }

    return replaceAll(str[1..$-1], regex("\\\"", "g"), "\"");
}

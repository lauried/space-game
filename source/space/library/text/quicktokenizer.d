module space.library.text.quicktokenizer;

import std.array;

struct QuickTokenizer
{
    string[] tokens;

    void tokenize(string data)
    {
        tokens = split(data, " ");
        trim();
    }

    string peek()
    {
        string s = tokens.length ? tokens[0] : "";
        return s;
    }

    string pop()
    {
        string s = tokens.length ? tokens[0] : "";
        tokens = tokens[1..$];
        trim();
        return s;
    }

    bool end()
    {
        return tokens.length == 0;
    }

    private void trim()
    {
        while (tokens.length && tokens[0] == "")
        {
            tokens = tokens[1..$];
        }
    }
}
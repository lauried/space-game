module space.library.text.conversion;

import std.conv;
import std.regex;
import std.typecons;

T[n] stringToVector(T : T[n], size_t n)(string value)
{
    auto pattern = regex(`\s+`);
    auto parts = value.splitter(pattern);
    T[n] result;
    for (size_t i = 0; i < n; i += 1)
    {
        result[i] = parts.empty ? 0 : to!T(parts.front);
        parts.popFront();
    }
    return result;
}

/**
 * Lets us read and type tokens from a string.
 */
struct TokenArray
{
    // Rewrite more clunkily if D doesn't guarantee this type.
    Splitter!(No.keepSeparators, string, Regex!char) parts;

    this(string value)
    {
        auto pattern = regex(`\s+`);
        parts = value.splitter(pattern);
    }

    T peek(T)(T def)
    {
        // TODO: Also return def if it doesn't parse.
        return !parts.empty && parts.front.length ? to!T(parts.front) : def;
    }

    T pop(T)(T def)
    {
        T result = peek(def);
        if (!parts.empty)
        {
            parts.popFront();
        }
        return result;
    }
}

module space.library.procedural.randomfield;

struct RandomField
{
    uint seed0 = 0x44ca1571;
    uint seed1 = 0xe35dc708;
    uint seed2 = 0x5019c3ba;
    uint seed3 = 0x193b2a36;
    uint seed4 = 0xdf6d195f;

    uint lcg(uint input)
    {
        ulong temp = 1103515245L * cast(ulong)input + 12345L;
        return temp & 0xffff_ffff;
    }

    uint calculate(int x)
    {
        uint val = wrappedLCG(toUnsigned(x)) ^ seed1;
        return wrappedLCG(val);
    }

    uint calculate(int x, int y)
    {
        uint val = wrappedLCG(toUnsigned(x)) ^ seed1;
        val = wrappedLCG(toUnsigned(y) + val) ^ seed2;
        return wrappedLCG(val);
    }

    uint calculate(int x, int y, int z)
    {
        uint val = wrappedLCG(toUnsigned(x)) ^ seed1;
        val = wrappedLCG(toUnsigned(y) + val) ^ seed2;
        val = wrappedLCG(toUnsigned(z) + val) ^ seed3;
        return wrappedLCG(val);
    }

    uint calculate(int x, int y, int z, int w)
    {
        uint val = wrappedLCG(toUnsigned(x)) ^ seed1;
        val = wrappedLCG(toUnsigned(y) + val) ^ seed2;
        val = wrappedLCG(toUnsigned(z) + val) ^ seed3;
        val = wrappedLCG(toUnsigned(w) + val) ^ seed4;
        return wrappedLCG(val);
    }

    pragma(inline, true) float toFloat(uint val)
    {
        return cast(float) (val & 0xffffff) / cast(float) 0x1000000;
    }

    private union Conv
    {
        int i;
        uint u;
    }

    pragma(inline, true) private uint toUnsigned(int v)
    {
        Conv conv;
        conv.i = v;
        return conv.u;
    }

    pragma(inline, true) private int toSigned(uint v)
    {
        Conv conv;
        conv.u = v;
        return conv.i;
    }

    pragma(inline, true) private uint wrappedLCG(uint input)
    {
        return lcg(input ^ seed0);
    }
}
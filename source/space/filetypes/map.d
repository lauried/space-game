module space.filetypes.map;

import space.library.geometry.vector;
import space.library.text.parser;
import std.conv;
import std.file;
import std.math;
import std.regex;

class MapType
{
    private MapParser parser;

    this(MapParser parser)
    {
        this.parser = parser;
    }

    Map loadMap(string filename)
    {
        string text = readText(filename);
        return parser.parseMap(text);
    }
}

struct Surface
{
    float[3] p1, p2, p3;
    float[4] plane;
    string texture;
    float[3] vs, vt;
    float[2] offset;
    float rotation;
    float[2] scale;
}

struct Brush
{
    Surface[] surfaces;
    bool remove = false;
}

struct Entity
{
    Brush[] brushes;
    string[string] values;
}

class Map
{
    Entity[] entities;
}

// Brush row formats:
// Q1: ( x1 y1 z1 ) ( x2 y2 z2 ) ( x3 y3 z3 ) TEXTURE Xoffset Yoffset rotation Xscale Yscale
// HL: ( x1 y1 z1 ) ( x2 y2 z2 ) ( x3 y3 z3 ) TEXTURE [ Tx1 Ty1 Tz1 Toffset1 ] [ Tx2 Ty2 Tz2 Toffset2 ] rotation Xscale Yscale
// Q3: ( x1 y1 z1 ) ( x2 y2 z2 ) ( x3 y3 z3 ) ( ( Tx1 Ty1 Toffset1 ) ( Tx2 Ty2 Toffset2 ) ) TEXTURE unused unused unused

// I think the last 3 items in the Q3 map are just flat out unused.
// Honestly, HL format is the nicest, but I can't get radiant to save it.
// We'll just have to use Q3 maps.

// After the 3 points we can detect Q3.
// Otherwise, after the texture we detect Q1 vs HL.

class MapParser
{
    private Parser parser;

    this()
    {
        // TODO: Should we make newlines mean something?
        this.parser = new Parser(new BasicParser([
            BasicParser.TokenType("string", new QuotedStringToken()),
            BasicParser.TokenType("{", new MatchToken("{")),
            BasicParser.TokenType("}", new MatchToken("}")),
            BasicParser.TokenType("(", new MatchToken("(")),
            BasicParser.TokenType(")", new MatchToken(")")),
            BasicParser.TokenType("[", new MatchToken("[")),
            BasicParser.TokenType("]", new MatchToken("]")),
            BasicParser.TokenType("comment", new RegexToken(regex(`\/\/[^\n\r]+`))),
            BasicParser.TokenType("newline", new NewlineToken(), true),
            BasicParser.TokenType("whitespace", new RegexToken(regex(`\s+`))),
            BasicParser.TokenType("identifier", new RegexToken(regex(`[a-zA-Z0-9-_./]+`))),
        ]), [ "newline", "whitespace", "comment" ]);
    }

    Map parseMap(string fileData)
    {
        Map map = new Map();
        parser.startParsing(fileData);
        while (!parser.eof())
        {
            map.entities ~= parseEntity();
        }
        return map;
    }

    private Entity parseEntity()
    {
        Entity entity;
        parser.requireTokenType("{");
        while (parser.peekToken().type != "}")
        {
            Token token = parser.peekToken();
            if (token.type == "string")
            {
                Token key = parser.requireTokenType("string");
                Token value = parser.requireTokenType("string");
                entity.values[key.value[1..$-1]] = value.value[1..$-1];
            }
            else if (token.type == "{")
            {
                entity.brushes ~= parseBrush();
            }
        }
        parser.requireTokenType("}");
        return entity;
    }

    private Brush parseBrush()
    {
        Brush brush;
        parser.requireTokenType("{");
        
        bool brushDef = parser.peekToken().value == "brushDef";
        if (brushDef)
        {
            parser.requireToken("brushDef");
            parser.requireTokenType("{");
        }

        while (parser.peekToken().type != "}")
        {
            brush.surfaces ~= parseSurface();
        }

        if (brushDef)
        {
            parser.requireTokenType("}");
        }

        parser.requireTokenType("}");
        return brush;
    }

    private Surface parseSurface()
    {
        Surface surface;
        surface.scale = [ 1, 1 ];
        surface.p1 = parseParenVector();
        surface.p2 = parseParenVector();
        surface.p3 = parseParenVector();
        surface.plane = planeForPoints(surface.p1, surface.p3, surface.p2);

        // Fix for negative zero values which break everything.
        for (int axis = 0; axis < 4; axis += 1)
        {
            if (surface.plane[axis] < 0.000001 && surface.plane[axis] > -0.000001)
            {
                surface.plane[axis] = 0;
            }
        }

        Token token = parser.peekToken();
        // Quake 3 format
        if (token.type == "(")
        {
            parser.requireTokenType("(");
            float[3] sParams = parseParenVector();
            float[3] tParams = parseParenVector();
            parser.requireTokenType(")");

            surface.offset[0] = sParams[2];
            surface.offset[1] = tParams[2];

            // Apparently how Q3 maps are processed:
            // float at1 = -atan2(surface.plane[2], sqrt(surface.plane[0] * surface.plane[0] + surface.plane[1] * surface.plane[1]));
            // float c1 = cos(at1);
            // float s1 = sin(at1);
            // float at2 = atan2(surface.plane[1], surface.plane[0]);
            // float c2 = cos(at2);
            // float s2 = sin(at2);
            // float[3] v1 = [
            //     -s2,
            //     c2,
            //     0,
            // ];
            // float[3] v2 = [
            //     -s1 * c1,
            //     -s1 * s2,
            //     -c1,
            // ];

            float[3] v1;
            float[3] v2;
            float[3] absPlane = [ abs(surface.plane[0]), abs(surface.plane[1]), abs(surface.plane[2]) ];
            if (absPlane[0] > absPlane[1] && absPlane[0] > absPlane[2])
            {
                v1 = [ 0, 1, 0 ];
                v2 = [ 0, 0, 1 ];
            }
            else if (absPlane[1] > absPlane[2])
            {
                v1 = [ 1, 0, 0 ];
                v2 = [ 0, 0, 1 ];
            }
            else
            {
                v1 = [ 1, 0, 0 ];
                v2 = [ 0, 1, 0 ];
            }

            surface.vs[] = v1[] * sParams[0] + v2[] * sParams[1];
            surface.vt[] = v1[] * tParams[0] + v2[] * tParams[1];

            surface.texture = parser.requireTokenType("identifier").value;

            surface.offset[] += [
                to!float(parser.requireTokenType("identifier").value),
                to!float(parser.requireTokenType("identifier").value),
            ][];
            surface.rotation = to!float(parser.requireTokenType("identifier").value);
            
            return surface;
        }

        surface.texture = parser.requireTokenType("identifier").value;
        token = parser.peekToken();
        // Half Life format
        if (token.type == "[")
        {
            parser.requireTokenType("[");
            surface.vs = parseVector();
            surface.offset[0] = to!float(parser.requireTokenType("identifier").value);
            parser.requireTokenType("]");

            parser.requireTokenType("[");
            surface.vt = parseVector();
            surface.offset[1] = to!float(parser.requireTokenType("identifier").value);
            parser.requireTokenType("]");

            surface.rotation = to!float(parser.requireTokenType("identifier").value);
        }
        // Quake format
        else
        {
            surface.offset = [
                to!float(parser.requireTokenType("identifier").value),
                to!float(parser.requireTokenType("identifier").value),
            ];

            surface.rotation = to!float(parser.requireTokenType("identifier").value);

            // TODO: Check this logic: maybe rotation is flipped, and maybe it's different for the negative axes.
            float absX = abs(surface.plane[0]);
            float absY = abs(surface.plane[1]);
            float absZ = abs(surface.plane[2]);
            if (absX > absY && absX > absZ)
            {
                auto rotate = RotateVectorAroundAxis!float([ 1, 0, 0 ]);
                surface.vs = rotate.rotateVector([ 0, 1, 0 ], surface.rotation);
                surface.vt = rotate.rotateVector([ 0, 0, 1 ], surface.rotation);
            }
            else if (absY > absZ)
            {
                auto rotate = RotateVectorAroundAxis!float([ 0, 1, 0 ]);
                surface.vs = rotate.rotateVector([ 1, 0, 0 ], surface.rotation);
                surface.vt = rotate.rotateVector([ 0, 0, 1 ], surface.rotation);
            }
            else
            {
                auto rotate = RotateVectorAroundAxis!float([ 0, 1, 0 ]);
                surface.vs = rotate.rotateVector([ 1, 0, 0 ], surface.rotation);
                surface.vt = rotate.rotateVector([ 0, 1, 0 ], surface.rotation);
            }
        }
        surface.scale = [
            to!float(parser.requireTokenType("identifier").value),
            to!float(parser.requireTokenType("identifier").value),
        ];
        return surface;
    }

    private float[3] parseParenVector()
    {
        parser.requireTokenType("(");
        float[3] result = parseVector();
        parser.requireTokenType(")");
        return result;
    }

    private float[3] parseVector()
    {
        Token f1 = parser.requireTokenType("identifier");
        Token f2 = parser.requireTokenType("identifier");
        Token f3 = parser.requireTokenType("identifier");
        return [
            to!float(f1.value),
            to!float(f2.value),
            to!float(f3.value),
        ];
    }
}

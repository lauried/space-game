module space.filetypes.eav;

import space.library.text.parser;
import space.library.text.string;
import std.file;
import std.regex;

struct Eav
{
    string type;
    string[string] data;
    Eav[] children;
}

class EavType
{
    this()
    {
    }

    Eav loadEav(string filename)
    {
        string text = readText(filename);
        EavParser parser = new EavParser();
        return parser.parseEav(text);
    }
}

class EavParser
{
    private Parser parser;

    this()
    {
        this.parser = new Parser(new BasicParser([
            BasicParser.TokenType("string", new QuotedStringToken()),
            BasicParser.TokenType("{", new MatchToken("{")),
            BasicParser.TokenType("}", new MatchToken("}")),
            BasicParser.TokenType("<", new MatchToken("<")),
            BasicParser.TokenType(">", new MatchToken(">")),
            BasicParser.TokenType("[", new MatchToken("[")),
            BasicParser.TokenType("=", new MatchToken("=")),
            BasicParser.TokenType("identifier", new RegexToken(regex(`[a-zA-Z][a-zA-Z0-9-_./]*`))),
            BasicParser.TokenType("comment", new RegexToken(regex(`\/\/[^\n\r]+`))),
            BasicParser.TokenType("newline", new NewlineToken(), true),
            BasicParser.TokenType("whitespace", new RegexToken(regex(`\s+`))),
        ]), [ "newline", "whitespace", "comment" ]);
    }

    Eav parseEav(string text)
    {
        parser.startParsing(text);

        Eav eav;
        eav.type = "";
        parseBlockContent(eav);
        return eav;
    }

    private void parseBlockContent(ref Eav eav)
    {
        while (!parser.eof())
        {
            auto token = parser.peekToken();

            switch (token.type)
            {
                case "}":
                    return;

                case "<":
                    eav.children ~= parseTypedEav();
                    break;

                case "{":
                    eav.children ~= parseUntypedEav();
                    break;

                default:
                    auto key = parser.requireTokenType("identifier").value;
                    parser.requireTokenType("=");
                    auto value = parser.requireTokenType("string").value;
                    value = decodeQuotedString(value);
                    eav.data[key] = value;
                    break;
            }
        }
    }

    private Eav parseTypedEav()
    {
        parser.requireTokenType("<");
        auto type = parser.requireTokenType("identifier").value;
        parser.requireTokenType(">");

        auto eav = parseUntypedEav();
        eav.type = type;
        return eav;
    }

    private Eav parseUntypedEav()
    {
        Eav eav;
        eav.type = "";
        parser.requireTokenType("{");
        parseBlockContent(eav);
        parser.requireTokenType("}");
        return eav;
    }
}

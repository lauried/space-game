module space.filetypes.image;

import space.exceptions;
import space.platform.sdl;
import derelict.sdl2.sdl;
import derelict.sdl2.image;
import std.conv;

class Image
{
    ubyte[] data;
    int width, height;

    this(int width, int height, const(ubyte*) source)
    {
        if (((width - 1) & width) != 0)
        {
            throw new RuntimeException("Width of image must be power of 2");
        }
        if (((height - 1) & height) != 0)
        {
            throw new RuntimeException("Height of image must be power of 2");
        }

        this.width = width;
        this.height = height;

        size_t size = width * height * 4;
        data.length = size;
        for (int i = 0; i < size; i += 1)
        {
            data[i] = source[i];
        }
    }
}

// This class seems a little redundant, but it'll be useful when we start
// loading from a filesystem interface instead of just files.
class ImageType
{
    private SdlPlatform sdl;

    this(SdlPlatform sdl)
    {
        this.sdl = sdl;
        DerelictSDL2Image.load();

        int flags = IMG_INIT_PNG;

        if (!(IMG_Init(flags) & flags))
        {
            throw new RuntimeException("Could not initialise SDL_Image");
        }
    }

    ~this()
    {
        IMG_Quit();
    }

    Image loadImage(string filename)
    {
        char[] filenameMutable = filename.dup();
        filenameMutable.length += 1;
        filenameMutable[filenameMutable.length - 1] = '\0';
        SDL_Surface* surface = IMG_Load(&filenameMutable[0]);

        if (surface is null)
        {
            throw new RuntimeException("Could not load image '" ~ filename ~ "': " ~ to!string(IMG_GetError()));
        }

        SDL_PixelFormat* format = SDL_AllocFormat(SDL_PIXELFORMAT_RGBA32);
        SDL_Surface* converted = SDL_ConvertSurface(surface, format, 0);
        converted = upscaleSurface(converted);

        SDL_FreeSurface(surface);
        SDL_FreeFormat(format);

        if (converted is null)
        {
            throw new RuntimeException("Could not convert image '" ~ filename ~ "': " ~ to!string(IMG_GetError()));
        }

        const ubyte* data = cast(ubyte*) converted.pixels;
        auto image = new Image(converted.w, converted.h, data);
        SDL_FreeSurface(converted);

        return image;
    }

    // Potentially destroys and recreates the surface given to it.
    // Upscales a surface so that it's power-of-2 sided.
    private SDL_Surface* upscaleSurface(SDL_Surface* surface)
    {
        int width = nextPowerOfTwo(surface.w);
        int height = nextPowerOfTwo(surface.h);

        if (surface.w == width && surface.h == height)
        {
            return surface;
        }

        SDL_Surface* newSurface = SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, SDL_PIXELFORMAT_RGBA32);
        SDL_Rect source = SDL_Rect(0, 0, surface.w, surface.h);
        SDL_Rect dest = SDL_Rect(0, 0, newSurface.w, newSurface.h);

        SDL_BlitScaled(surface, &source, newSurface, &dest);
        SDL_FreeSurface(surface);

        return newSurface;
    }

    private int nextPowerOfTwo(const int value)
    {
        int v = value;
        int r = 0;
        while (v >>= 1)
        {
            r++;
        }
        return 1 << r;
    }
}


import core.stdc.signal;
import core.time;
import space.dependency;
import space.drawing.gui;
import space.engine.systems.atmosphere;
import space.engine.systems.detection;
import space.engine.systems.dynamics;
import space.engine.systems.health;
import space.engine.systems.scene;
import space.engine.systems.ship;
import space.engine.entity;
import space.game.scenario;
import space.game.entities.planet;
import space.game.entities.spacecraft;
import space.game.entities.waypoint;
import space.input.inputcontroller;
import space.input.shipcontrolscheme;
import space.library.geometry.frustum;
import space.library.geometry.orientation;
import space.library.geometry.vector;
import space.models.starfield;
import space.platform.interfaces;
import space.platform.sdl;
import space.renderer.interfaces;
import space.renderer.opengl;
import space.time;
import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.stdio;
import std.typecons;

extern(C) nothrow @nogc void handleSegv(int) { assert(0); }

private int run(string[] args)
{
    signal(SIGSEGV, &handleSegv);

    writeln("Hello world");

    PlatformInterface platform;
    RendererInterface renderer;
    FrameTimer timer;

    SceneSystem scene;
    ShipSystem ship;
    DetectionSystem detection;
    DynamicsSystem dynamics;
    HealthSystem healthSystem;
    AtmosphereSystem atmosphereSystem;

    StarfieldFactory starfieldFactory;

    ScenarioLoader scenarioLoader;
    PlanetFactory planetFactory;
    SpacecraftFactory spacecraftFactory;
    WaypointFactory waypointFactory;

    InputController inputController;
    ShipControlScheme shipControls;
    GuiFactory guiFactory;
    GuiManager guiManager;
    Ecs ecs;

    {
        // Let the DI container be collected once it's been used to set up.
        auto di = scoped!DIContainer; // @suppress(dscanner.suspicious.unmodified)

        // Configure interfaces.
        di.register!(PlatformInterface, SdlPlatform);
        di.register!(RendererInterface, OpenGl);

        // Get instances of the items we need here.
        platform = di.resolve!PlatformInterface;
        renderer = di.resolve!RendererInterface;
        timer = di.resolve!FrameTimer;
        scene = di.resolve!SceneSystem;

        ship = di.resolve!ShipSystem;
        detection = di.resolve!DetectionSystem;
        dynamics = di.resolve!DynamicsSystem;
        healthSystem = di.resolve!HealthSystem;
        atmosphereSystem = di.resolve!AtmosphereSystem;
        
        scenarioLoader = di.resolve!ScenarioLoader;
        planetFactory = di.resolve!PlanetFactory;
        spacecraftFactory = di.resolve!SpacecraftFactory;
        waypointFactory = di.resolve!WaypointFactory;

        starfieldFactory = di.resolve!StarfieldFactory;
        
        ecs = di.resolve!Ecs;
        inputController = di.resolve!InputController;
        shipControls = di.resolve!ShipControlScheme;
        guiFactory = di.resolve!GuiFactory;
        guiManager = di.resolve!GuiManager;
    }

    bool run = true;
    platform.getOnQuit().add(delegate() { run = false; });

    float vidAspect;
    float[2] screenSize;
    platform.getOnVideoMode.add(delegate(int w, int h, bool f) {
        vidAspect = cast(float)h / cast(float)w;
        screenSize = [
            cast(float)w,
            cast(float)h,
        ];
        guiManager.setScreenSize(screenSize);
    });

    platform.setWindowTitle("Space Game");
    platform.startup();

    inputController.setControlScheme(shipControls);

    // Simulation
    ecs.systems.register(dynamics);
    ecs.systems.register(ship);
    ecs.systems.register(healthSystem);
    ecs.systems.register(scene);
    ecs.systems.register(detection);
    ecs.systems.register(atmosphereSystem);

    scenarioLoader.addEntity("Planet", planetFactory);
    scenarioLoader.addEntity("Star", planetFactory);
    scenarioLoader.addEntity("Waypoint", waypointFactory);
    scenarioLoader.addEntity("Spacecraft", spacecraftFactory);

    // TODO: When there's more backgrounds, scenario will want to pick them.
    auto starfield = ecs.entities.create();
    starfield.register!PositionComponent(Orientation!float.identity());
    starfield.register!BackgroundModelComponent(starfieldFactory.createStarfield());

    scenarioLoader.loadScenario("data/scenarios/demo.scn", ecs);

    auto player = ecs.entities.create();
    auto cameraPosition = player.register!PositionComponent(Orientation!float.fromPosition([ -20000, 0, 0 ]));
    auto playerMovement = player.register!MovementComponent([0, 0, 0].staticArray!float, [0, 0, 0].staticArray!float);
    player.register!DynamicsComponent().setRadius(1);
    auto playerShip = player.register!ShipComponent();
    auto playerHealth = player.register!HealthComponent(100);

    // GUI
    auto gui = guiFactory.createGui(import("main.gui"));
    guiManager.setGui(gui);
    string* speedText = gui.getWidget("speedometer").getProperties().get!string("text");
    float* thrustFraction = gui.getWidget("thrust_bar").getProperties().get!float("fraction");
    string* tmiText = gui.getWidget("thrust_mode_indicator").getProperties().get!string("text");
    string* ariText = gui.getWidget("auto_righting_mode_indicator").getProperties().get!string("text");
    float[4]* arColour = gui.getWidget("auto_righting_mode_indicator").getProperties().get!(float[4])("bg");
    float* health = gui.getWidget("health_bar").getProperties().get!float("fraction");
    RadarItem[]* waypoints = gui.getWidget("waypoints").getProperties().get!(RadarItem[])("items");
    float[2]* waypointsFov = gui.getWidget("waypoints").getProperties().get!(float[2])("fov");
    RadarItem[]* radarItems = gui.getWidget("radar").getProperties().get!(RadarItem[])("items");

    float throttleMin = 0;
    float throttleMax = 0;
    float[2] lookDir = [ 0, 0 ];
    float lookYaw = 0;

    while (run)
    {
        platform.handleEvents();
        float frameTime = cast(float) timer.getFrameTime() / 1024.0f;

        playerShip.rotation[0] = shipControls.rollRight.value - shipControls.rollLeft.value;
        playerShip.rotation[1] = shipControls.pitchUp.value - shipControls.pitchDown.value;
        playerShip.rotation[2] = shipControls.turnLeft.value - shipControls.turnRight.value;
        if (shipControls.toggleThrustMode.pressed && shipControls.toggleThrustMode.changed)
        {
            playerShip.mode = playerShip.mode == ShipComponent.Mode.wire ? ShipComponent.Mode.manual : ShipComponent.Mode.wire;
            if (playerShip.mode == ShipComponent.Mode.wire && shipControls.throttleUp.pressed)
            {
                playerShip.throttle[0] = 1.0f;
            }
        }
        if (playerShip.mode == ShipComponent.Mode.wire)
        {
            if (shipControls.throttleDown.changed && shipControls.throttleDown.pressed)
            {
                throttleMin = playerShip.throttle[0] > 0 ? 0 : -0.5;
            }
            if (shipControls.throttleUp.changed && shipControls.throttleUp.pressed)
            {
                throttleMax = playerShip.throttle[0] >= 0 ? 1 : 0;
            }

            playerShip.throttle[0] += (shipControls.throttleUp.value - shipControls.throttleDown.value) * 0.666 * frameTime;
            playerShip.throttle[0] = max(throttleMin, min(throttleMax, playerShip.throttle[0]));
        }
        else
        {
            playerShip.throttle[0] = shipControls.throttleUp.value - shipControls.throttleDown.value;
        }
        if (shipControls.cycleAutoLevel.changed && shipControls.cycleAutoLevel.pressed)
        {
            switch (playerShip.autoLevel)
            {
                case ShipComponent.AutoLevelMode.none: playerShip.autoLevel = ShipComponent.AutoLevelMode.roll; break;
                case ShipComponent.AutoLevelMode.roll: playerShip.autoLevel = ShipComponent.AutoLevelMode.both; break;
                case ShipComponent.AutoLevelMode.both: playerShip.autoLevel = ShipComponent.AutoLevelMode.pitch; break;
                default:
                case ShipComponent.AutoLevelMode.pitch: playerShip.autoLevel = ShipComponent.AutoLevelMode.none; break;
            }
        }

        lookDir = [
            shipControls.lookLeft.value - shipControls.lookRight.value,
            shipControls.lookUp.value - shipControls.lookDown.value,
        ];
        if (lookDir[0] == 0 && lookDir[1] == 0)
        {
            lookYaw = 0;
        }
        else
        {
            lookYaw = atan2(lookDir[0], lookDir[1]);
        }

        inputController.endFrame();

        float fov = 1.0f;
        Orientation!float camera = cameraPosition.orientation;
        camera.rotateZ(lookYaw);

        scene.setCamera(camera, fov, fov * vidAspect);

        // TODO: Update timer to use durations.
        renderer.startFrame();
        ecs.systems.run(dur!"usecs"(cast(int)(frameTime * 1_000_000)));

        renderer.set2D();
        *speedText = to!string(cast(int)length(playerMovement.velocity));
        *thrustFraction = playerShip.throttle[0];
        *ariText = to!string(playerShip.autoLevel);
        *tmiText = to!string(playerShip.mode);
        *arColour = playerShip.autoLevelActive ? [ 0.0f, 0.33f, 0.0f, 1.0f ] : [ 0.0f, 0.0f, 0.0f, 0.0f ];
        *health = playerHealth.health * 0.01;
        auto items = detection.getRadarItems(ecs.entities, cameraPosition.orientation);
        *waypoints = items;
        *radarItems = items;
        *waypointsFov = [ fov, fov * vidAspect ];
        guiManager.draw();

        timer.delayFrame(16);
        timer.frame();
        renderer.finishFrame();
    }

    platform.shutdown();

    return 0;
}

// This stops unit tests messing up.
version (unittest)
{
    // Unit tests generate their own main function.
    // Without the version check, the unit tests fail to compile.
    // There's probably a better way to fix this.
}
else
{
    int main(string[] args)
    {
        return run(args);
    }
}

# Procedural Planet Shorthand

The planet models use a shorthand for procedurally generating their terrain.
It's a series of commands to be performed one after the other to create the
resultant height.

The planet's surface begins completely spherical and the commands each command
successively modifies its height.

## Commands

### noise

```
noise <frequency> <scale>
n <frequency> <scale>
```

Frequency is the horizontal frequency of the noise to generate. Higher
frequencies generate smaller/sharper hills, and it's logarithmic so frequency
2 is half the size of frequency 1.

Scale is the height of the noise to add at that frequency. It's divided by the
frequency, so the same value at frequency 2 adds half the height that it would
at frequency 1.

### noise range

```
noiserange <min_freq> <max_freq> <scale>
nr <min_freq> <max_freq> <scale>
```

As `noise` except noise is added at frequencies starting from the minimum up to
and including the maximum. The same scale is used for all frequencies but is
still divided by the individual frequency to get the height it added. I.e. this
is just a batch command for performing multiple noise commands at once.

### limit

```
limit <min_height> <max_height>
l <min_height> <max_height>
```

Limits/clamps the height so that it is never lower than the minimum or higher
than the maximum. Good for generating flat valleys or plateaus.

### scale

```
scale <scale>
s <scale>
```

Scales the height by the given scaling factor.

### curve

```
curve <power>
c <power>
```

Applies a power curve to the height.
The height is first normalized to the range `-1` to `1`, and then the formula
`height = pow(abs(height), <power>) * sgn(h)`, then the height is restored to
its previous range.

### push

```
push
u
```

Pushes the current height onto a stack and then sets it to zero.
This allows for intermediate values to be saved for reuse later.

### popmul

```
popmul
pm
```

Pops the height from the top of the stack and multiplies it with the current
height.

### popadd

```
popadd
pa
```

Pops the height from the top of the stack and adds it to the current height.
